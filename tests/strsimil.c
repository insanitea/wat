#include "../src/utf8.c"

#include <stdio.h>

int main(int argc, char **argv) {
    size_t i,j;
    double s;

    for(i=1,j=2; j < argc; i+=2,j+=2) {
        if(utf8_strsimil(argv[i], argv[j], &s) == UTF8_MALLOC) {
            fprintf(stderr, "out of memory\n");
            return 1;
        }

        printf("\"%s\" \e[37m$\e[0m \"%s\" = \e[97m%.2f\e[0m\n", argv[i], argv[j], s);
    }

    return 0;
}
