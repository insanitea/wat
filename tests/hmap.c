#include "../src/hmap.c"
#include "../src/string.c"

#define DEFAULT_SIZE 512
#define DEFAULT_POOL 4
#define DEFAULT_COUNT 500000

int main(int argc, char **argv) {
    struct hmap hm;
    size_t size,
           pool,
           count,
           z,i,n,
           max,
           min;
    double opt,
           avg;
    char name[16];

    size = DEFAULT_SIZE;
    pool = DEFAULT_POOL;
    count = DEFAULT_COUNT;

    z = argc-1;
    for(i=1; i < argc; ++i) {
        if(strcmp(argv[i], "-s") == 0 && i < z)
            size = strtoull(argv[++i], NULL, 10);
        else if(strcmp(argv[i], "-p") == 0 && i < z)
            pool = strtoull(argv[++i], NULL, 10);
        else if(strcmp(argv[i], "-i") == 0 && i < z)
            count = strtoull(argv[++i], NULL, 10);
    }

    printf("size: %zu\n", size);
    printf("pool: %zu\n", pool);
    printf("count: %zu\n", count);

    srand(time(NULL));
    hmap_init(&hm, size, pool);
    for(i=0; i < count; ++i) {
        //snprintf(name, sizeof(name), "%i", i);
        strrandlex(name, 4, "0123456789abcdef", 16);
        hmap_insert(&hm, name, NULL, NULL);
    }

    max = 0;
    opt = (double)count/size;
    avg = 0;

    for(i=0; i < size; ++i) {
        avg += hm.slots[i].n_keys;
        if(hm.slots[i].n_keys >= 1)
            ++n;
        if(hm.slots[i].n_keys > max)
            max = hm.slots[i].n_keys;
    }

    avg /= size;
    min = max;

    for(i=0; i < size; ++i) {
        if(hm.slots[i].n_keys >= 1 && hm.slots[i].n_keys < min)
            min = hm.slots[i].n_keys;
    }

    printf("fill: %3zu%% (%zu/%zu) %.2f:%.2f\n", (size_t)(100 * (double)n/size),
                                                 n, size, avg, opt);

    return 0;
}
