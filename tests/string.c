#include "../src/string.c"

#define TEST_STR "'wat',no,duck,'duck',jew,'duck',duck,\"fuck\",'no'"

int main(int argc, char **argv) {
    int e;
    char *str,
         **strv,
         *tk;
    size_t len,
           alloc_len,
           new_len,
           strc,
           count,
           i;
    struct strtoken_s s;

    alloc_len = 256;
    str = malloc(alloc_len+1);
    memset(str, 0, alloc_len);
    len = snprintf(str, alloc_len+1, TEST_STR);

    // strrepl

    printf("\e[97mstrrepl():\e[0m\n");
    printf("  %s [\e[97m%zu\e[0m]\n", str, len);

    struct strrepl_map replv[] = {
        { "jew",  "kike" },
        { "fuck", "wut" }
    };

    len = strlen(str);
    new_len = strrepl(&str, len, &alloc_len, 2, replv, 0, -1);

    printf("> %s [\e[97m%zu\e[0m]\n", str, new_len);
    printf("\n");

    len = new_len;

    // strsplit

    printf("\e[97mstrsplit():\e[0m\n");

    strv = strsplit(str, ",", &strc);
    if(strv == NULL)
        goto e1;

    for(i=0; i < strc; ++i)
        printf("\"%s\" [\e[97m%zu\e[0m]\n", strv[i], strlen(strv[i]));

    strv_free(strv, strc);
    printf("\n");

    // strsplit quoted

    printf("\e[97mstrsplitq():\e[0m\n");

    strv = strsplitq(str, ",", &strc);
    if(strv == NULL)
        goto e1;

    for(i=0; i < strc; ++i)
        printf("\"%s\" [\e[97m%zu\e[0m]\n", strv[i], strlen(strv[i]));

    strv_free(strv, strc);
    printf("\n");

    // strtoken

    printf("\e[97mstrtoken():\e[0m\n");

    strtoken_init(&s, STRTOKEN_CONSUME);

    while(true) {
        i = strtoken_get_next(&s);

        e = strtoken(str, len, &s, ",", &tk, NULL);
        if(e == -1) {
            goto e1;
        }
        else if(e == 0) {
            printf("\e[97m%02zu\e[0m: \"%s\"\n", i, tk);
            if(s.next == -1)
                break;
        }
        else if(e == 1) {
            break;
        }
    }

    printf("\e[97m%zu\e[0m: \e[33m\\0\e[0m\n", i);
    printf("\n");

    return 0;

e1: fprintf(stderr, "out of memory\n");

    return 1;
}
