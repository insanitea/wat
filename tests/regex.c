#include "../src/utf8.c"
#include "../src/regex.c"

int main(int argc, char **argv) {
    int e;
    char *expr,
         *input;
    struct re_node *re;
    struct re_match *m;
    size_t i;

    if(argc < 3) {
        fprintf(stderr, "usage: ./regex-test <expr> <input>\n");
        goto e0;
    }

    expr = argv[1];
    input = argv[2];

    printf("expr: \e[97m%s\e[0m\n", expr);
    printf("input: \e[7;97m%s\e[0m\n", input);
    printf("\n");

    e = re_compile(expr, &re);
    if(e != RE_OK)
        goto e1;

    e = re_exec(re, input, false, &m);
    if(e != RE_OK)
        goto e2;

    printf("%zu captures:\n\n", m->n_captures);
    for(i=0; i < m->n_captures; ++i) {
        if(m->names[i] != NULL)
            printf("\e[37m%2zu\e[0m \e[97m%s\e[0m\e[37m:\e[0m \e[7;97m%s\e[0m\n", i, m->names[i], m->results[i]);
        else
            printf("\e[37m%2zu\e[0m \e[7;97m%s\e[0m\n", i, m->results[i]);
    }

    printf("\n");

    re_destroy(re);
    re_destroy_match(m);

    return 0;

e2: re_destroy(re);
e1: fprintf(stderr, "\e[91merror\e[0m\e[31m:\e[0m %s\n", re_error_str[e]);

e0: return 1;
}
