#include "../src/string.c"
#include "../src/file.c"

#include <limits.h>

int main(int argc, char **argv) {
    char a[PATH_MAX],
         b[PATH_MAX],
         *sp,
         *jp;

//    if(argc < 3) {
//        fprintf(stderr, "not enough arguments, 3 required.");
//        return 1;
//    }

    printf("\e[0;97mfsanitizepath()\e[0m:\n");

    snprintf(a, sizeof(a), "/../this/is/some/stupid/../thing/../path/");
    printf("\e[0;37m>\e[0m %s\n", a);

    if(fsanitizepath(a, &sp) != 0) {
        fprintf(stderr, "fsanitizepath() failed.\n");
        return 1;
    }

    printf("\e[0;37m>\e[0m %s", sp);
    printf("\n");
    printf("\n");

    printf("\e[0;97mfjoinpath()\e[0m:\n");

    snprintf(b, sizeof(b), "/and/this/is/the/rest/of/it");
    printf("\e[0;37m>\e[0m %s\n", sp);

    jp = fjoinpath(sp, b);
    if(jp == NULL) {
        fprintf(stderr, "fjoinpath() failed.\n");
        return 1;
    }

    printf("\e[0;37m>\e[0m %s", jp);
    printf("\n");
    printf("\n");

    return 0;
}
