char tree_str[] = "{\n"
    "  \"wat\": {\n"
    "    \"strings\": [\n"
    "      \"wat\",\n"
    "      \"the\",\n"
    "      \"fuck\"\n"
    "    ],\n"
    "    \"misc\": [\n"
    "      true,\n"
    "      206,\n"
    "      75157.2211\n"
    "    ]\n"
    "  }\n"
"}\n";

#include "../src/string.c"
#include "../src/tree.c"

int main(int argc, char **argv) {
    int e;
    struct tree_node *n,
                     *c;
    struct tree_error_pos ep;
    size_t epos;
    char *s1,
         *s2,
         *s3;
    bool b;
    long long int i;
    long double f;
    size_t j,
           n_items;
    struct tree_node_map *m;

    printf("\e[97mtree_deserialize():\e[0m\n");
    printf("\n");

    e = tree_deserialize(&n, tree_str, &ep);
    if(e != TREE_OK) {
        fprintf(stderr, "tree_deserialize(): %s at %zu:%zu)\n", tree_error_str[e], ep.line, ep.offset);
        return 1;
    }

    tree_write(n, true, "  ", 0, stdout);
    printf("\n");

    // ----

    printf("\e[97mtree_get_multi():\e[0m\n");
    printf("\n");

    struct tree_node_mapping map[] = {
        { "wat.strings[0]", TREE_NODE_STR, &s1 },
        { "wat.strings[1]", TREE_NODE_STR, &s2 },
        { "wat.strings[2]", TREE_NODE_STR, &s3 },
        { "wat.misc[0]", TREE_NODE_BOOL, &b },
        { "wat.misc[1]", TREE_NODE_INT, &i },
        { "wat.misc[2]", TREE_NODE_FLOAT, &f },
    };

    n_items = sizeof(map)/sizeof(struct tree_node_mapping);
    for(j=0; j < n_items; ++j)
        printf("> \"%s\"\n", map[j].path);
    printf("\n");

    tree_get_multi(n, map, n_items);

    printf("strings: %s, %s, %s\n", s1, s2, s3);
    printf("misc: %s, %lli, %Lf\n", b ? "true" : "false", i, f);

    return 0;
}
