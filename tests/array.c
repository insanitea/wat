#include "../src/array.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

void print_int_buf(size_t *x, size_t n) {
    size_t i;

    printf("{ ");

    if(n > 0) {
        printf("%zu", x[0]);
        for(i=1; i < n; ++i)
            printf(", %zu", x[i]);
    }

    printf(" }\n");
}

int main(int argc, char **argv) {
    size_t *x;
    size_t size,
           alloc_size;

    size_t iv[] = { 1, 66,66,66,66,66,66, 22,22, 9,10 },
    //     --
           pos1 = 1,
           nr1 = 6,
           insert1[] = { 2,3 },
           ni1 = sizeof(insert1)/sizeof(insert1[0]),
    //     --
           pos2 = pos1+ni1,
           nr2 = 2,
           insert2[] = { 4,5,6,7,8 },
           ni2 = sizeof(insert2)/sizeof(insert2[0]);

    size = sizeof(iv)/sizeof(iv[0]);
    alloc_size = size;

    x = malloc(sizeof(iv));
    memcpy(x, iv, sizeof(iv));

    printf("\e[97msplice():\e[0m\n");
    printf("        ");
    print_int_buf(x, size);

    if(splice(x, sizeof(iv[0]), size,alloc_size,4, pos1,nr1,ni1,insert1, &x,&size,&alloc_size) == -1)
        goto e1;

    printf("1:(%s) ", nr1 > ni1 ? "+NR" : "+NI");
    print_int_buf(x, size);

    if(splice(x, sizeof(iv[0]), size,alloc_size,4, pos2,nr2,ni2,insert2, &x,&size,&alloc_size) == -1)
        goto e1;

    printf("2:(%s) ", nr2 > ni2 ? "+NR" : "+NI");
    print_int_buf(x, size);

    return 0;

e1: fprintf(stderr, "out of memory\n");
    return 1;
}
