#include "../src/string.c"
#include "../src/btree.c"

#include <stdint.h>
#include <time.h>
#include <math.h>

bool is_unbalanced(struct btree_node *root, ssize_t diff) {
    if(root == NULL || root->height <= 2)
        return false;

    return llabs(diff) >= log2(root->size)+1;
}

int main(int argc, char **argv) {
    int e;
    char **keys,
         key[16];
    struct btree_node *root,
                      *x;
    size_t z,i,r;

    bool rebuild = false;
    size_t insert_count = 12,
           remove_count = 0;

    z = argc-1;
    for(i=1; i < argc; ++i) {
        if(strcmp(argv[i], "-i") == 0 && i < z)
            insert_count = strtoull(argv[++i], NULL, 10);
        else if(strcmp(argv[i], "-r") == 0 && i < z)
            remove_count = strtoull(argv[++i], NULL, 10);
        else if(strcmp(argv[i], "-z") == 0)
            rebuild = true;
    }

    srand(time(NULL));
    if(remove_count > insert_count)
        remove_count = insert_count;

    keys = calloc(insert_count, sizeof(char *));
    if(keys == NULL)
        return 1;

    root = NULL;
    for(i=0; i < insert_count; ++i) {
        while(true) {
            //snprintf(key, sizeof(key), "%zu", i);
            strrandlex(key, 4, "0123456789abcdef", 16);

            e = btree_insert(&root, key, NULL, NULL);
            if(e == BTREE_OK) {
                keys[i] = strdup(key);
                break;
            }
        }
    }

    if(rebuild)
        btree_rebuild(&root);

    // integrity test
    for(i=0; i < insert_count; ++i) {
        if(btree_get(root, keys[i], NULL) == BTREE_NOT_FOUND)
            printf("inaccessible key: %s\n", keys[i]);
    }

    if(remove_count > 0) {
        for(i=(insert_count/2)-(remove_count/2),r=0; r < remove_count; ++i,++r)
            btree_remove(&root, keys[i]);
    }

    printf("\n");
    btree_fprint(stdout, root, NULL);

    printf("\n");
    printf("────────────────────────\n");
    printf(" height:   \e[0;97m%zu\e[0m\n", btree_get_height(root));
    printf(" size:     \e[0;97m%zu\e[0m\n", btree_get_size(root));
    printf(" balance:  \e[0;97m%zd\e[0m\n", btree_get_balance(root));
    printf("────────────────────────\n");
    printf("\n");

    for(i=0; i < insert_count; ++i)
        free(keys[i]);

    free(keys);

    return 0;
}
