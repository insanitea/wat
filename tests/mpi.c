#include <math.h>
#include <stdio.h>
#include <gmp.h>

#define MPI_16BIT
#define MPI_DIGIT_ALLOC_POOL 2
#define MPI_DIGIT_ALLOC_DEFAULT MPI_DIGIT_ALLOC_POOL
#define MPI_DEBUG_BITS

#include "../src/mpi.c"
#include "../src/time.c"

void debug_ref(mpi_t *x, uintmax_t ref) {
    uintmax_t ui;
    unsigned int colour;
    char *cond;

    mpi_to_uint(x,&ui);
    if(ui == ref) {
        cond = "==";
        colour = 2;
    }
    else {
        cond = "!=";
        colour = 1;
    }

    mpi_debug(x);
    fprintf(stderr, "\e[0;9%um", colour);
    mpi_debug_fixed(ui);
    fprintf(stderr, "\e[0m");
    mpi_debug_fixed(ref);
    fprintf(stderr, "\n");

    fprintf(stderr, "\e[0;9%um%jx\e[0m %s \e[0;97m%jx\e[0m\n", colour, ui, cond, ref);
}

void swap_ab(mpi_t *a, mpi_t *b) {
    mpi_t s;

    if(mpi_ucmp(a,b) < 0) {
        s = *a;
        *a = *b;
        *b = s;
    }
}

int main(int argc, char **argv) {
    int r;
    mpi_t r1,r2,r3,a,b,c,s;
    mpi_digit_t d1,d2;
    mpz_t R1,R2,R3,A,B,C,i,j,k;
    uintmax_t t1,t2,t3,c1,c2,c3,x,y,z;
    char *t,*u,*v;
    char T[2048],
         U[2048],
         V[2048],
         R[2048];
    size_t len;
    ptime_t start,end;

    // init

    mpz_inits(R1,R2,R3,A,B,C,i,j,k, NULL);
    mpi_init_multi(&r1,&r2,&r3,&a,&b,&c, NULL);

    // uint conversion

    printf("\e[1;97muint conversion:\e[0m\n");
    printf("\n");

    mpi_random(&a, 64, MPI_NONZERO);
    mpi_to_uint(&a,&x);
    mpi_from_uint(&b,x);
    debug_ref(&b,x);

    printf("\n");

    // string conversion

    printf("\e[1;97mstring conversion:\e[0m\n\n");

    mpi_random(&a, 64, MPI_NONZERO);
    mpi_debug(&a);

    mpi_to_uint(&a,&x);
    mpi_to_str(&a, 16, T, NULL);
    mpi_from_str(&b, 16, T, NULL);
    mpi_debug(&b);
    printf("\n");

    r = mpi_cmp(&a,&b);
    printf("\e[0;9%im%s\e[0m %s %jx\n", r == 0 ? 2 : 1, T, r == 0 ? "==" : "!=", x);
    printf("\n");

    mpi_radix_len(&a, 16, &len);
    printf("base 16 length: \e[0;97m%zu\e[0m\n", len);
    printf("\n");

    // random_range

    printf("\e[1;97mmpi_random_range():\e[0m\n");
    printf("\n");

    x = 0x01;
    y = 0x1000000;

    mpi_from_uint(&a,x);
    mpi_from_uint(&b,y);
    mpi_debug(&a);
    mpi_debug(&b);

    mpi_random_range(&r1,&a,&b, MPI_NONZERO);
    mpi_debug(&r1);
    printf("\n");

    mpi_to_uint(&r1,&t1);
    printf("%jx < \e[0;97m%jx\e[0m < %jx\n", x, t1, y);
    printf("\n");

    // random prime

    printf("\e[1;97mmpi_random_prime():\e[0m\n\n");
    if(mpi_random_prime(&r1, 2, 128, MPI_NONZERO|MPI_PRIME_SAFE) == MPI_OK) {
        mpi_debug(&r1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // add

    printf("\e[1;97mmpi_add():\e[0m\n\n");

    mpi_random(&a, 48, MPI_NONZERO);
    mpi_random(&b, 48, MPI_NONZERO);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = x+y;

    printf("%jx + %jx = \e[97m%jx\e[0m\n", x,y,t1);

    if(mpi_add(&r1,&a,&b) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // sub

    printf("\e[1;97mmpi_sub():\e[0m\n\n");

    mpi_random(&a, 32, MPI_NONZERO);
    mpi_random(&b, 32, MPI_NONZERO);
    swap_ab(&a,&b);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = x-y;

    printf("%jx - %jx = \e[97m%jx\e[0m\n", x,y,t1);

    if(mpi_sub(&r1,&a,&b) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // mul

    printf("\e[1;97mmpi_mul():\e[0m\n\n");

    mpi_random(&a, 128, MPI_NONZERO);
    mpi_random(&b, 128, MPI_NONZERO);
    mpi_to_str(&a,16,T,NULL);
    mpi_to_str(&b,16,U,NULL);

    mpz_set_str(i,T,16);
    mpz_set_str(j,U,16);

    mpz_mul(R1,i,j);
    mpz_get_str(R,16,R1);

    printf("%s * %s = \e[97m%s\e[0m\n", T,U,R);
    printf("\n");

    if(mpi_mul(&r1,&a,&b) == MPI_OK) {
        mpi_debug(&r1);
        printf("\n");

        mpi_to_str(&r1,16,T,NULL);
        r = strcmp(T,R);
        printf("\e[0;9%im%s\e[0m\n", r == 0 ? 2 : 1, T);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // div

    printf("\e[1;97mmpi_div():\e[0m\n\n");

    mpi_random(&a, 32, MPI_NONZERO);
    mpi_random(&b, 24, MPI_NONZERO);
    swap_ab(&a,&b);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = x/y;
    t2 = x%y;

    printf("%jx / %jx = \e[97m%jx\e[0m rem \e[97m%jx\e[0m\n", x,y,t1,t2);

    if(mpi_div(&r1,&r2,&a,&b) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
        debug_ref(&r2,t2);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // lshift

    printf("\e[1;97mmpi_lshift():\e[0m\n\n");

    mpi_from_uint(&a, 0xCCCCCC);
    mpi_from_uint(&b, 4);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = x << y;

    mpi_debug(&a);
    printf("%jx << %ji = \e[97m%jx\e[0m\n", x,y,t1);
    printf("\n");

    if(mpi_lshift(&r1,&a,y) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // rshift

    printf("\e[1;97mmpi_rshift():\e[0m\n\n");

    mpi_from_uint(&a, 0xA0A0A0);
    mpi_from_uint(&b, 4);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = x >> y;

    mpi_debug(&a);
    printf("%jx >> %ji = \e[97m%jx\e[0m\n", x,y,t1);
    printf("\n");

    if(mpi_rshift(&r1,&a,y) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // sqrt

    printf("\e[1;97mmpi_sqrt():\e[0m\n\n");

    mpi_random(&a, 16, MPI_NONZERO);
    mpi_to_uint(&a,&x);
    t1 = sqrtl(x);

    printf("sqrt(%jx)\n", x);
    printf("\n");

    if(mpi_sqrt(&r1,&a) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // log

    printf("\e[1;97mmpi_log():\e[0m\n\n");

    mpi_random(&a, 48, MPI_NONZERO);
    mpi_from_uint(&b,2);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = log2(x);

    printf("log2(%jx)\n", x);
    printf("\n");

    if(mpi_log(&d1,&a,y) == MPI_OK) {
        mpi_from_uint(&r1,d1);
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // pow

    printf("\e[1;97mmpi_pow():\e[0m\n\n");

    mpi_random(&a, 4, MPI_NONZERO);
    mpi_from_uint(&b, 2);
    mpi_to_uint(&a,&x);
    mpi_to_uint(&b,&y);
    t1 = powl(x,y);

    mpi_debug(&a);
    mpi_debug(&b);
    printf("\n");

    printf("pow(%jx,%jx) = \e[97m%jx\e[0m\n", x,y,t1);
    printf("\n");

    if(mpi_pow(&r1,&a,&b) == MPI_OK) {
        debug_ref(&r1,t1);
        printf("\n");
    }
    else {
        printf("mpi_pow() failed\n");
    }

    // powm

    printf("\e[1;97mmpi_powm():\e[0m\n\n");

    mpi_random(&a, 16, MPI_NONZERO);
    mpi_random(&b, 1024, MPI_NONZERO);
    mpi_random(&c, 4096, MPI_NONZERO);
    mpi_to_str(&a,16,T,NULL);
    mpi_to_str(&b,16,U,NULL);
    mpi_to_str(&c,16,V,NULL);

    printf("a = \e[0;97m%s\e[0m / %zu bits\n", T, mpi_bit_count(&a));
    printf("b = \e[0;97m%s\e[0m / %zu bits\n", U, mpi_bit_count(&b));
    printf("c = \e[0;97m%s\e[0m / %zu bits\n", V, mpi_bit_count(&c));
    printf("\n");

    mpz_set_str(i,T,16);
    mpz_set_str(j,U,16);
    mpz_set_str(k,V,16);

    mpz_powm(R1,i,j,k);
    mpz_get_str(R,16,R1);

    start = time_now(CLOCK_MONOTONIC);

    if(mpi_powm(&r1,&a,&b,&c) == MPI_OK) {
        end = time_now(CLOCK_MONOTONIC);
        printf("powm() took %Lf seconds\n", end-start); 
        printf("\n");

        mpi_to_str(&r1,16,T,NULL);
        r = strcmp(T,R);
        printf("\e[0;9%im%s\e[0m %s \e[0;97m%s\e[0m\n", r == 0 ? 2 : 1, T, r == 0 ? "==" : "!=", R);
        printf("\n");
    }
    else {
        printf("\e[0;91merror\e[0m\n");
        printf("\n");
    }

    // inv mod

//     printf("\e[1;97mmpi_inv_mod():\e[0m\n\n");
// 
//     mpi_random(&a, 64, MPI_NONZERO);
//     mpi_random(&b, 64, MPI_NONZERO);
//     mpi_to_uint(&a,&x);
//     mpi_to_uint(&b,&y);
// 
//     mpz_set_ui(i,x);
//     mpz_set_ui(j,y);
//     mpz_invert(R1,i,j);
//     t1 = mpz_get_ui(R1);
// 
//     gmp_printf("%Zx ~%% %Zx\n", i,j);
//     printf("\n");
// 
//     if(mpi_inv_mod(&r1,&a,&b) == MPI_OK) {
//         debug_ref(&r1,t1);
//         printf("\n");
//     }
//     else {
//         printf("\e[0;91merror\e[0m\n");
//         printf("\n");
//     }

    // ext_gcd

//    printf("\e[1;97mmpi_ext_gcd():\e[0m\n\n");
//
//    mpi_random(&a, 16, MPI_NONZERO);
//    mpi_random(&b, 24, MPI_NONZERO);
//
//    mpz_set_ui(i,x);
//    mpz_set_ui(j,y);
//    mpz_gcdext(R1,R2,R3,i,j);
//    t1 = mpz_get_ui(R1);
//    t2 = mpz_get_ui(R2);
//    t3 = mpz_get_ui(R3);
//
//    gmp_printf("ext_gcd (%Zx,%Zx) = \e[97m(%Zx, %Zx, %Zx)\e[0m\n", i,j,R1,R2,R3);
//
//    if(mpi_ext_gcd(&r1,&r2,&r3,&a,&b) == MPI_OK) {
//        debug_ref(&r1,t1);
//        debug_ref(&r2,t2);
//        debug_ref(&r3,t3);
//        printf("\n");
//    }
//    else {
//        printf("\e[0;91merror\e[0m\n");
//        printf("\n");
//    }
//
//    printf("\n");

    mpi_clear_multi(&r1,&r2,&r3,&a,&b,&c, NULL);
    mpz_clears(R1,R2,R3,i,j,k, NULL);

    return 0;
}
