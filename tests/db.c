#include "../src/db/api.c"
#include "../src/db/const.c"
#include "../src/db/context.c"
#include "../src/db/page.c"
#include "../src/db/item.c"
#include "../src/db/cursor.c"
#include "../src/db/qitem.c"
#include "../src/db/misc.c"

#include "../src/string.c"
#include "../src/time.c"

#define NAME_LEN 4
#define STR_LEN 16

int insert_str(struct watdb_item *z, char *name, char *str, ssize_t i) {
    int e;
    struct watdb_qitem q;

    e = watdb_qitem_init(&q, WATDB_STR, name, str, -1);
    if(e != WATDB_OK) {
        fprintf(stderr, "watdb_qitem_new() failed\n");
        goto e0;
    }

    e = watdb_list_insert(z, i, +1, &q);
    if(e != WATDB_OK) {
        fprintf(stderr, "watdb_list_insert() failed\n");
        goto e1;
    }

    watdb_qitem_deinit(&q);

    return WATDB_OK;

e1: watdb_qitem_deinit(&q);

e0: return e;
}

int get_data_paged(struct watdb_item *z, size_t page_size) {
    int e;
    ssize_t i;
    size_t p;
    struct watdb_qitem *r;
    ptime_t t1,t2,et;

    p = 1;
    i = watdb_list_len(z)-1;

    while(true) {
        t1 = time_now(CLOCK_MONOTONIC);

        e = watdb_list_get(z, i, page_size, -1, NULL, NULL, &r, &i);
        if(WATDB_FATAL(e)) {
            fprintf(stderr, "watdb_list_get() failed: %s\n", watdb_status_str[e]);
            return e;
        }

        t2 = time_now(CLOCK_MONOTONIC);
        et = t2-t1;

        fprintf(stderr, "%Lfs elapsed\npage %zu\nnext: %zi\n\n", et, p++, i);
        watdb_qitem_print(r, stderr, 0);
        fprintf(stderr, "\n");

        watdb_qitem_destroy(r);

        if(e == WATDB_NOT_FOUND)
            break;
    }

    return WATDB_OK;
}

int serialize_test(void) {
    int e;
    char *str;
    struct watdb_qitem *r;
    struct watdb_err_cursor v;
    size_t i;

    str = "{ name: \"insanitea\", id: 206.7, likes: \"tea\", secret: bin(ae5acea0fe) }";
    fprintf(stderr, "\nDESERIALIZE\n------------\n%s\n", str);

    e = watdb_deserialize(str, &r, &v);
    if(e != WATDB_OK) {
        for(i=0; i < v.offset; ++i)
            fwrite(" ", 1, 1, stderr);

        fwrite("^\n", 2, 1, stderr);
        fprintf(stderr, "\e[0;31m%s\e[0m\n\n", watdb_status_str[e]);

        return e;
    }

    fprintf(stderr, "\nSERIALIZE\n----------\n");
    str = NULL;

    e = watdb_serialize(r, 2, &str);
    if(e != WATDB_OK) {
        fprintf(stderr, "watdb_serialize() failed\n");
        return e;
    }

    fprintf(stderr, "%s\n\n", str);

    return WATDB_OK;
}

int main(int argc, char **argv) {
    int e;
    struct watdb_ctx *ctx;
    struct watdb_item *root,
                      *z;
    size_t i,j;
    char name[NAME_LEN+1],
         str[STR_LEN+1];
    ptime_t t1,t2,et;

    bool insert = false,
         get = false,
         serialize = false;
    size_t n_inserts = 25,
           page_size = 10;

    for(i=1,j=i+1; i < argc; ++i,j=i+1) {
        if(strcmp(argv[i], "-i") == 0) {
            insert = true;
        }
        else if(strcmp(argv[i], "-n") == 0) {
            n_inserts = strtoul(argv[j], NULL, 10);
        }
        else if(strcmp(argv[i], "-g") == 0) {
            get = true;
        }
        else if(strcmp(argv[i], "-s") == 0) {
            serialize = true;
        }
        else if(strcmp(argv[i], "-ps") == 0 && j < argc) {
            page_size = strtoul(argv[j], NULL, 10);
        }
    }

    e = watdb_open("test", NULL, &ctx, &root);
    if(e != WATDB_OK) {
        fprintf(stderr, "watdb_open() failed\n");
        goto e0;
    }

    e = watdb_access(root, "stuff", WATDB_LIST, true, &z);
    if(e != WATDB_OK) {
        fprintf(stderr, "watdb_access() failed\n");
        goto e0;
    }

    if(insert) {
        srand(time(NULL));

        t1 = time_now(CLOCK_MONOTONIC);

        for(i=1; i <= n_inserts; ++i) {
            snprintf(name, NAME_LEN, "%02zi", i);
            strrandlex(str, STR_LEN, "abcdefghijklmnopqrstuvwxyz0123456789", 36);

//             e = insert_str(z, name, str, rand()%(z->meta->chain.len+1));
            e = insert_str(z, name, str, -1);
            if(e != WATDB_OK)
                goto e0;
        }

//         e = insert_str(z, "**", "xxxxxxxx", 0);
//         if(e != WATDB_OK)
//             goto e0;

        t2 = time_now(CLOCK_MONOTONIC);
        et = t2-t1;

        fprintf(stderr, "%Lfs elapsed\n\n", et);

        if(get) {
//             item_print(root, stderr, 0);
//             fprintf(stderr, "\n");

            get_data_paged(z, page_size);
        }
    }
    
    if(serialize)
        serialize_test();

    watdb_close(ctx);

    return 0;

e0: fprintf(stderr, "error: [%i] %s\n", e, watdb_status_str[e]);

    return 1;
}
