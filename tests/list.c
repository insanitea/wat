#include "../src/list.c"
#include "../src/time.c"
#include "../src/string.c"

#define DEFAULT_INSERT_COUNT 10000000
#define ITEM_MAX_LEN 8

char *lex = "0123456789abcdef";
size_t lex_len = 16;

void print_list_range(struct list *l, size_t start, size_t end) {
    size_t i;
    for(i=start; i < end; ++i)
        printf("%2zu: %s\n", i, (char *)l->items[i].ptr);
}

int main(int argc, char **argv) {
    int e;
    struct list L;

    e = list_init(&L, 0, 256);
    if(e != 0)
        goto e1;

    size_t insert_count,
           i,
           ni;
    char *item;
    double start,
           elapsed;

    insert_count = argc > 1 ? strtoull(argv[1], NULL, 10) : DEFAULT_INSERT_COUNT;

    srand(time_now(CLOCK_REALTIME));

    start = time_now(CLOCK_MONOTONIC);
    for(i=0; i < insert_count; ++i) {
        item = malloc(ITEM_MAX_LEN+1);
        if(item == NULL)
            goto e1;

        strrandlex(item, ITEM_MAX_LEN, lex, lex_len);
        list_append(&L, item, free);
    }

    elapsed = time_now(CLOCK_MONOTONIC)-start;
    printf("> list generation of %zu items took %f s\n", insert_count, elapsed);

    if(insert_count <= 50) {
        printf("\n");
        print_list_range(&L, 0, insert_count);
        printf("\n");
    }

    char *insert[] = { "1", "2", "3", "4", "5" };

    ni = sizeof(insert)/sizeof(insert[0]);

    i = insert_count/2;
    start = time_now(CLOCK_MONOTONIC);
    e = list_splice(&L, i,0,ni, (void **)insert, NULL);
    if(e != 0)
        goto e1;

    elapsed = time_now(CLOCK_MONOTONIC)-start;
    printf("> list insertion at index %zu took %f s\n", i, elapsed);

    if(insert_count <= 50) {
        printf("\n");
        print_list_range(&L, 0, L.used_size);
        printf("\n");
    }

    i = insert_count/2;
    start = time_now(CLOCK_MONOTONIC);
    list_splice(&L, i,ni,0, NULL, NULL);

    elapsed = time_now(CLOCK_MONOTONIC)-start;
    printf("> list removal at index %zu took %f s\n", i, elapsed);

    if(insert_count <= 50) {
        printf("\n");
        print_list_range(&L, 0, L.used_size);
        printf("\n");
    }

    start = time_now(CLOCK_MONOTONIC);
    list_qsort(&L, list_qsort_str_lex_asc);
    elapsed = time_now(CLOCK_MONOTONIC)-start;
    printf("> list sort took %f s\n", elapsed);

    if(insert_count <= 50) {
        printf("\n");
        print_list_range(&L, 0, L.used_size);
        printf("\n");
    }

    return 0;

e1: fprintf(stderr, "out of memory\n");
    return 1;
}
