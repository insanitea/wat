#include "../src/utf8.c"

#define DEFAULT_TEST_STR "\xE2\x9D\x8A×w×a×t×"

void binwrite(char x, FILE *fh) {
    int i;
    for(i=0; i < 8; ++i) {
        if(x & (1 << (8-i-1)))
            fwrite("1",1,1,fh);
        else
            fwrite("0",1,1,fh);
    }
}

int main(int argc, char **argv) {
    int e;
    size_t ec,dc,
           blen,
           i,j,
           ulen;
    char es[7];
    bool v;
    uint32_t code;
    char *repr_str,
         *test_str,
         rc;

    if(argc == 3 && strcmp(argv[1], "-e") == 0) {
        code = strtoull(argv[2], NULL, 16);

        if(utf8_encode_char(code, es, &ec) == UTF8_INVALID) {
            printf("invalid utf-8 code\n");
            return 1;
        }

        printf("%.*s %x/%u:\n", (int)ec,es, code, code);

        printf("\e[37m%02hhx\e[0m ", es[0]);
        if(ec > 1) printf("\e[45m");
        else       printf("\e[44m");
        binwrite(es[0], stdout);
        printf("\e[0m\n");

        for(i=1; i < ec; ++i) {
            printf("\e[37m%02hhx\e[0m ", es[i]);
            printf("\e[46m"); // continuation = cyan bg
            binwrite(es[i], stdout);
            printf("\e[0m\n");
        }

        return 0;
    }

    if(argc > 1) {
        test_str = argv[1];
    }
    else {
        test_str = alloca(256);
        snprintf(test_str, 256, DEFAULT_TEST_STR);
    }

    e = utf8_validate(test_str, &ulen, &blen);
    if(e == UTF8_OK)
        v = true;
    else
        v = false;

    repr_str = malloc((ulen*6)+1);

    for(i=0; i < blen; i+=dc) {
        e = utf8_decode_char(test_str+i, &code, &dc);

        // byte index
        printf("\e[37m%-4zu\e[0m ", i);

        // char
        if(dc >= 1)
            printf("\e[97m%.*s\e[0m ", (int)dc, test_str+i);
        else
            printf("%*s ", 1,"");

        // separator
        printf("\e[37m│\e[0m ");

        // byte
        if(dc >= 1)
            printf("%02hhx ", test_str[i]);
        else
            printf("%*s ", 2,"");

        // bits
        if(e == UTF8_INVALID || dc == -1)
            printf("\e[41m"); // invalid; red bg
        else if(dc == 1)
            printf("\e[44m"); // ascii; blue bg
        else
            printf("\e[45m"); // unicode = magenta bg
        binwrite(test_str[i], stdout);
        printf("\e[0m ");

        // separator
        printf("\e[37m│\e[0m ");

        if(dc > 1) {
            e = utf8_encode_char(code, repr_str+i, &ec);

            // byte
            printf("%02hhx ", repr_str[i]);

            // bits
            if(e == UTF8_INVALID || ec != dc)
                printf("\e[41m"); // invalid; red bg
            else if(ec == 1)
                printf("\e[44m"); // ascii; blue bg
            else
                printf("\e[45m"); // unicode = magenta bg
            binwrite(repr_str[i], stdout);
            printf("\e[0m ");

            printf("\e[37m│\e[0m ");

            // code
            printf("%x", code);

            // continuation bytes
            for(j=1; j < dc; ++j) {
                printf("\n");

                // index
                printf("\e[37m%-4zu\e[0m ", i+j);
                // align(char)
                printf("  ");
                // separator
                printf("\e[37m│\e[0m ");
                // byte
                printf("%02hhx ", test_str[i+j]);
                // bits
                printf("\e[46m");
                binwrite(test_str[i+j], stdout);
                printf("\e[0m ");

                printf("\e[;37m│\e[0m ");

                // reencoded
                if(j < ec) {
                    // byte
                    printf("%02hhx ", repr_str[i+j]);
                    // bits
                    printf("\e[46m");
                    binwrite(repr_str[i+j], stdout);
                    printf("\e[0m ");
                    // separator
                    printf("\e[37m│\e[0m ");
                    // align(code)
                    printf("\e[37m--\e[0m");
                }
                else {
                    printf("%*s %*s ", 2,"", 8,"");
                }
            }
        }
        else {
            // align(byte) + align(bits)
            printf("%*s %*s ", 2,"", 8,"");
            // separator
            printf("\e[;37m│\e[0m ");
            // code
            printf("%x", code);

            if(dc == -1)
                dc = 1;

            repr_str[i] = test_str[i];
        }

        printf("\n");
    }

    // status + length info

    printf("\n");
    printf("valid: %s\n", v ? "\e[;92myes\e[0m" : "\e[;91mno\e[0m");
    printf("bytes: %zu\n", blen);
    printf("chars: %zu\n", ulen);
    printf("\n");

    // utf8_normalize() test

    rc='_';
    printf("\e[097mutf8_normalize(repl='%c'):\e[0m\n", rc);
    printf("\n");

    blen = utf8_normalize(repr_str, rc);

    printf("\e[97m");
    printf("%2c", repr_str[0]);
    for(i=1; i < blen; ++i)
        printf(" %2c", repr_str[i]);
    printf("\e[0m\n");

    printf("\e[37m");
    fwrite("──", 2, 3, stdout);
    for(i=1; i < blen; ++i)
        fwrite("───", 3, 3, stdout);
    printf("\e[0m\n");

    printf("%02hhx", repr_str[0]);
    for(i=1; i < blen; ++i)
        printf(" %02hhx", repr_str[i]);
    printf("\n");

    printf("\n");

    return 0;
}
