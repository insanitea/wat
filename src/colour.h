#ifndef WAT_COLOUR_H
#define WAT_COLOUR_H

#include <stdbool.h>
#include <stdint.h>

#define rgba_copy(dest,src)   memcpy(dest, src, sizeof(struct rgba))
#define rgba_cairo_set(cr,c)  cairo_set_source_rgba(cr, (c)->r, (c)->g, (c)->b, (c)->a)
#define hsva_copy(dest,src)   memcpy(dest, src, sizeof(struct hsva))

struct hsva { double h,s,v,a; };
struct rgba { double r,g,b,a; };
struct rgba_pair { struct rgba bg,fg; };
struct rgba_values { unsigned long long int r,g,b,a; };

typedef char rgba_hex[10];

enum {
  RGBA_8,
  RGBA_16,
  RGBA_32,
  RGBA_64
};

inline static
void rgba_set (struct rgba *c, double r, double g, double b, double a) {
  c->r = r;
  c->g = g;
  c->b = b;
  c->a = a;
}

int      rgba_from_str   (struct rgba *c, char *str);
void     rgba_desat_avg  (struct rgba *dest, struct rgba *src);
void     rgba_mix        (struct rgba *dest, struct rgba *a, struct rgba *b, double m);
void     rgba_mix_ch     (struct rgba *dest, struct rgba *a, struct rgba *b, double rm,
                                                                             double gm,
                                                                             double bm,
                                                                             double am);
void     rgba_shade      (struct rgba *c, double v);
void     rgba_to_hsva    (struct rgba *in, struct hsva *out);
void     rgba_to_hex     (struct rgba *c, char *dest, bool alpha);
uint32_t rgba_to_uint32  (struct rgba *c);
void     rgba_get_values (struct rgba *c, struct rgba_values *v, int prec);

inline static
void hsva_set (struct hsva *c, double h, double s, double v, double a) {
  c->h = h;
  c->s = s;
  c->v = v;
  c->a = a;
}

int      hsva_from_str   (struct hsva *c, char *str);
void     hsva_mix        (struct hsva *dest, struct hsva *a, struct hsva *b, double m);
void     hsva_mix_ch     (struct hsva *dest, struct hsva *a, struct hsva *b, double hm,
                                                                             double sm,
                                                                             double vm,
                                                                             double am);
void     hsva_shade      (struct hsva *c, double v);
void     hsva_to_rgba    (struct hsva *in, struct rgba *out);

#endif
