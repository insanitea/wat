#ifndef WAT_TIME_H
#define WAT_TIME_H

#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <sys/types.h>

// requires full year
#define is_leap_year(y)    (((y) % 400) == 0 || (((y) % 100) != 0 && ((y) % 4) == 0))
// requires 'struct tm' values
#define name_of_day(y,m,d) day_names[day_of_week(y,m,d)]
#define name_of_month(m)   month_names[m]
#define days_in_year(y)    (is_leap_year(1900+y) ? 366 : 365)
#define days_in_month(m,y) ((m) < 12 ? ((m) == FEBRUARY && is_leap_year(1900+y) ? mday_counts[m]+1 : mday_counts[m]) : 0)

enum time_offset_unit {
    YEARS,
    MONTHS,
    DAYS,
    HOURS,
    MINUTES,
    SECONDS,
    MSECONDS,
    UNIT_MAX
};

enum {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    DAY_MAX
};

enum {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER,
    MONTH_MAX
};

typedef long double ptime_t;

struct time_diff {
    int sign;
    unsigned int base,
                 years, months, // fuzzy diffs
                 days, hours, minutes, seconds, mseconds;
};

extern char *time_units[];
extern char *day_names[7];
extern char *month_names[12];
extern uint8_t mday_counts[12];

unsigned int day_of_week(unsigned int year, unsigned int month, unsigned int day);
unsigned int day_of_year(unsigned int year, unsigned int month, unsigned int day);

int     time_cmp(struct tm *t1, struct tm *t2);
ptime_t time_now(clockid_t clock);
int     time_parse_offset(char *offset, struct tm *ref, ptime_t *diff_ret);
void    time_parse_diff(ptime_t diff, struct tm *ref, unsigned int base_unit, struct time_diff *d);
ssize_t time_snprint_diff(char *buf, size_t n, char *delim, struct time_diff *d, bool ltrim, bool rtrim);
int     time_round(ptime_t *t, unsigned int precision, ptime_t threshold);

#endif
