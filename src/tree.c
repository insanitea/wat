#include "tree.h"
#include "string.h"

#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <sys/types.h>

char *tree_error_str[] = {
    [TREE_OK] = "ok",
    [TREE_NOT_FOUND] = "node not found",
    [TREE_BAD_PATH] = "bad path",
    [TREE_BAD_TYPE] = "incorrect node type",
    [TREE_MALLOC] = "out of memory",
    [TREE_INVALID_NUMBER] = "invalid number",
    [TREE_UNEXPECTED_CHAR] = "unexpected character",
    [TREE_UNEXPECTED_DELIM] = "unexpected delimiter",
    [TREE_EXPECTED_DELIM] = "expected delimiter",
    [TREE_EXPECTED_COLON] = "expected colon",
    [TREE_EXPECTED_DIGIT] = "expected digit",
    [TREE_UNCLOSED_COMMENT] = "unclosed comment",
    [TREE_UNCLOSED_QUOTE] = "unclosed quote",
    [TREE_UNCLOSED_LIST] = "unclosed list",
    [TREE_UNCLOSED_DICT] = "unclosed dictionary",

};

struct strrepl_map qmap = { "\"", "\\\"" },
                   rqmap = { "\\\"", "\"" };

int tree_descend_named(struct tree_node *n, int type, bool touch, char *tk, char *ptr, struct tree_node **n_ret) {
    int e;
    size_t name_len;
    char *name;
    struct tree_node *c;

    name_len = ptr-tk;
    name = strndup(tk, name_len);
    if(name == NULL) {
        e = TREE_MALLOC;
        goto e0;
    }

    for(c=n->child; c != NULL; c=c->next) {
        if(c->name != NULL && strcmp(c->name, name) == 0)
            break;
    }

    if(c == NULL) {
        if(touch) {
            e = tree_node_new(&c, type, name);
            if(e != TREE_OK)
                goto e1;

            tree_node_attach(c,n);
        }
        else {
            e = TREE_NOT_FOUND;
            goto e1;
        }
    }

    free(name);

    *n_ret = c;

    return TREE_OK;

e1: free(name);

e0: return e;
}

int tree_descend_subscript(struct tree_node *n, int type, bool touch, char *ptr, char **ptr_ret, struct tree_node **n_ret) {
    int e,t;
    int attach;
    ssize_t ss;
    char *end;
    size_t i;
    struct tree_node *c;

    attach = 0;

    ++ptr;
    if(*ptr == ':') {
        touch = true;
        attach = -1;
        ++ptr;
    }

    errno = 0;
    ss = strtoll(ptr, &end, 10);
    if(errno != 0 || end == ptr)
        return TREE_BAD_PATH;

    ptr = end;
    if(*ptr == ':') {
        ++ptr;

        if(attach != 0) {
            // syntax enforcement.
            // insert before or after, not both.
            return TREE_BAD_PATH;
        }

        touch = true;
        attach = +1;
    }
    else if(*ptr != ']') {
        return TREE_BAD_PATH;
    }

    ++ptr;

    if(ss < 0) {
        ss = llabs(ss)-1;
        for(i=0,c=n->last_child; i < ss && c != NULL; ++i,c=c->prev);
    }
    else {
        for(i=0,c=n->child; i < ss && c != NULL; ++i,c=c->next);
    }

    if(attach != 0 && c != NULL) {
        if(*ptr == '.') {
            t = TREE_NODE_DICT;
            ++ptr;
        }
        else if(*ptr == '[') {
            t = TREE_NODE_LIST;
        }
        else if(*ptr == '\0') {
            t = type;
        }
        else {
            return TREE_BAD_PATH;
        }

        n=c;

        e = tree_node_new(&c, t, NULL);
        if(e != TREE_OK)
            return e;

        if(attach == -1)
            tree_node_attach_before(c,n);
        else // if(attach == +1)
            tree_node_attach_after(c,n);
    }
    else if(touch && c == NULL) {
        if(*ptr == '.') {
            t = TREE_NODE_DICT;
            ++ptr;
        }
        else if(*ptr == '[') {
            t = TREE_NODE_LIST;
        }
        else if(*ptr == '\0') {
            t = type;
        }
        else {
            return TREE_BAD_PATH;
        }

        e = tree_node_new(&c, t, NULL);
        if(e != TREE_OK)
            return e;

        tree_node_attach(c,n);
    }

    *ptr_ret = ptr;
    *n_ret = c;

    return TREE_OK;
}

inline static
int tree_serialize_calc_len(struct tree_node *n, bool pretty, size_t indent_len, int indent_level, size_t *len) {
    int e;

    while(true) {
        if(pretty)
            *len += (indent_len * indent_level);

        if(n->parent != NULL && n->parent->type == TREE_NODE_DICT) {
            if(n->name != NULL) {
                *len += 1+strlen(n->name)+2; // opening quote + key + closing quote + colon
                if(pretty)
                    *len += 1; // space
                *len += strstrc(n->name, "\""); // increment for escaped quotations marks
            }
            else {
                *len += 3; // two quotes + colon
                if(pretty)
                    *len += 1; // space
            }
        }

        if(n->type == TREE_NODE_DICT || n->type == TREE_NODE_LIST) {
            *len += 1; // opening bracket
            if(pretty)
                *len += 1; // newline

            e = tree_serialize_calc_len(n->child, pretty, indent_len, indent_level+1, len);
            if(e != TREE_OK)
                return e;

            if(pretty)
                *len += (indent_len * indent_level);

            *len += 1; // closing bracket
        }
        else if(n->type == TREE_NODE_BOOL) {
            if(n->data.b)
                *len += 4; // true
            else
                *len += 5; // false
        }
        else if(n->type == TREE_NODE_INT) {
            *len += snprintf(NULL, 0, "%lli", n->data.i);
        }
        else if(n->type == TREE_NODE_FLOAT) {
            *len += snprintf(NULL, 0, "%Lf", n->data.f);
        }
        else if(n->type == TREE_NODE_STR) {
            if(n->data.s != NULL)
                *len += strlen(n->data.s)+2; // string + quotes
            else
                *len += 4; // null

            *len += strstrc(n->data.s, "\"");
        }
        else if(n->type == TREE_NODE_PTR) {
            *len += snprintf(NULL, 0, "%p", n->data.ptr);
        }
        else {
            return TREE_BAD_TYPE;
        }

        if(n->next != NULL) {
            *len += 1; // comma
            if(pretty)
                *len += 1; // newline

            n = n->next;
            continue;
        }

        if(pretty)
            *len += 1; // newline

        break;
    }

    return TREE_OK;
}

inline static
void tree_serialize_concat(char **dest, char *src, size_t n) {
    memcpy(*dest, src, n);
    *dest += n;
}

inline static
void tree_serialize_recurse(struct tree_node *n, char **sp, bool pretty, char *indent_str, size_t indent_len, unsigned int indent_level) {
    size_t i;
    char* tmp;
    char buf[32];
    size_t len;

    while(true) {
        if(pretty) {
            for(i=0; i < indent_level; ++i)
                tree_serialize_concat(sp, indent_str, indent_len);
        }

        if(n->parent != NULL && n->parent->type == TREE_NODE_DICT) {
            if(n->name != NULL) {
                tmp = strdup(n->name);
                strrepl(&tmp, -1, NULL, 1, &qmap, 0, -1);

                tree_serialize_concat(sp, "\"", 1);
                tree_serialize_concat(sp, tmp, strlen(tmp));
                tree_serialize_concat(sp, "\":", 2);

                free(tmp);
            }
            else {
                // empty name for consistency
                tree_serialize_concat(sp, "\"\":", 3);
            }

            if(pretty)
                tree_serialize_concat(sp, " ", 1); // space after colon
        }

        if(n->type == TREE_NODE_DICT) {
            tree_serialize_concat(sp, "{", 1);
            if(pretty)
                tree_serialize_concat(sp, "\n", 1);

            tree_serialize_recurse(n->child, sp, pretty, indent_str, indent_len, indent_level+1);

            if(pretty) {
                for(i=0; i < indent_level; ++i)
                    tree_serialize_concat(sp, indent_str, indent_len);
            }

            tree_serialize_concat(sp, "}", 1);
        }
        else if(n->type == TREE_NODE_LIST) {
            tree_serialize_concat(sp, "[", 1);
            if(pretty)
                tree_serialize_concat(sp, "\n", 1);

            tree_serialize_recurse(n->child, sp, pretty, indent_str, indent_len, indent_level+1);

            if(pretty) {
                for(i=0; i < indent_level; ++i)
                    tree_serialize_concat(sp, indent_str, indent_len);
            }

            tree_serialize_concat(sp, "]", 1);
        }
        else if(n->type == TREE_NODE_BOOL) {
            if(n->data.b)
                tree_serialize_concat(sp, "true", 4);
            else
                tree_serialize_concat(sp, "false", 5);
        }
        else if(n->type == TREE_NODE_INT) {
            len = snprintf(buf, sizeof(buf), "%lli", n->data.i);
            tree_serialize_concat(sp, buf, len);
        }
        else if(n->type == TREE_NODE_FLOAT) {
            len = snprintf(buf, sizeof(buf), "%Lf", n->data.f);
            tree_serialize_concat(sp, buf, len);
        }
        else if(n->type == TREE_NODE_STR) {
            if(n->data.s != NULL) {
                tmp = strdup(n->data.s);
                strrepl(&tmp, -1, NULL, 1, &qmap, 0, -1);

                tree_serialize_concat(sp, "\"", 1);
                tree_serialize_concat(sp, tmp, strlen(tmp));
                tree_serialize_concat(sp, "\"", 1);

                free(tmp);
            }
            else {
                tree_serialize_concat(sp, "null", 4);
            }
        }
        else if(n->type == TREE_NODE_PTR) {
            len = snprintf(buf, sizeof(buf), "%p", n->data.ptr);
            tree_serialize_concat(sp, buf, len);
        }

        if(n->next != NULL) {
            tree_serialize_concat(sp, ",", 1);
            if(pretty)
                tree_serialize_concat(sp, "\n", 1);

            n = n->next;
            continue;
        }

        if(pretty)
            tree_serialize_concat(sp, "\n", 1);

        break;
    }
}

inline static
void tree_write_indent(FILE *fh, unsigned int level, char *str) {
    unsigned int i;
    for(i=0; i < level; ++i)
        fprintf(fh, "%s", str);
}

inline static
void tree_write_recurse(struct tree_node *n, bool pretty, char *indent_str, size_t indent_level, FILE *fh) {
    char buf[32];
    size_t len;
    char* tmp;

    for(; n != NULL; n = n->next) {
        if(pretty)
            tree_write_indent(fh, indent_level, indent_str);

        // don't write invalid node types
        // otherwise it would fuck up the results
        if(n->type > TREE_NODE_PTR)
            continue;

        if(n->parent != NULL && n->parent->type == TREE_NODE_DICT)
            fprintf(fh, "\"%s\": ", n->name);

        if(n->type == TREE_NODE_DICT) {
            fprintf(fh, "{");
            if(pretty)
                fprintf(fh, "\n");

            tree_write_recurse(n->child, pretty, indent_str, indent_level+1, fh);
            tree_write_indent(fh, indent_level, indent_str);
            fprintf(fh, "}");
        }
        else if(n->type == TREE_NODE_LIST) {
            fprintf(fh, "[");
            if(pretty)
                fprintf(fh, "\n");

            tree_write_recurse(n->child, pretty, indent_str, indent_level+1, fh);
            tree_write_indent(fh, indent_level, indent_str);
            fprintf(fh, "]");
        }
        else if(n->type == TREE_NODE_BOOL) {
            fprintf(fh, "%s", n->data.b ? "true" : "false");
        }
        else if(n->type == TREE_NODE_INT) {
            fprintf(fh, "%lli", n->data.i);
        }
        else if(n->type == TREE_NODE_FLOAT) {
            len = snprintf(buf, sizeof(buf), "%Lf", n->data.f);

            // don't write trailing 0s
            tmp = strstripr(buf, len, "0", false, &len);

            fwrite(tmp, 1, len, fh);
        }
        else if(n->type == TREE_NODE_STR) {
            if(n->data.s != NULL) {
                tmp = strdup(n->data.s);
                strrepl(&tmp, -1, NULL, 1, &qmap, 0, -1);
                fprintf(fh, "\"%s\"", tmp);
                free(tmp);
            }
            else {
                fprintf(fh, "\"\"");
            }
        }
        else if(n->type == TREE_NODE_PTR) {
            fprintf(fh, "%p", n->data.ptr);
        }

        if(n->next != NULL)
            fprintf(fh, ",");
        if(pretty)
            fprintf(fh, "\n");
    }
}

// ----

int tree_node_new(struct tree_node **n_ret, int type, char *name) {
    int e;
    struct tree_node *n;

    n = malloc(sizeof(struct tree_node));
    if(n == NULL)
        return TREE_MALLOC;

    memset(n, 0, sizeof(struct tree_node));

    n->type = type;
    if(name != NULL) {
        e = tree_node_set_name(n, name);
        if(e != TREE_OK) {
            free(n);
            return e;
        }
    }

    *n_ret = n;

    return TREE_OK;
}

void tree_node_destroy(struct tree_node *n) {
    struct tree_node *c,
                     *cn;

    tree_node_detach(n);

    for(c=n->child; c != NULL; c=cn) {
        cn = c->next;
        tree_node_destroy(c);
    }

    free(n->name);
    if(n->type == TREE_NODE_STR)
        free(n->data.s);

    free(n);
}

void tree_node_attach(struct tree_node *n, struct tree_node *p) {
    if(n->parent != NULL)
        tree_node_detach(n);

    if(p->last_child != NULL) {
        n->prev = p->last_child;
        p->last_child->next = n;
        p->last_child = n;
    }
    else {
        p->child = n;
        p->last_child = n;
    }

    n->parent = p;
}

void tree_node_attach_after(struct tree_node *n, struct tree_node *s) {
    struct tree_node *p;

    if(n->parent != NULL)
        tree_node_detach(n);

    n->prev = s;
    n->next = s->next;
    s->next = n;

    p = s->parent;
    if(p != NULL && s == p->last_child)
        p->last_child = n;

    n->parent = s->parent;
}

void tree_node_attach_before(struct tree_node *n, struct tree_node *s) {
    struct tree_node *p;

    if(n->parent != NULL)
        tree_node_detach(n);

    n->prev = s->prev;
    n->next = s;
    s->prev = n;

    p = s->parent;
    if(p != NULL && s == p->child)
        p->child = n;

    n->parent = p;
}

void tree_node_detach(struct tree_node *n) {
    struct tree_node *p;

    p = n->parent;
    if(p != NULL) {
        if(n == p->child)
            p->child = n->next;
        if(n == p->last_child)
            p->last_child = n->prev;
    }

    if(n->prev != NULL)
        n->prev->next = n->next;
    if(n->next != NULL)
        n->next->prev = n->prev;

    n->parent = NULL;
    n->prev = NULL;
    n->next = NULL;
}

int tree_node_set_name(struct tree_node *n, char *name) {
    char *new_name;

    if(name != NULL) {
        new_name = strdup(name);
        if(new_name == NULL)
            return TREE_MALLOC;
    }
    else {
        new_name = NULL;
    }

    if(n->name != NULL)
        free(n->name);

    n->name = new_name;

    return TREE_OK;
}

int tree_node_set_data(struct tree_node *n, void *data) {
    char *s;

    if(n->type == TREE_NODE_BOOL) {
        n->data.b = *((bool *) data);
    }
    else if(n->type == TREE_NODE_INT) {
        n->data.i = *((long long *) data);
    }
    else if(n->type == TREE_NODE_FLOAT) {
        n->data.f = *((long double *) data);
    }
    else if(n->type == TREE_NODE_STR) {
        if(data != NULL) {
            s = strdup(data);
            if(s == NULL)
                return TREE_MALLOC;
        }
        else {
            s = NULL;
        }

        if(n->data.s != NULL)
            free(n->data.s);

        n->data.s = s;
    }
    else if(n->type == TREE_NODE_PTR) {
        n->data.ptr = data;
    }
    else {
        return TREE_BAD_TYPE;
    }

    return TREE_OK;
}

int tree_descend(struct tree_node *n, char *path, int type, bool touch, struct tree_node **n_ret) {
    int e;
    char *tk,
         *ptr;

    ptr = path;
    tk = ptr;

    while(true) {
        if(*ptr == '.') {
            if(ptr > tk) {
                e = tree_descend_named(n, TREE_NODE_DICT, touch, tk, ptr, &n);
                if(e != TREE_OK)
                    return e;
            }

            tk = ++ptr;
        }
        else if(*ptr == '[') {
            if(ptr > tk) {
                e = tree_descend_named(n, TREE_NODE_LIST, touch, tk, ptr, &n);
                if(e != TREE_OK)
                    return e;
            }

            e = tree_descend_subscript(n, type, touch, ptr, &ptr, &n);
            if(e != TREE_OK)
                return e;

            tk = ptr;
        }
        else if(*ptr == '\0') {
            if(ptr > tk) {
                e = tree_descend_named(n, type, touch, tk, ptr, &n);
                if(e != TREE_OK)
                    return e;
            }

            break;
        }
        else {
            ++ptr;
        }
    }

    if(n->type != type)
        return TREE_BAD_TYPE;

    *n_ret = n;

    return TREE_OK;
}

int tree_set(struct tree_node *n, char *path, int type, void *data) {
    int e;

    e = tree_descend(n, path, type, true, &n);
    if(e != TREE_OK)
        return e;

    e = tree_node_set_data(n, data);
    if(e != TREE_OK)
        return e;

    return TREE_OK;
}

int tree_set_multi(struct tree_node *n, struct tree_node_mapping *map, size_t n_items) {
    int e;
    size_t i;
    struct tree_node_mapping *m;

    for(i=0; i < n_items; ++i) {
        m = &map[i];
        e = tree_set(n, m->path, m->type, m->data_ret);
        if(e != TREE_OK)
            return e;
    }

    return TREE_OK;
}

int tree_get(struct tree_node *n, char *path, int type, void *data_ret) {
    int e;

    e = tree_descend(n, path, type, false, &n);
    if(e != TREE_OK)
        return e;

    if(n->type == TREE_NODE_BOOL)
        *((bool *)data_ret) = n->data.b;
    else if(n->type == TREE_NODE_INT)
        *((long long int *)data_ret) = n->data.i;
    else if(n->type == TREE_NODE_FLOAT)
        *((long double *)data_ret) = n->data.f;
    else if(n->type == TREE_NODE_STR)
        *((char **)data_ret) = n->data.s;
    else if(n->type == TREE_NODE_PTR)
        *((void **)data_ret) = n->data.ptr;
    else
        *((struct tree_node **)data_ret) = n;

    return TREE_OK;
}

int tree_get_multi(struct tree_node *n, struct tree_node_mapping *map, size_t n_items) {
    int e;
    size_t i;
    struct tree_node_mapping *m;

    for(i=0; i < n_items; ++i) {
        m = &map[i];
        e = tree_get(n, m->path, m->type, m->data_ret);
        if(e == TREE_OK)
            m->found = true;
    }

    return TREE_OK;
}

int tree_serialize(struct tree_node *n, bool pretty, char *indent_str, char **str_ret, size_t *len_ret) {
    int e;
    size_t len,
           indent_len;
    char *str,
         *ptr;

    len = 0;
    indent_len = strlen(indent_str);

    e = tree_serialize_calc_len(n, pretty, indent_len, 0, &len);
    if(e != TREE_OK)
        return e;

    str = malloc(len+1);
    if(str == NULL)
        return TREE_MALLOC;

    if(n != NULL) {
        ptr = str;
        tree_serialize_recurse(n, &ptr, pretty, indent_str, indent_len, 0);
    }

    str[len] = '\0';

    *str_ret = str;
    if(len_ret != NULL)
        *len_ret = len;

    return TREE_OK;
}

int tree_deserialize(struct tree_node **root_ret, char *buf, struct tree_error_pos *ep_out) {
    int e,
        state,
        expect;
    size_t i;
    struct tree_node *root,
                     *parent,
                     *node;
    char *ptr,
         *tk,
         *end,
         *name,
         *s;

    enum EXPECT {
        DATA,
        DELIM
    };

    // error handling

    struct tree_error_pos ep = { 1,0 };
    char *line_start = buf;

    // create root node

    ptr = buf;
    while(true) {
        if(*ptr == '\n') {
            ++ep.line;
            line_start = ++ptr;
        }
        else if(isspace(*ptr)) {
            ++ptr;
        }
        else if(*ptr == '{') {
            e = tree_node_new(&root, TREE_NODE_DICT, NULL);
            if(e != TREE_OK)
                goto e0;

            ++ptr;
            break;
        }
        else if(*ptr == '[') {
            e = tree_node_new(&root, TREE_NODE_LIST, NULL);
            if(e != TREE_OK)
                goto e0;

            ++ptr;
            break;
        }
        else {
            e = TREE_EXPECTED_DELIM;
            goto e0;
        }
    }

    state = 0;
    name = NULL;
    parent = root;
    expect = DATA;

    // loop

    while(true) {
        while(true) {
            if(*ptr == '\n') {
                ++ep.line;
                line_start = ++ptr;
            }
            else if(isspace(*ptr)) {
                ++ptr;
            }
            else {
                break;
            }
        }

        if(*ptr == '\0')
            break;

        if(state == 0) {
            // open dictionary
            if(*ptr == '{') {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                e = tree_node_new(&node, TREE_NODE_DICT, name);
                if(e != TREE_OK)
                    goto e1;

                tree_node_attach(node, parent);

                name = NULL;
                parent = node;
                expect = DATA;

                ++ptr;
            }
            // open list
            else if(*ptr == '[') {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                e = tree_node_new(&node, TREE_NODE_LIST, name);
                if(e != TREE_OK)
                    goto e1;

                tree_node_attach(node, parent);

                name = NULL;
                parent = node;
                expect = DATA;

                ++ptr;
            }
            // close dictionary
            else if(*ptr == '}') {
                if(expect != DELIM) {
                    e = TREE_UNEXPECTED_DELIM;
                    goto e1;
                }
                else if(parent->type != TREE_NODE_DICT) {
                    e = TREE_UNCLOSED_LIST;
                    goto e1;
                }

                parent = parent->parent;
                expect = DELIM;

                ++ptr;
            }
            // close list
            else if(*ptr == ']') {
                if(expect != DELIM) {
                    e = TREE_UNEXPECTED_DELIM;
                    goto e1;
                }
                else if(parent->type != TREE_NODE_LIST) {
                    e = TREE_UNCLOSED_DICT;
                    goto e1;
                }

                parent = parent->parent;
                expect = DELIM;

                ++ptr;
            }
            // open string
            else if(*ptr == '\"') {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                state = 1;
                tk = ++ptr;
            }
            // parse booleans
            else if(strncasecmp(ptr, "true", 4) == 0) {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                e = tree_node_new(&node, TREE_NODE_BOOL, name);
                if(e != TREE_OK)
                    goto e1;

                tree_node_attach(node, parent);
                node->data.b = true;

                name = NULL;
                expect = DELIM;

                ptr += 4;
            }
            else if(strncasecmp(ptr, "false", 5) == 0) {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                e = tree_node_new(&node, TREE_NODE_BOOL, name);
                if(e != TREE_OK)
                    goto e1;

                tree_node_attach(node, parent);
                node->data.b = false;

                name = NULL;
                expect = DELIM;

                ptr += 5;
            }
            // parse number
            else if(*ptr == '-') {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                i=1;
                goto n1;
            }
            else if(isdigit(*ptr)) {
                if(expect != DATA) {
                    e = TREE_EXPECTED_DELIM;
                    goto e1;
                }

                i=0;
            n1: while(isdigit(ptr[i]))
                    ++i;

                if(ptr[i] == '.') {
                    e = tree_node_new(&node, TREE_NODE_FLOAT, name);
                    if(e != TREE_OK)
                        goto e1;

                    tree_node_attach(node, parent);

                    errno = 0;
                    node->data.f = strtold(ptr, &end);
                    if(errno != 0) {
                        e = TREE_INVALID_NUMBER;
                        goto e1;
                    }
                }
                else {
                    e = tree_node_new(&node, TREE_NODE_INT, name);
                    if(e != TREE_OK)
                        goto e1;

                    tree_node_attach(node, parent);

                    errno = 0;
                    node->data.i = strtoll(ptr, &end, 0);
                    if(errno != 0) {
                        e = TREE_INVALID_NUMBER;
                        goto e1;
                    }
                }

                name = NULL;
                expect = DELIM;

                ptr = end;
            }
            // parse null
            else if(strncasecmp(ptr, "null", 4) == 0) {
                e = tree_node_new(&node, TREE_NODE_PTR, name);
                if(e != TREE_OK)
                    goto e1;

                tree_node_attach(node, parent);
                node->data.ptr = NULL;

                name = NULL;
                parent = node;
                expect = DELIM;

                ptr += 4;
            }
            // check for comma
            else if(*ptr == ',') { 
                if(expect != DELIM) {
                    e = TREE_UNEXPECTED_DELIM;
                    goto e1;
                }

                ++ptr;
                expect = DATA;
            }
            // open comment
            else if(strncmp(&*ptr, "/*", 2) == 0) {
                state = 2;
                ptr += 2;
            }
            // open line comment
            else if(strncmp(&*ptr, "//", 2) == 0) {
                state = 3;
                ptr += 2;
            }
            // invalid character
            else {
                e = TREE_UNEXPECTED_CHAR;
                goto e1;
            }
        }
        // consume string
        else if(state == 1) {
            if(*ptr == '\\' && *(ptr+1) != '\0') {
                // skip escaped character
                // if it's a newline, handle it anyway
                if(*(ptr+1) =='\n') {
                    ptr += 2;
                    ++ep.line;
                    line_start = ptr;
                }
                else {
                    ptr += 2;
                }
            }
            else if(*ptr == '\"') {
                // configure next node name
                if(parent->type == TREE_NODE_DICT && name == NULL) {
                    name = strndup(tk, ptr-tk);
                    strrepl(&name, -1, NULL, 1, &rqmap, 0, -1);

                    ++ptr; // skip closing quote

                    if(*ptr != ':') {
                        e = TREE_EXPECTED_COLON;
                        goto e1;
                    }

                    ++ptr; // skip colon

                    // skip any whitespace
                    while(true) {
                        if(*ptr == '\n') {
                            ++ep.line;
                            line_start = ++ptr;
                        }
                        else if(isspace(*ptr)) {
                            ++ptr;
                        }
                        else {
                            break;
                        }
                    }
                }
                // set string node data
                else {
                    e = tree_node_new(&node, TREE_NODE_STR, name);
                    if(e != TREE_OK)
                        goto e1;

                    tree_node_attach(node, parent);

                    s = strndup(tk, ptr-tk);
                    if(s == NULL) {
                        e = TREE_MALLOC;
                        goto e1;
                    }

                    strrepl(&s, -1, NULL, 1, &rqmap, 0, -1);
                    node->data.s = s;

                    name = NULL;
                    expect = DELIM;

                    ++ptr; // skip closing quote
                }

                state = 0;
            }
            else {
                ++ptr;
            }
        }
        // consume comment
        else if(state == 2) {
            if(strncmp(ptr, "*/", 2) == 0) {
                state = 0;
                ptr += 2;
            }
            else {
                ++ptr;
            }
        }
        // consume line comment
        else if(state == 3) {
            if(strncmp(ptr, "\n", 1) == 0) {
                state = 0;
                ++ep.line;
                line_start = ++ptr;
            }
            else {
                ++ptr;
            }
        }
    }

    if(state == 1) {
        e = TREE_UNCLOSED_QUOTE;
        goto e1;
    }
    else if(state == 2) {
        e = TREE_UNCLOSED_COMMENT;
        goto e1;
    }
    else if(parent != NULL) {
        if(parent->type == TREE_NODE_DICT)
            e = TREE_UNCLOSED_DICT;
        else // if(parent->type == TREE_NODE_LIST)
            e = TREE_UNCLOSED_LIST;

        goto e1;
    }

    *root_ret = root;

    return TREE_OK;

e1: if(root != NULL)
        tree_node_destroy(root);

e0: if(ep_out != NULL) {
        ep.offset = ptr-line_start;
        memcpy(ep_out, &ep, sizeof(ep));
    }

    return e;
}

int tree_read(struct tree_node **root_ret, FILE *fh, struct tree_error_pos *ep_out) {
    int e;
    size_t len;
    char *buf;
    ssize_t nr;

    fseek(fh, 0L, SEEK_END);
    len = ftell(fh);
    buf = malloc(len+1);
    buf[len] = '\0';

    nr = fread(buf, 1, len, fh);
    if(nr < len)
        return -1;

    e = tree_deserialize(root_ret, buf, ep_out);
    free(buf);

    if(e != TREE_OK)
        return e;

    return 0;
}

void tree_write(struct tree_node *n, bool pretty, char *indent_str, FILE *fh) {
    tree_write_recurse(n, pretty, indent_str, 0, fh);
}

int tree_qsort(struct tree_node *n, tree_qsort_func *qsort_func) {
    size_t count,
           i;
    struct tree_node **buf,
                     *c;

    // count nodes
    count=0;
    for(c=n->child; c != NULL; c=c->next)
        ++count;

    // allocate buffer
    buf = malloc(sizeof(struct tree_node *) * count);
    if(buf == NULL)
        return TREE_MALLOC;

    // populate buffer and detach nodes
    for(i=0,c=n->child; c != NULL; ++i,c=c->next)
        buf[i] = c;

    // sort and reattach nodes
    qsort(buf, count, sizeof(struct tree_node *), qsort_func);
    for(i=0; i < count; ++i)
        tree_node_attach(buf[i], n);

    free(buf);

    return TREE_OK;
}
