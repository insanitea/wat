#include "bin.h"

size_t bingap(uintmax_t x) {
    size_t g,
           max;

    // shift until first toggled bit
    for(; x != 0; x >>= 1) {
        if(x & 1) {
            x >>= 1;
            break;
        }
    }

    for(g=0,max=0; x != 0; x >>= 1) {
        if((x & 1) && g > 0) {
            if(g > max)
                max = g;
            else
                g = 0;
        }
        else {
            ++g;
        }
    }

    return max;
}

size_t popcnt(uintmax_t x) {
    size_t c;

    for(c=0; x != 0; x >>= 1) {
        if(x & 1)
            ++c;
    }

    return c;
}
