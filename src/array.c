#include "array.h"

#include <stdlib.h>
#include <string.h>

int _splice(void *array, size_t item_size,
            size_t size, size_t alloc_size, size_t pool_size,
            size_t pos, size_t nr, size_t ni, void *items,
            void **array_ret, size_t *size_ret, size_t *alloc_size_ret)
{
    size_t diff,
           new_size,
           new_alloc_size,
           src,
           dest;
    void *tmp;

    // reallocate
    if(nr > ni) {
        diff = nr-ni;
        new_size = size-diff;
    }
    else if(ni > nr) {
        diff = ni-nr;
        new_size = size+diff;

        if(alloc_size == -1 || new_size > alloc_size) {
            new_alloc_size = (new_size/pool_size)*pool_size;
            if(new_size % pool_size)
                new_alloc_size += pool_size;

            tmp = realloc(array, item_size*new_alloc_size);
            if(tmp == NULL)
                return -1;

            array = tmp;
            alloc_size = new_alloc_size;
        }
    }
    else {
        goto c1;
    }

    // apply offset
    src = pos+nr;
    dest = pos+ni;
    memmove(array+(item_size*dest), array+(item_size*src), item_size*(size-src));

    // insert
    size = new_size;
c1: memcpy(array+(item_size*pos), items, item_size*ni);

    if(array_ret != NULL)
        *array_ret = array;
    if(size_ret != NULL)
        *size_ret = size;
    if(alloc_size_ret != NULL)
        *alloc_size_ret = alloc_size;

    return 0;
}
