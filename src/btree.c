#include "btree.h"

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
    #define debug(...) fprintf(stderr, __VA_ARGS__)
#else
    #define debug(...)
#endif

//
// nodes
//

struct btree_node * node_create(char *key, void *data, btree_deref_cb *deref,
                                struct btree_node *parent, struct btree_node **link)
{
    struct btree_node *x;

    x = malloc(sizeof(struct btree_node));
    if(x == NULL)
        goto e0;

    memset(x, 0, sizeof(struct btree_node));

    x->key = strdup(key);
    if(x->key == NULL)
        goto e1;

    x->height = 1;
    x->data = data;
    x->deref = deref;
    x->size = 1;
    x->left = NULL;
    x->right = NULL;
    x->parent = parent;
    x->link = link;

    *link = x;

    return x;

e1: free(x->key);
e0: return NULL;
}

void node_destroy(struct btree_node *x) {
    free(x->key);
    if(x->deref != NULL && x->data != NULL)
        x->deref(x->data);

    free(x);
}

//
// removal
//

void update_height(struct btree_node *x) {
    size_t lh,rh;

    if(x->left != NULL)
        lh = x->left->height;
    else
        lh = 0;

    if(x->right != NULL)
        rh = x->right->height;
    else
        rh = 0;

    x->height = (lh >= rh ? lh:rh) + 1;
}

int merge(struct btree_node *c, struct btree_node *p, struct btree_node **link) {
    struct btree_node *x,*t;
    int r;

    x = *link;
    t = p->parent;

    while(x != NULL) {
        r = strcmp(c->key, x->key);
        if(r < 0) {
            p = x;
            link = &x->left;
            x = x->left;
        }
        else if(r > 0) {
            p = x;
            link = &x->right;
            x = x->right;
        }
        else {
            return BTREE_CORRUPT;
        }
    }

    *c->link = NULL;
    c->parent = p;
    c->link = link;
    *link = c;

    for(; p != t; p=p->parent) {
        update_height(p);
        p->size += c->size;
    }

    return BTREE_OK;
}

int pop(struct btree_node **link) {
    int e;
    struct btree_node *x,*p;
    size_t z;

    x = *link;

    if(x->left != NULL && x->right != NULL) {
        z = btree_get_balance(x);
        if(z >= 0)
            goto merge_left;
        else
            goto merge_right;
    }

    if(x->left != NULL) {
    merge_left:
        if(x->right != NULL) {
            // merge x->right into x->left
            e = merge(x->right, x->left, &x->left->right);
            if(e != BTREE_OK)
                return e;
        }

        // replace x with x->left
        *link = x->left;
        x->left->parent = x->parent;
        x->left->link = x->link;
        x->left = NULL;
    }
    else if(x->right != NULL) {
    merge_right:
        if(x->left != NULL) {
            // merge x->left into x->right
            e = merge(x->left, x->right, &x->right->left);
            if(e != BTREE_OK)
                return e;
        }

        // replace x with x->right
        *link = x->right;
        x->right->parent = x->parent;
        x->right->link = x->link;
        x->right = NULL;
    }
    else {
        *link = NULL;
    }

    for(p=x->parent; p != NULL; p=p->parent) {
        update_height(p);
        --p->size;
    }

    node_destroy(x);

    return BTREE_OK;
}

struct btree_node * sibling(struct btree_node *x) {
    struct btree_node *p;

    if(x->parent == NULL)
        return NULL;

    p = x->parent;
    if(x == p->left)
        return p->right;
    else // if(x == p->right)
        return p->left;
}

//
// rebuilding
//

void presort(struct btree_node *x, struct btree_node **buf, size_t *count_ptr) {
    if(x->left != NULL)
        presort(x->left, buf, count_ptr);

    buf[(*count_ptr)++] = x;

    if(x->right != NULL)
        presort(x->right, buf, count_ptr);
}

void rebuild(struct btree_node **link, struct btree_node *parent,
             struct btree_node **buf, size_t left, size_t right,
             size_t size, size_t height)
{
    size_t middle,
           norm_size,
           left_size,
           right_size;
    struct btree_node *x;

    middle = (left+right)/2;
    x = buf[middle];

    x->height = height;
    x->size = size;
    x->link = link;
    x->parent = parent;

    norm_size = size-1;
    left_size = norm_size/2;
    right_size = norm_size-left_size;

    if(left_size > 0)
        rebuild(&x->left, x, buf, left, middle-1, left_size, height-1);
    else
        x->left = NULL;

    if(right_size > 0)
        rebuild(&x->right, x, buf, middle+1, right, right_size, height-1);
    else
        x->right = NULL;

    *link = x;
}

// rotation

bool rotate(struct btree_node *x, struct btree_node **top_ret) {
    struct btree_node *p,*g,*a,*b;

    p = x->parent;
    if(p == NULL)
        return false;

    g = p->parent;
    if(g == NULL)
        return false;

    if(p->right == x && p->left == NULL && g->right == p && g->left == NULL) {
        debug("matched XLL @ %s\n", x->key);

        // before:
        //
        //   g
        //  / ╲
        // #   p
        //    / ╲
        //   #   x
        //
        // after:
        //
        //   p   
        //  ╱ ╲
        // g   x

        *g->link = p;

        p->left = g;
        p->parent = g->parent;
        p->link = g->link;

        g->right = NULL;
        g->parent = p;
        g->link = &p->left;

        update_height(g);
        update_height(p);

        *top_ret = p;

        return true;
    }
    else if(p->left == x && p->right == NULL && g->left == p && g->right == NULL) {
        debug("matched XRR @ %s\n", x->key);

        // before:
        //
        //     g
        //    ╱ ╲
        //   p   #
        //  ╱ ╲
        // x   #
        //
        // after:
        //
        //   p   
        //  ╱ ╲
        // x   g

        *g->link = p;

        p->right = g;
        p->parent = g->parent;
        p->link = g->link;

        g->left = NULL;
        g->parent = p;
        g->link = &p->right;

        update_height(g);
        update_height(p);

        *top_ret = p;

        return true;
    }
    else if(p->left == NULL && p->right == x && g->left == p && g->right == NULL) {
        debug("matched XLR @ %s\n", x->key);

        // before:
        //
        //     g
        //    ╱ ╲
        //   p   #
        //  ╱ ╲
        // #   x
        //    ╱ ╲
        //   a   b
        //
        // after:
        //
        //      x   
        //    ╱   ╲
        //   p     g
        //  ╱ ╲   ╱ ╲
        // #   a b   #

        *g->link = x;

        a = x->left;
        b = x->right;

        x->left = p;
        x->right = g;
        x->parent = g->parent;
        x->link = g->link;

        p->right = a;
        p->parent = x;
        p->link = &x->left;

        if(a != NULL) {
            a->parent = p;
            a->link = &p->right;
        }

        g->left = b;
        g->parent = x;
        g->link = &x->right;

        if(b != NULL) {
            b->parent = g;
            b->link = &g->left;
        }

        update_height(g);
        update_height(p);
        update_height(x);

        *top_ret = x;
    }
    else if(p->left == x && p->right == NULL && g->left == NULL && g->right == p) {
        debug("matched XRL @ %s\n", x->key);

        // before:
        //
        //   g
        //  ╱ ╲
        // #   p
        //    ╱ ╲
        //   x   #
        //  ╱ ╲
        // a   b
        //
        // after:
        //
        //      x
        //    ╱   ╲
        //   g     p
        //  ╱ ╲   ╱ ╲
        // #   a b   #

        *g->link = x;

        a = x->left;
        b = x->right;

        x->left = g;
        x->right = p;
        x->parent = g->parent;
        x->link = g->link;

        g->right = a;
        g->parent = x;
        g->link = &x->left;

        if(a != NULL) {
            a->parent = g;
            a->link = &g->right;
        }

        p->left = b;
        p->parent = x;
        p->link = &x->right;

        if(b != NULL) {
            b->parent = p;
            b->link = &p->left;
        }

        update_height(g);
        update_height(p);
        update_height(x);

        *top_ret = x;

        return true;
    }

    return false;
}

//
// general
//

size_t btree_get_height(struct btree_node *root) {
    if(root == NULL)
        return 0;

    return root->height;
}

size_t btree_get_size(struct btree_node *root) {
    if(root == NULL)
        return 0;

    return root->size;
}

ssize_t btree_get_balance(struct btree_node *root) {
    size_t lh,
           rh;

    if(root != NULL) {
        lh = root->left != NULL ? root->left->height : 0;
        rh = root->right != NULL ? root->right->height : 0;
    }
    else {
        lh = 0;
        rh = 0;
    }

    if(lh > rh)
        return (lh-rh) * -1;
    else if(rh > lh)
        return rh-lh;
    else
        return 0;
}

void btree_destroy(struct btree_node **link) {
    if(*link == NULL)
        return;

    node_destroy(*link);
    *link = NULL;
}

int btree_insert(struct btree_node **link, char *key, void *data, btree_deref_cb *deref) {
    int e,r;
    struct btree_node *x,*p;

    x = *link;
    p = NULL;

    while(x != NULL) {
        r = strcmp(key, x->key);
        if(r < 0) {
            p = x;
            link = &x->left;
            x = x->left;
        }
        else if(r > 0) {
            p = x;
            link = &x->right;
            x = x->right;
        }
        else {
            break;
        }
    }

    if(x == NULL) {
        x = node_create(key, data, deref, p, link);
        if(x == NULL)
            return BTREE_MALLOC;

        rotate(x,&p);
        for(; p != NULL; p=p->parent) {
            update_height(p);
            ++p->size;
        }

        e = BTREE_OK;
    }
    else {
        if(x->deref != NULL && x->data != NULL)
            x->deref(x->data);

        x->data = data;
        x->deref = deref;

        e = BTREE_REPLACE;
    }

    return e;
}

int btree_remove(struct btree_node **link, char *key) {
    struct btree_node *x;
    int r;

    x = *link;

    while(x != NULL) {
        r = strcmp(key, x->key);
        if(r < 0) {
            link = &x->left;
            x = x->left;
        }
        else if(r > 0) {
            link = &x->right;
            x = x->right;
        }
        else {
            break;
        }
    }

    if(x == NULL)
        return BTREE_NOT_FOUND;

    return pop(link);
}

int btree_get(struct btree_node *root, char *key, void **data_ret) {
    struct btree_node *x;
    int r;

    x = root;
    while(x != NULL) {
        r = strcmp(key, x->key);
        if(r < 0)
            x = x->left;
        else if(r > 0)
            x = x->right;
        else
            return BTREE_CORRUPT;
    }

    if(x == NULL) {
        if(data_ret != NULL)
            *data_ret = NULL;

        return BTREE_NOT_FOUND;
    }

    if(data_ret != NULL)
        *data_ret = x->data;

    return BTREE_OK;
}

int btree_iter(struct btree_node **link, btree_iter_cb *callback, void *arg) {
    int r,e;
    struct btree_node *x;

    x = *link;
    if(x == NULL)
        return BTREE_OK;

    if(x->left != NULL) {
        r = btree_iter(&x->left, callback, arg);
        if(r & BTREE_REMOVE) {
            e = pop(&x->left);
            if(e != BTREE_OK)
                return e;
        }

        if(r & BTREE_BREAK)
            return BTREE_OK;
    }

    r = callback(x->key, x->data, arg);
    if(r & BTREE_REMOVE) {
        e = pop(link);
        if(e != BTREE_OK)
            return e;
    }

    if(r & BTREE_BREAK)
        return BTREE_OK;

    if(x->right != NULL) {
        r = btree_iter(&x->right, callback, arg);
        if(r & BTREE_REMOVE) {
            e = pop(&x->right);
            if(e != BTREE_OK)
                return e;
        }

        if(r & BTREE_BREAK)
            return BTREE_OK;
    }

    return BTREE_OK;
}

void btree_fprint(FILE *fh, struct btree_node *x, btree_iter_cb *callback) {
    static uint64_t vmap[1024]; // max depth: 65536 (64 KB)
    static uint8_t cmap[6] = { 1,3,2,4,6,5 };
    static size_t depth = 0;

    unsigned int c;
    size_t i;

#define vmap_set(i)    (vmap[i/sizeof(vmap[0])] |=  (1<<(i%sizeof(vmap[0]))))
#define vmap_unset(i)  (vmap[i/sizeof(vmap[0])] &= ~(1<<(i%sizeof(vmap[0]))))
#define vmap_check(i) ((vmap[i/sizeof(vmap[0])] &   (1<<(i%sizeof(vmap[0])))) > 0)

    c = depth%(sizeof(cmap)/sizeof(cmap[0]));

    if(x != NULL) {
        if(callback != NULL) {
            fprintf(fh, "\e[0;97m");
            callback(x->key, x->data, fh);
            fprintf(fh, "\e[0m\n");
        }
        else {
            fprintf(fh, "\e[0;9%im%s\e[0m [s=\e[0;97m%zu\e[0m h=\e[0;97m%zu\e[0m]\n", cmap[c], x->key, x->size, x->height);
        }

        if(x->left != NULL || x->right != NULL) {
            ++depth;

            vmap_set(depth);
            for(i=0; i < depth; ++i)
                fprintf(fh, "%s ", vmap_check(i) ? "\e[0;37m│\e[0m" : " ");

            fprintf(fh, "\e[0;37m├╴\e[0m");
            btree_fprint(fh, x->left, callback);

            vmap_unset(depth);
            for(i=0; i < depth; ++i)
                fprintf(fh, "%s ", vmap_check(i) ? "\e[0;37m│\e[0m" : " ");

            fprintf(fh, "\e[0;37m└╴\e[0m");
            btree_fprint(fh, x->right, callback);

            --depth;
        }
    }
    else {
        fprintf(fh, "\e[3%im(null)\e[0m\n", cmap[c]);
    }
}

int btree_rebuild(struct btree_node **link) {
    struct btree_node *root,
                      **buf;
    size_t n;

    root = *link;
    if(root == NULL)
        return BTREE_OK;

    n = root->size;
    buf = malloc(sizeof(struct btree_node *) * n);
    if(buf == NULL)
        return BTREE_MALLOC;

    n=0;
    presort(root, buf, &n);
    rebuild(link, NULL, buf, 0, n-1, n, log2(n)+1);
    free(buf);

    return BTREE_OK;
}
