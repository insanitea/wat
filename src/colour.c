#include "colour.h"

#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int extract_hex(double *vals, char *str) {
    char *ptr;
    size_t len,np,i;
    uint8_t a,b,incr;
    char vstr[3];

    ptr = ++str;
    len = strlen(ptr);

    if(len == 3 || len == 4) {
        a = 0;
        b = 0;
        incr = 1;
    }
    else if(len == 6 || len == 8) {
        a = 0;
        b = 1;
        incr = 2;
    }
    else {
        return 1;
    }

    np = len/2;
    vstr[2] = '\0';

    for(i=0; i < np; ++i) {
        vstr[0] = str[a];
        vstr[1] = str[b];
        vals[i] = (double) strtoul(vstr, NULL, 16) / UINT8_MAX;
        ptr += incr;
    }

    return 0;
}

int extract_float(double *vals, char *ptr) {
    *vals++ = strtod(ptr, &ptr);
    if(*ptr != ',')
        return 1;

    ++ptr;
    *vals++ = strtod(ptr, &ptr);
    if(*ptr != ',')
        return 1;

    ++ptr;
    *vals++ = strtod(ptr, &ptr);
    if(*ptr != ',')
        return 1;

    ++ptr;
    *vals = strtod(ptr, &ptr);
    if(*ptr != ',')
        return 1;

    return 0;
}

inline static
void mix_channels(double *dest, double *a, double *b, double m1, double m2, double m3, double m4) {
    double m[4] = { m1,m2,m3,m4 };

    int i;
    for(i=0; i < 4; ++i) {
        if(a[i] > b[i])
            dest[i] = a[i] - ((a[i] - b[i]) * m[i]);
        else if(b[i] > a[i])
            dest[i] = a[i] + ((b[i] - a[i]) * m[i]);
        else // a[i] == b[i]
            dest[i] = a[i];
    }
}

// RGBA

int rgba_from_str(struct rgba *c, char *str) {
    struct hsva g;
    int e;

    if(str == NULL)
        return -1;

    if(strncmp(str, "#", 1) == 0) {
        return extract_hex(&c->r, str+1);
    }
    else if(strncmp(str, "rgba(", 5) == 0) {
        return extract_float(&c->r, str+5);
    }
    else if(strncmp(str, "hsva(", 5) == 0) {
        if(extract_float(&g.h, str+5) != 0)
            return 1;

        hsva_to_rgba(&g,c);
        return 0;
    }
    else {
        return 1;
    }

    return e;
}

void rgba_desat_avg(struct rgba *dest, struct rgba *src) {
    double avg = (src->r + src->g + src->b) / 3;

    dest->r = avg;
    dest->g = avg;
    dest->b = avg;
    dest->a = src->a;
}

void rgba_mix(struct rgba *dest, struct rgba *a, struct rgba *b, double m) {
    mix_channels(&dest->r, &a->r, &b->r, m, m, m, m);
}

void rgba_mix_ch(struct rgba *dest, struct rgba *a, struct rgba *b, double rm, double gm, double bm, double am) {
    mix_channels(&dest->r, &a->r, &b->r, rm,gm,bm,am);
}

void rgba_shade(struct rgba *c, double v) {
    struct hsva g;
    rgba_to_hsva(c,&g);
    hsva_shade(&g,v);
    hsva_to_rgba(&g,c);
}

void rgba_to_hsva(struct rgba *in, struct hsva *out) {
    double max,
           min;

    if(in->r > in->g) {
        if(in->r > in->b)
            max = in->r;
        else
            max = in->b;
    }
    else {
        if(in->g > in->b)
            max = in->g;
        else
            max = in->b;
    }

    if(in->r < in->g) {
        if(in->r < in->b)
            min = in->r;
        else
            min = in->b;
    }
    else {
        if(in->g < in->b)
            min = in->g;
        else
            min = in->b;
    }

    if(max == 0) {
        out->h = 0;
        out->s = 0;
        out->v = 0;
        return;
    }

    out->v = max;

    if(max == min) {
        out->h = 0;
        out->s = 0;
        return;
    }

    out->s = max - min;
    if(max == in->r) {
        out->h = 60.0 * (in->g - in->b);
        if(out->h < 0)
            out->h += 360;
    }
    else if(max == in->g) {
        out->h = 20.0 + (60.0 * (in->b - in->r));
    }
    else { // max == in->b
        out->h = 240.0 + (60.0 * (in->r - in->g));
    }
}

void rgba_to_hex(struct rgba *c, char *dest, bool alpha) {
    struct rgba_values v;
    char vstr[3];

    *dest++ = '#';
    rgba_get_values(c, &v, RGBA_8);

    snprintf(vstr, sizeof(vstr), "%02llX", v.r);
    memcpy(dest, vstr, 2);
    dest += 2;

    snprintf(vstr, sizeof(vstr), "%02llX", v.g);
    memcpy(dest, vstr, 2);
    dest += 2;

    snprintf(vstr, sizeof(vstr), "%02llX", v.b);
    memcpy(dest, vstr, 2);
    dest += 2;

    if(alpha) {
        snprintf(vstr, sizeof(vstr), "%02llX", v.a);
        memcpy(dest, vstr, 2);
        dest += 2;
    }

    *dest = '\0';
}

uint32_t rgba_to_uint32(struct rgba *c) {
    struct rgba_values v;
    rgba_get_values(c, &v, RGBA_8);
    return (v.a << 24) | (v.r << 16) | (v.g << 8) | (v.b << 0);
}

void rgba_get_values(struct rgba *c, struct rgba_values *v, int prec) {
    unsigned long long max;

    if(prec == RGBA_8)
        max = UINT8_MAX;
    else if(prec == RGBA_16)
        max = UINT16_MAX;
    else if(prec == RGBA_32)
        max = UINT32_MAX;
    else // prec == RGBA_64
        max = UINT64_MAX;

    v->r = c->r * max;
    v->g = c->g * max;
    v->b = c->b * max;
    v->a = c->a * max;
}

// HSVA

int hsva_from_str(struct hsva *c, char *str) {
    int e;
    double *vals;
    struct rgba tmp;

    *c = (struct hsva) { 0,0,0,1 };
    if(str == NULL)
        return 1;

    if(strncmp(str, "#", 1) == 0) {
        vals = &tmp.r;
        e = extract_hex(vals, str);
        rgba_to_hsva(&tmp, c);
        return e;
    }
    else if(strncmp(str, "rgba(", 5) == 0) {
        vals = &tmp.r;
        extract_float(vals, str);
        rgba_to_hsva(&tmp, c);
        return 0;
    }
    else if(strncmp(str, "hsva(", 5) == 0) {
        vals = &c->h;
        extract_float(vals, str);
        return 0;
    }

    return 1;
}

void hsva_mix(struct hsva *dest, struct hsva *a, struct hsva *b, double m) {
    mix_channels(&dest->h, &a->h, &b->h, m, m, m, m);
}

void hsva_mix_ch(struct hsva *dest, struct hsva *a, struct hsva *b, double hm, double sm, double vm, double am) {
    mix_channels(&dest->h, &a->h, &b->h, hm,sm,vm,am);
}

void hsva_shade(struct hsva *c, double v) {
    c->v += v;
    if(c->v < 0)
        c->v = 0;
    else if(c->v > 1)
        c->v = 1;
}

void hsva_to_rgba(struct hsva *in, struct rgba *out) {
    if(in->s == 0) {
        *out = (struct rgba) { in->v, in->v, in->v };
        return;
    }

    double h = in->h * 6,
           i = floor(h),
           f = h-i,
           p = in->v * (1.0 - in->s),
           q = in->v * (1.0 - (in->s * f)),
           t = in->v * (1.0 - (in->s * (1.0 - f)));

         if(i == 0) *out = (struct rgba) { in->v, t, p, in->a };
    else if(i == 1) *out = (struct rgba) { q, in->v, p, in->a };
    else if(i == 2) *out = (struct rgba) { p, in->v, t, in->a };
    else if(i == 3) *out = (struct rgba) { p, q, in->v, in->a };
    else if(i == 4) *out = (struct rgba) { t, p, in->v, in->a };
    else /*i == 5*/ *out = (struct rgba) { in->v, p, q, in->a };
}
