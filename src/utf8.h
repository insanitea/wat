#ifndef CLIP_UTF8_H
#define CLIP_UTF8_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>

#define UTF8_C1 0x80 // x0000000
#define UTF8_C2 0xC0 // xx000000
#define UTF8_C3 0xE0 // xxx00000
#define UTF8_C4 0xF0 // xxxx0000
#define UTF8_C5 0xF8 // xxxxx000
#define UTF8_C6 0xFC // xxxxxx00
#define UTF8_C7 0xFE // xxxxxxx0

#define UTF8_M1 0x7F // 0xxxxxxx
#define UTF8_M2 0x3F // 00xxxxxx
#define UTF8_M3 0x1F // 000xxxxx
#define UTF8_M4 0x0F // 0000xxxx
#define UTF8_M5 0x07 // 00000xxx
#define UTF8_M6 0x03 // 000000xx
#define UTF8_M7 0x01 // 0000000x

enum utf8_error {
    UTF8_OK,
    UTF8_MALLOC,
    UTF8_INVALID,
    UTF8_RANGE
};

int utf8_byte_count(uint32_t code, size_t *byte_count_ret);
int utf8_encode_char(uint32_t code, char *ptr, size_t *byte_count_ret);
int utf8_decode_char(char *ptr, uint32_t *code_ret, size_t *byte_count_ret);
size_t utf8_strlen(char *str, size_t *byte_len_ret);
int utf8_validate(char *str, size_t *len_ret, size_t *byte_len_ret);
size_t utf8_normalize(char *str, char repl);
int utf8_to_utf32(char *str, uint32_t **buf_ret, size_t *len_ret, size_t *byte_len_ret);
int utf8_offset_to_byte(char *str, size_t offset, size_t *byte_ret);
int utf8_byte_to_offset(char *str, size_t byte, size_t *offset_set);
int utf8_strsimil(char *a, char *b, double *result_ret);

size_t utf32_strlen(uint32_t *str32);
double utf32_strsimil(uint32_t *a, ssize_t a_len, uint32_t *b, ssize_t b_len);
int utf32_to_utf8(uint32_t *str32, ssize_t len, char **str_ret, size_t *byte_len_ret);

#endif
