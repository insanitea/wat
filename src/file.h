#ifndef WAT_FILE_H
#define WAT_FILE_H

#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>

#define WAT_FILE_FREADALL_POOL 8192
#define WAT_FILE_FREADLINE_POOL 128

int    fsanitizepath(char *path, char **path_ret);
char * fjoinpath(char *a, char *b);
int    freadall(void *data_ret, size_t *len_ret, FILE *fh);
int    freadall_pooled(void *data_ret, size_t *len_ret, size_t *alloc_len_ret, FILE *fh);
int    freadline(char **data_ret, size_t *len_ret, size_t *alloc_len_ret, FILE *fh);

#endif
