#include "list.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_POOL_SIZE 64

int list_init(struct list *l, ssize_t alloc_size, size_t pool_size) {
    l->items = NULL;
    l->used_size = 0;
    l->alloc_size = 0;
    l->pool_size = pool_size > 0 ? pool_size : DEFAULT_POOL_SIZE;

    if(alloc_size > 0)
        return list_realloc(l, alloc_size);

    return LIST_OK;
}

int list_realloc(struct list *l, size_t size) {
    size_t i;
    struct list_item *p,
                     *tmp;

    if(size < l->used_size) {
        for(i=size; i < l->used_size; ++i) {
            p = &l->items[i];
            if(p->deref != NULL)
                p->deref(p->ptr);
        }

        l->used_size = size;
    }

    if(size == 0) {
        free(l->items);

        l->items = NULL;
        l->alloc_size = 0;
    }
    else {
        tmp = realloc(l->items, sizeof(struct list_item) * size);
        if(tmp == NULL)
            return LIST_MALLOC;

        l->items = tmp;
        l->alloc_size = size;
    }

    return LIST_OK;
}

int list_grow(struct list *l, size_t size) {
    if(size <= l->used_size)
        return LIST_INVALID;

    return list_realloc(l, size);
}

void list_clear(struct list *l, bool dealloc) {
    size_t i;
    struct list_item *p;

    for(i = 0; i < l->used_size; ++i) {
        p = &l->items[i];
        if(p->deref != NULL)
            p->deref(p->ptr);

        p->ptr = NULL;
        p->deref = NULL;
    }

    if(dealloc)
        list_realloc(l,0);
    else
        l->used_size = 0;
}

void list_size(struct list *l, size_t *used_ret, size_t *alloc_ret) {
    if(used_ret != NULL)
        *used_ret = l->used_size;
    if(alloc_ret != NULL)
        *alloc_ret = l->alloc_size;
}

// ----

int list_find(struct list *l, void *item, size_t *pos_ret) {
    size_t i;

    for(i=0; i < l->used_size; ++i) {
        if(l->items[i].ptr == item) {
            if(pos_ret != NULL)
                *pos_ret = i;

            return LIST_OK;
        }
    }

    return LIST_NOT_FOUND;
}

int list_get(struct list *l, size_t pos, void **item_ret) {
    if(pos >= l->used_size)
        return LIST_INVALID;

    *item_ret = l->items[pos].ptr;

    return LIST_OK;
}

// ----

int list_insert(struct list *l, size_t pos, void *item, list_deref_func *deref) {
    int e;
    struct list_item *p;

    if(pos > l->used_size)
        return LIST_INVALID;

    if(l->used_size == l->alloc_size) {
        e = list_realloc(l, l->alloc_size+l->pool_size);
        if(e != 0)
            return e;
    }

    memmove(l->items+pos+1, l->items+pos, l->used_size-pos);

    p = &l->items[pos];
    p->ptr = item;
    p->deref = deref;

    ++l->used_size;

    return LIST_OK;
}

int list_set(struct list *l, size_t pos, void *item, list_deref_func *deref) {
    struct list_item *p;

    if(pos >= l->used_size)
        return LIST_INVALID;

    p = &l->items[pos];
    if(p->deref != NULL)
        p->deref(p->ptr);

    p->ptr = item;
    p->deref = deref;

    return LIST_OK;
}

int list_remove(struct list *l, void *item) {
    int e;
    size_t pos;

    e = list_find(l, item, &pos);
    if(e != LIST_OK)
        return e;

    return list_splice(l,pos,1,0,NULL,NULL);
}

int list_splice(struct list *l,
                size_t pos, size_t nr,
                size_t ni, void **items, list_deref_func **derefs)
{
    struct list_item *p;
    size_t i,
           diff,
           new_size,
           new_alloc_size,
           src,dest; // shrink gap range
    void *tmp;

    if(pos+nr > l->used_size)
        return LIST_INVALID;

    // deref
    for(i=0; i < nr; ++i) {
        p = &l->items[pos+i];
        if(p->deref != NULL)
            p->deref(p->ptr);
    }

    // reallocate
    if(nr > ni) {
        diff = nr-ni;
        new_size = l->used_size-diff;
    }
    else if(ni > nr) {
        diff = ni-nr;
        new_size = l->used_size+diff;

        if(new_size > l->alloc_size) {
            new_alloc_size = (new_size/l->pool_size)*l->pool_size;
            if(new_size % l->pool_size)
                new_alloc_size += l->pool_size;

            tmp = realloc(l->items, sizeof(struct list_item)*new_alloc_size);
            if(tmp == NULL)
                return -1;

            l->items = tmp;
            l->alloc_size = new_alloc_size;
        }
    }
    else {
        goto c1;
    }

    src = pos+nr;
    dest = pos+ni;
    memmove(l->items+dest, l->items+src, sizeof(struct list_item)*(l->used_size-src));

    l->used_size = new_size;

    // insert
c1: if(derefs != NULL) {
        for(i=0; i < ni; ++i) {
            p = &l->items[pos+i];
            p->ptr = items[i];
            p->deref = derefs[i];
        }
    }
    else {
        for(i=0; i < ni; ++i) {
            p = &l->items[pos+i];
            p->ptr = items[i];
            p->deref = NULL;
        }
    }

    return LIST_OK;
}

// ----

void list_qsort(struct list *l, list_qsort_func *qsort_func) {
    qsort(l->items, l->used_size, sizeof(struct list_item), qsort_func);
}

int list_qsort_str_lex_asc(const void *ap, const void *bp) {
    return strcmp(((struct list_item *)ap)->ptr, ((struct list_item *)bp)->ptr);
}

int list_qsort_str_lex_desc(const void *ap, const void *bp) {
    return strcmp(((struct list_item *)bp)->ptr, ((struct list_item *)ap)->ptr);
}

int list_qsort_lli_asc(const void *ap, const void *bp) {
    long long av,
              bv;

    av = *((long long *)((struct list_item *)ap)->ptr);
    bv = *((long long *)((struct list_item *)bp)->ptr);

    if(av < bv)
        return -1;
    else if(av > bv)
        return +1;
    else // if(av == bv)
        return 0;
}

int list_qsort_lli_desc(const void *ap, const void *bp) {
    long long av,
              bv;

    av = *((long long *)((struct list_item *)ap)->ptr);
    bv = *((long long *)((struct list_item *)bp)->ptr);

    if(av < bv)
        return +1;
    else if(av > bv)
        return -1;
    else // if(av == bv)
        return 0;
}
