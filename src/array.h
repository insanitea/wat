#ifndef WAT_ARRAY_H
#define WAT_ARRAY_H

#include <sys/types.h>

#define splice(array, item_size, \
               size, alloc_size, pool_size, \
               pos, nr, ni, insert_buf, \
               array_ret, size_ret, alloc_size_ret) \
        _splice((void *)array, item_size, \
                size, alloc_size, pool_size, \
                pos, nr, ni, (void *)insert_buf, \
                (void **)array_ret, size_ret, alloc_size_ret)

int _splice(void *array, size_t item_size,
            size_t size, size_t alloc_size, size_t pool_size,
            size_t pos, size_t nr, size_t ni, void *insert_buf,
            void **array_ret, size_t *size_ret, size_t *alloc_size_ret);

#endif
