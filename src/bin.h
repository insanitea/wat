#ifndef WAT_BIN_H
#define WAT_BIN_H

#include <stdint.h>
#include <sys/types.h>

size_t bingap(uintmax_t n);
size_t popcnt(uintmax_t n);

#endif
