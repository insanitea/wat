#ifndef WAT_BTREE
#define WAT_BTREE

#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>

enum btree_status {
    BTREE_OK,
    BTREE_REPLACE,
    BTREE_NOT_FOUND,
    BTREE_MALLOC,
    BTREE_CORRUPT
};

enum btree_iter_action {
    BTREE_REMOVE = 0x01,
    BTREE_BREAK = 0x02
};

typedef void (btree_deref_cb)(void *data);
typedef int (btree_iter_cb)(char *key, void *data, void *arg);

struct btree_node {
    char *key;
    void *data;
    btree_deref_cb *deref;
    size_t height,
           size;
    struct btree_node *left,
                      *right,
                      *parent,
                      **link;
};

ssize_t btree_get_balance(struct btree_node *root);
size_t btree_get_height(struct btree_node *root);
size_t btree_get_size(struct btree_node *root);

void btree_destroy(struct btree_node **link);
int btree_insert(struct btree_node **link, char *key, void *data, btree_deref_cb *deref);
int btree_remove(struct btree_node **link, char *key);
int btree_get(struct btree_node *root, char *key, void **data_ret);
int btree_iter(struct btree_node **link, btree_iter_cb *callback, void *arg);
void btree_fprint(FILE *fh, struct btree_node *root, btree_iter_cb *callback);
int btree_rebuild(struct btree_node **link);

#endif
