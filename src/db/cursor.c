#include "db-private.h"
#include "db.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int find_closest(struct watdb_item *z, size_t i, struct watdb_cursor *v_ret) {
    struct watdb_cursor v;
    struct watdb_page *p;

    memset(&v, 0, sizeof(struct watdb_cursor));

    if(i < z->meta->chain.len/2) {
        for(p=z->chain->head; p != NULL; p=p->next) {
            if(p->tail_i < i) {
                // still left of target index; select page and continue.

                v.rel = +1;
                v.index = p->tail_i;
                v.offset = p->len-1;
                v.page = p;
            }
            else if(p->head_i > i) {
                // now right of target index; select page if closer and break.

                if(v.page == NULL || (p->head_i - i) < (i - v.page->tail_i)) {
                    v.rel = -1;
                    v.index = p->head_i;
                    v.offset = 0;
                    v.page = p;
                }

                break;
            }
            else {
                // target index found; select page and break.

                v.rel = 0;
                v.index = i;
                v.offset = i - p->head_i;
                v.page = p;

                break;
            }
        }
    }
    else {
        for(p=z->chain->tail; p != NULL; p=p->prev) {
            if(p->head_i > i) {
                // still right of target index; select page and continue.

                v.rel = -1;
                v.index = p->head_i;
                v.offset = 0;
                v.page = p;
            }
            else if(p->tail_i < i) {
                // now left of target index; select page if closer and break.

                if(v.page == NULL || (i - p->tail_i) < (v.page->head_i - i)) {
                    v.rel = +1;
                    v.index = p->tail_i;
                    v.offset = p->len-1;
                    v.page = p;
                }

                break;
            }
            else {
                // target index found; select page and break.

                v.rel = 0;
                v.index = i;
                v.offset = i - p->head_i;
                v.page = p;

                break;
            }
        }
    }

    if(v.page == NULL)
        return WATDB_NOT_FOUND;

    v.p_ref = v.page->ref;
    v.p_meta = v.page->meta;
    v.p_len = v.page->len;
    v.p_max = v.page->max;

    *v_ret = v;

    return WATDB_OK;
}

// ----

int cursor_reset(struct watdb_item *z, struct watdb_cursor *v, int rel) {
    int e;
    struct watdb_cursor tv;

    //
    // rel:
    //
    //  -1 = first item in chain
    //   0 = last item in chain
    //  +1 = max (for appension)
    //

    memset(&tv, 0, sizeof(struct watdb_cursor));

    if(rel < 0) {
        tv.p_ref = z->meta->chain.head;
        tv.page = z->chain->head;
    }
    else if(rel == 0) {
        tv.p_ref = z->meta->chain.tail;
        tv.page = z->chain->tail;
        tv.index = z->meta->chain.len > 0 ? z->meta->chain.len-1 : 0;
    }
    else { // if(rel > 0)
        tv.p_ref = z->meta->chain.tail;
        tv.page = z->chain->tail;
        tv.index = z->meta->chain.len;
    }

    if(tv.page != NULL && ref_cmp(tv.page->ref, tv.p_ref) == 0) {
        tv.p_meta = tv.page->meta;
        tv.p_len = tv.page->len;
        tv.p_max = tv.page->max;
    }
    else {
        if(REF_IS_NULL(tv.p_ref) == false) {
            e = ctx_read_data(z->ctx, &tv.p_meta, sizeof(struct watdb_page_meta), tv.p_ref);
            if(e != WATDB_OK)
                return e;

            tv.p_len = tv.p_meta.len / sizeof(struct watdb_item_meta);
            tv.p_max = tv.p_meta.max / sizeof(struct watdb_item_meta);
        }

        if(tv.page != NULL) {
            // target p_ref/index is before/after target page
            if(rel < 0)
                tv.rel = -1;
            else
                tv.rel = +1;
        }
    }

    if(rel < 0)
        tv.offset = 0;
    else if(rel == 0)
        tv.offset = tv.p_len > 0 ? tv.p_len-1 : 0;
    else
        tv.offset = tv.p_len;

    *v = tv;

    return WATDB_OK;
}

int cursor_seek_name(struct watdb_item *z, struct watdb_cursor *v, char *name) {
    int e;
    size_t i;
    struct watdb_item *x;

    e = cursor_reset(z,v,-1);
    if(e != WATDB_OK)
        return e;

    while(true) {
        e = cursor_load_page(z,v);
        if(e == WATDB_NOT_FOUND)
            break;
        else if(e != WATDB_OK)
            return e;

        for(i=0; i < v->p_len; ++i) {
            x = &v->page->x_items[i];

            if(x->name != NULL && strcmp(name, x->name) == 0) {
                v->offset = i;
                v->index = v->page->head_i+i;

                return WATDB_OK;
            }
        }

        e = cursor_next_page(z,v);
        if(e == WATDB_NOT_FOUND)
            break;
        else if (e != WATDB_OK)
            return e;
    }

    return WATDB_NOT_FOUND;
}

int cursor_seek_index(struct watdb_item *z, struct watdb_cursor *v, size_t i) {
    int e;
    struct watdb_cursor tv;
    size_t n;

    if(i == v->index)
        return WATDB_OK;
    else if(i == 0)
        return cursor_reset(z,v,-1);
    else if(i == z->meta->chain.len-1)
        return cursor_reset(z,v,0);
    else if(i > z->meta->chain.len || i == z->meta->chain.len)
        return cursor_reset(z,v,+1);

    // find closest loaded page and scan as needed

    e = find_closest(z, i, &tv);
    if(e != WATDB_OK)
        return e;

    if(tv.index < i) {
        fprintf(stderr, "scanning forwards..");

        e = cursor_next_page(z, &tv);
        if(e != WATDB_OK)
            return e;

        for(n=i-tv.index; n >= tv.p_len; n -= tv.p_len) {
            e = cursor_next_page(z, &tv);
            if(e != WATDB_OK)
                return e;
        }

        tv.offset = n;
    }
    else if(tv.index > i) {
        fprintf(stderr, "scanning backwards..");

        e = cursor_prev_page(z, &tv);
        if(e != WATDB_OK)
            return e;

        for(n=tv.index-i; n >= tv.p_len; n -= tv.p_len) {
            e = cursor_prev_page(z, &tv);
            if(e != WATDB_OK)
                return e;
        }

        tv.offset = tv.p_len-n-1;
    }

    *v = tv;

    return WATDB_OK;
}

int cursor_load_page(struct watdb_item *z, struct watdb_cursor *v) {
    int e;
    struct watdb_page *p;
    size_t body_offset,
           i,j;
    struct watdb_slice *t;

    if(v->rel == 0 && v->page != NULL)
        return WATDB_OK;
    else if(REF_IS_NULL(v->p_ref))
        return WATDB_NOT_FOUND;

    e = page_new(z, sizeof(struct watdb_page_meta) + v->p_meta.max, &p);
    if(e != WATDB_OK)
        goto e0;

    t = &z->ctx->slices[v->p_ref.slice];

    body_offset = v->p_ref.offset + sizeof(struct watdb_page_meta);
    if(lseek(t->fd, body_offset, SEEK_SET) == -1) {
        e = WATDB_SEEK;
        goto e1;
    }

    if(read_all(t->fd, p->x_meta, v->p_meta.len) == -1) {
        e = WATDB_READ;
        goto e1;
    }

    for(i=0; i < v->p_len; ++i) {
        e = item_init(&p->x_items[i], z->ctx, &p->x_meta[i]);
        if(e != WATDB_OK)
            goto e2;
    }

    p->ref = v->p_ref;
    p->meta = v->p_meta;
    p->len = v->p_len;
    p->max = v->p_max;

    p->head_i = v->index - v->offset;
    p->tail_i = p->len > 0 ? p->head_i+(p->len-1) : p->head_i;

    page_attach(z, p, v->rel, v->page);

    v->rel = 0;
    v->page = p;

    watdb_log(LOG_INFO, "page loaded (\e[0;97m%zu\e[0m-\e[0;97m%zu\e[0m "
                        "+\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m "
                        "\e[0;97m%zu\e[0m:\e[0;97m%zu\e[0m)",
                        p->head_i, p->tail_i,
                        p->meta.len, p->meta.max,
                        p->ref.slice, p->ref.offset);

    return WATDB_OK;

e2: for(j=0; j < i; ++j)
        item_deinit(&p->x_items[j]);

e1: page_destroy(p);

e0: return e;
}

int cursor_load_adj_page(struct watdb_item *z, struct watdb_cursor *v, int rel) {
    int e;
    struct watdb_cursor tv;

    memcpy(&tv, v, sizeof(struct watdb_cursor));

    if(rel == +1) {
        e = cursor_next_page(z, &tv);
        if(e != WATDB_OK)
            return e;
    }
    else {
        e = cursor_prev_page(z, &tv);
        if(e != WATDB_OK)
            return e;
    }

    e = cursor_load_page(z, &tv);
    if(e != WATDB_OK)
        return e;

    return WATDB_OK;
}

int cursor_prev_page(struct watdb_item *z, struct watdb_cursor *v) {
    int e;
    struct watdb_page *s;
    struct watdb_page_meta meta;

    if(REF_IS_NULL(v->p_meta.prev))
        return WATDB_NOT_FOUND;

    if(v->rel == 0 && v->page != NULL && (s=v->page->prev) != NULL &&
       ref_cmp(s->ref, v->p_meta.prev) == 0)
    {
        v->index = s->tail_i;
        v->offset = s->len > 0 ? s->len-1 : 0;
        v->page = s;

        v->p_ref = s->ref;
        v->p_meta = s->meta;
        v->p_len = s->len;
        v->p_max = s->max;
    }
    else if(v->rel == +1 && (s=v->page) != NULL &&
            ref_cmp(s->ref, v->p_meta.prev) == 0)
    {
        v->rel = 0;
        v->index = s->tail_i;
        v->offset = s->len > 0 ? s->len-1 : 0;

        v->p_ref = s->ref;
        v->p_meta = s->meta;
        v->p_len = s->len;
        v->p_max = s->max;
    }
    else {
        e = ctx_read_data(z->ctx, &meta, sizeof(struct watdb_page_meta), v->p_meta.prev);
        if(e != WATDB_OK)
            return e;

        v->rel = -1;
        v->index -= v->offset+1;
        v->offset = v->p_len > 0 ? v->p_len-1 : 0;

        v->p_ref = v->p_meta.prev;
        v->p_meta = meta;
        v->p_len = meta.len / sizeof(struct watdb_item_meta);
        v->p_max = meta.max / sizeof(struct watdb_item_meta);
    }

    return WATDB_OK;
}

inline
int cursor_next_page(struct watdb_item *z, struct watdb_cursor *v) {
    int e;
    struct watdb_page *s;
    struct watdb_page_meta meta;

    if(REF_IS_NULL(v->p_meta.next))
        return WATDB_NOT_FOUND;

    if(v->rel == 0 && v->page != NULL && (s=v->page->next) != NULL &&
       ref_cmp(s->ref, v->p_meta.next) == 0)
    {
        v->index = s->head_i;
        v->offset = 0;
        v->page = s;

        v->p_ref = s->ref;
        v->p_meta = s->meta;
        v->p_len = s->len;
        v->p_max = s->max;
    }
    else if(v->rel == -1 && (s=v->page) != NULL &&
            ref_cmp(s->ref, v->p_meta.next) == 0)
    {
        v->rel = 0;
        v->index = s->head_i;
        v->offset = 0;

        v->p_ref = s->ref;
        v->p_meta = s->meta;
        v->p_len = s->len;
        v->p_max = s->max;
    }
    else {
        e = ctx_read_data(z->ctx, &meta, sizeof(struct watdb_page_meta), v->p_meta.next);
        if(e != WATDB_OK)
            return e;

        v->rel = +1;
        v->index += v->p_len - v->offset;
        v->offset = 0;

        v->p_ref = v->p_meta.next;
        v->p_meta = meta;
        v->p_len = meta.len / sizeof(struct watdb_item_meta);
        v->p_max = meta.max / sizeof(struct watdb_item_meta);
    }

    return WATDB_OK;
}

inline
int cursor_prev(struct watdb_item *z, struct watdb_cursor *v) {
    int e;

    if(v->offset > 0) {
        --v->offset;
        --v->index;
    }
    else {
        e = cursor_prev_page(z,v);
        if(e != WATDB_OK)
            return e;
    }

    return WATDB_OK;
}

inline
int cursor_next(struct watdb_item *z, struct watdb_cursor *v) {
    int e;
    size_t max_offset;

    if(ref_cmp(v->p_ref, z->meta->chain.tail) == 0)
        max_offset = v->p_len;
    else
        max_offset = v->p_len-1;

    if(v->offset < max_offset) {
        ++v->offset;
        ++v->index;
    }
    else {
        e = cursor_next_page(z,v);
        if(e != WATDB_OK)
            return e;
    }

    return WATDB_OK;
}

int cursor_set(struct watdb_item *z, struct watdb_cursor *v, struct watdb_qitem *q) {
    int e;
    struct watdb_item *x;

    if(v->p_len == 0 || v->index >= z->meta->chain.len)
        return WATDB_NOT_FOUND;

    e = cursor_load_page(z,v);
    if(e != WATDB_OK)
        return e;

    x = &v->page->x_items[v->offset];

    item_clear(x);

    e = item_from_q(x,q);
    if(e != WATDB_OK)
        return e;

    page_set_dirty(z, v->page);

    return WATDB_OK;
}

int cursor_alloc(struct watdb_item *z, struct watdb_cursor *v, size_t n) {
    int e;
    size_t max_per_page,
           n_pages,
           old_tail,
           i,j;
    struct watdb_page *s,
                      *p,
                      *pn;

    max_per_page = (z->ctx->config.page_size - sizeof(struct watdb_page_meta))
                     / sizeof(struct watdb_item_meta);

    n_pages = (n+(max_per_page-1)) / max_per_page;
    old_tail = z->ctx->slices[z->ctx->slice_i].tail;

    // allocate pages

    s = v->page;
    for(i=0; i < n_pages; ++i) {
        e = page_new(z, z->ctx->config.page_size, &p);
        if(e != WATDB_OK)
            goto e1;

        e = ctx_push_page(z->ctx, &p->ref);
        if(e != WATDB_OK) {
            page_destroy(p);
            goto e1;
        }

        page_attach_meta(z, p, +1, s);
        page_attach(z, p, +1, s);

        s = p;
    }

    return WATDB_OK;

e1: z->ctx->slices[z->ctx->slice_i].tail = old_tail;

    p = v->rel == 0 && v->page != NULL ? v->page->next : z->chain->head;
    for(j=0; j < i; ++j) {
        pn = p->next;
        page_destroy(p);
        p = pn;
    }

    return e;
}

int cursor_insert(struct watdb_item *z, struct watdb_cursor *v, struct watdb_qitem *q) {
    int e;
    struct watdb_qitem *qi;
    size_t n_insert,
           n_free,
           n_shift,
           i,j,k,
           meta_len,
           items_len,
           base_i;
    struct watdb_page *p,
                      *s;
    struct watdb_item *x;

    if(q == NULL)
        return WATDB_INPUT;

    n_insert = 0;
    for(qi=q; qi != NULL; qi=qi->next)
        ++n_insert;

    // make sure current page is loaded

    e = cursor_load_page(z,v);
    if(WATDB_FATAL(e))
        goto e0;

    if(e == WATDB_NOT_FOUND) {
        e = cursor_alloc(z, v, n_insert);
        if(e != WATDB_OK)
            goto e0;

        p = z->chain->head;

        v->rel = 0;
        v->index = 0;
        v->offset = 0;
        v->page = p;

        v->p_ref = p->ref;
        v->p_meta = p->meta;
        v->p_len = 0;
        v->p_max = p->max;
    }
    else {
        if(v->offset == 0) {
            // append to end of previous page if possible

            e = cursor_load_adj_page(z,v,-1);
            if(e == WATDB_OK) {
                s = v->page->prev;
                if(s->len < s->max) {
                    v->rel = 0;
                    v->offset = s->len;
                    v->page = s;

                    v->p_ref = s->ref;
                    v->p_meta = s->meta;
                    v->p_len = s->len;
                    v->p_max = s->max;

                    goto s1;
                }
            }
            else if(e != WATDB_NOT_FOUND) {
                goto e0;
            }
        }

        // ensure right-adjacent page is loaded for page allocation metadata updates

        e = cursor_load_adj_page(z,v,+1);
        if(WATDB_FATAL(e))
            goto e0;

    s1: n_free = v->page->max - v->page->len;

        if(n_free >= n_insert) {
            if(v->page->len > 0 && v->offset < v->page->len) {
                n_shift = v->page->len - v->offset;
                watdb_debug("shift inline: %zu items, from offset %zu to %zu\n",
                            n_shift, v->offset, v->offset+n_insert);

                for(i=0; i < n_shift; ++i) {
                    j = v->offset + (n_shift-i-1);
                    k = j + n_insert;

                    v->page->x_meta[k] = v->page->x_meta[j];
                    v->page->x_items[k] = v->page->x_items[j];
                    v->page->x_items[k].meta = &v->page->x_meta[k];
                }

                meta_len = sizeof(struct watdb_item_meta) * n_insert;
                items_len = sizeof(struct watdb_item) * n_insert;

                memset(&v->page->x_meta[v->offset], 0, meta_len);
                memset(&v->page->x_items[v->offset], 0, items_len);
            }

            page_set_dirty(z, v->page);
        }
        else {
            if(v->offset < v->page->len) {
                n_shift = v->page->len - v->offset;
                watdb_debug("shift dynamic: %zu items, from index %zu to %zu\n",
                            n_shift, v->index, v->index+n_insert);

                e = cursor_alloc(z, v, n_shift);
                if(e != WATDB_OK)
                    goto e0;

                p = v->page->next;
                j = 0;

                for(i=v->offset; i < v->page->len; ++i) {
                    p->x_meta[j] = v->page->x_meta[i];
                    p->x_items[j] = v->page->x_items[i];
                    p->x_items[j].meta = &p->x_meta[j];

                    ++p->len;
                    p->meta.len += sizeof(struct watdb_item_meta);

                    if(++j == p->max) {
                        p = p->next;
                        j = 0;
                    }
                }

                meta_len = sizeof(struct watdb_item_meta) * n_shift;
                items_len = sizeof(struct watdb_item) * n_shift;

                memset(&v->page->x_meta[v->offset], 0, meta_len);
                memset(&v->page->x_items[v->offset], 0, items_len);

                v->page->len = v->offset;
                v->page->meta.len = sizeof(struct watdb_item_meta) * v->page->len;

                n_free += n_shift;
            }

            if(n_insert > n_free) {
                n_insert -= n_free;

                e = cursor_alloc(z, v, n_insert);
                if(e != WATDB_OK)
                    goto e0;
            }

            if(v->page->len == v->page->max) {
                s = v->page->next;

                v->rel = 0;
                v->offset = 0;
                v->page = s;

                v->p_ref = s->ref;
                v->p_meta = s->meta;
                v->p_len = s->len;
                v->p_max = s->max;
            }
        }
    }

    p = v->page;
    i = v->offset;

    while(q != NULL) {
        x = &p->x_items[i];

        x->ctx = z->ctx;
        x->meta = &p->x_meta[i];

        e = item_from_q(x,q);
        if(e != WATDB_OK)
            goto e1;

        ++p->len;
        p->meta.len += sizeof(struct watdb_item_meta);

        q = q->next;
        if(++i == p->max) {
            p = p->next;
            i = 0;
        }
    }

    v->p_len = v->page->len;
    v->p_meta = v->page->meta;
    z->meta->chain.len += n_insert;

    // update index ranges for current and subsequent cached pages

    base_i = v->page->head_i;
    for(p=v->page; p != NULL; p=p->next) {
        p->head_i = base_i;
        p->tail_i = p->len > 0 ? base_i+(p->len-1) : base_i;
        base_i += p->len;
    }

    return WATDB_OK;

e1: for(p=v->page->next; p != NULL && p->len == 0; p=s) {
        s = p->next;
        page_destroy(p);
    }

e0: return e;
}

int cursor_remove(struct watdb_item *z, struct watdb_cursor *v, size_t n) {
    int e;
    struct watdb_page *p;
    size_t n_shift,
           meta_len,
           items_len;

    v = &z->chain->cursor;

    if(v->p_len == 0)
        return WATDB_NOT_FOUND;

    e = cursor_load_page(z,v);
    if(e != WATDB_OK)
        return e;

    n_shift = v->p_len - v->offset;

    --v->p_len;
    --v->p_max;
    --v->page->len;
    --z->meta->chain.len;

    if(n_shift > 0) {
        meta_len = sizeof(struct watdb_item_meta) * n_shift;
        items_len = sizeof(struct watdb_item) * n_shift;

        // shift items to left
        memcpy(&v->page->x_meta[v->offset], &v->page->x_meta[v->offset+1], meta_len);
        memcpy(&v->page->x_items[v->offset], &v->page->x_items[v->offset+1], items_len);

        // clear from old page
        memset(&v->page->x_meta[v->p_len], 0, sizeof(struct watdb_item_meta));
        memset(&v->page->x_items[v->p_len], 0, sizeof(struct watdb_item));
    }

    page_set_dirty(z, v->page);

    return WATDB_OK;
}

void cursor_print(struct watdb_cursor *v, FILE *fh) {
    struct watdb_item *x;

    if(v->rel == 0 && v->page != NULL) {
        fprintf(fh, "\e[0;97m%02zu\e[0m \e[0;96m%02zu\e[0m "
                    "(\e[0;97m%02zu\e[0m-\e[0;97m%02zu\e[0m "
                    "+\e[0;97m%02zu\e[0m/\e[0;97m%02zu\e[0m "
                    "\e[0;97m%zu\e[0m:\e[0;97m%zu\e[0m)",
                    v->index, v->offset,
                    v->page->head_i, v->page->tail_i,
                    v->p_len, v->p_max,
                    v->p_ref.slice, v->p_ref.offset);

        x = &v->page->x_items[v->offset];
        if(x->meta != NULL) {
            fprintf(fh, " -> \e[0;97m%s\e[0m",
                        watdb_type_str[v->page->x_meta[v->offset].type]);

            if(x->name != NULL)
                fprintf(fh, ":\e[0;93m%s\e[0m", x->name);
        }

        fprintf(fh, "\n");
    }
    else {
        fprintf(fh, "\e[0;97m%02zu\e[0m \e[0;96m%02zu\e[0m"
                    "(+\e[0;97m%02zu\e[0m/\e[0;97m%02zu\e[0m "
                    "\e[0;97m%zu\e[0m:\e[0;97m%zu\e[0m)",
                    v->index, v->offset,
                    v->p_len, v->p_max,
                    v->p_ref.slice, v->p_ref.offset);
    }
}
