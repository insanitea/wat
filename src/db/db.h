#ifndef WAT_DB_H
#define WAT_DB_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>

#define WATDB_FATAL(e) (e > WATDB_NOT_FOUND)

enum watdb_status {
    WATDB_OK,
    WATDB_NOT_FOUND,
//  ----
    WATDB_MALLOC,
    WATDB_OPEN,
    WATDB_SEEK,
    WATDB_READ,
    WATDB_WRITE,
//  ----
    WATDB_TYPE,
    WATDB_PATH,
    WATDB_INPUT,
    WATDB_RANGE,
    WATDB_CORRUPT,
//  ----
    WATDB_UNCLOSED_DICT,
    WATDB_UNCLOSED_LIST,
    WATDB_UNCLOSED_STR,
    WATDB_EXPECTED_COLON,
    WATDB_EXPECTED_COMMA,
    WATDB_EXPECTED_PAREN,
    WATDB_UNEXPECTED_CHAR,
    WATDB_INVALID_VALUE,
    WATDB_INVALID_BINARY_SEQ
};

enum watdb_item_type {
    WATDB_DICT,
    WATDB_LIST,
//  ----
    WATDB_BOOL,
    WATDB_INT,
    WATDB_UINT,
    WATDB_FLOAT,
    WATDB_STR,
    WATDB_BIN,
//  ----
    WATDB_TYPE_MAX
};

enum watdb_op_code {
    WATDB_INSERT,
    WATDB_REMOVE
};

// metadata
// ---------

#pragma pack(push,1)

struct watdb_ref {
    uint64_t slice,
             offset;
};

struct watdb_chain_meta {
    uint64_t len;
    struct watdb_ref head,
                     tail;
};

struct watdb_page_meta {
    uint64_t len,
             max;
    struct watdb_ref prev,
                     next;
};

struct watdb_data_meta {
    uint64_t len,
             max;
    struct watdb_ref ref;
};

struct watdb_item_meta {
    uint64_t type;
    struct watdb_data_meta name;
    union {
        struct watdb_chain_meta chain;
        struct watdb_data_meta data;
    };
};

struct watdb_ctx_meta {
    uint64_t tail;
    struct watdb_item_meta root,
                           reuse;
    struct watdb_data_meta aux;
};

struct watdb_wal_meta {
    uint64_t op, n, i, rel,
             path_len,
             data_len;
};

#pragma pack(pop)

// chain
// ------

struct watdb_item; // predeclaration

struct watdb_page {
    struct watdb_page_meta meta;
    struct watdb_ref ref;

    struct watdb_item_meta *x_meta;
    struct watdb_item *x_items;

    bool dirty;
    size_t head_i,
           tail_i,
           len,
           max;

    struct watdb_page *prev,
                      *next,
                      *c_prev,
                      *c_next,
                      *s_prev,
                      *s_next;
};

struct watdb_cursor {
    struct watdb_ref p_ref;
    struct watdb_page_meta p_meta;
    size_t p_len,
           p_max;

    int rel;
    size_t index,
           offset;
    struct watdb_page *page;
};

// item
// -----

struct watdb_ctx; // predeclaration

struct watdb_chain {
    struct watdb_item *parent;
    struct watdb_cursor cursor;
    struct watdb_page *head,
                      *tail,   // primary
                      *c_head,
                      *c_tail, // clean
                      *s_head,
                      *s_tail; // sync

//   void * (*key_get)(struct watdb_item *x, void *arg);
//   int (*key_cmp)(void *a, void *b);
};

union watdb_data {
    bool b;
    int64_t i;
    uint64_t ui;
    long double f;
    char *str;
    void *bin;
};

struct watdb_item {
    struct watdb_ctx *ctx;
    struct watdb_item_meta *meta;

    char *name;
    union {
        struct watdb_chain *chain;
        union watdb_data data;
    };

    unsigned int sync;
};

// qitem
// ------

struct watdb_qitem {
    int type;
    size_t len;

    char *name;
    union {
        struct { struct watdb_qitem *head,
                                    *tail; };
        union watdb_data data;
    };

    struct watdb_qitem *parent,
                       *prev,
                       *next;
};

// context
// --------

struct watdb_config {
    size_t slice_size,
           page_size;
    double aux_pad;
};

struct watdb_slice {
    int fd;
    size_t tail;
};

struct watdb_wal_entry {
    struct watdb_wal_meta meta;
    char *path,
         *data;
};

struct watdb_ctx {
    struct watdb_ctx_meta meta;

    char *name;
    struct watdb_config config;

    struct watdb_slice *slices;
    size_t slice_i,
           n_slices,
           n_slices_alloc;

    struct watdb_item root,
                      reuse;

    struct {
        int fd;
        struct watdb_wal_entry *buf;
        size_t len,
               max;
    } wal;
};

// const
// ------

extern const char *watdb_status_str[];
extern const char *watdb_type_str[];

// qitem
// ------

struct watdb_err_cursor {
    char *line;
    size_t offset;
};

int watdb_serialize(struct watdb_qitem *q, size_t indent, char **str_ret);
int watdb_deserialize(char *str, struct watdb_qitem **q_ret, struct watdb_err_cursor *v_ret);

int watdb_qitem_init(struct watdb_qitem *q, int type, char *name, void *data, ssize_t len);
int watdb_qitem_new(struct watdb_qitem **q_ret, int type, char *name, void *data, ssize_t len);
void watdb_qitem_deinit(struct watdb_qitem *q);
void watdb_qitem_destroy(struct watdb_qitem *q);
size_t watdb_qitem_len(struct watdb_qitem *x);
int watdb_qitem_get_name(struct watdb_qitem *q, char **name_ret);
int watdb_qitem_get_data(struct watdb_qitem *q, void *data_ret, size_t *len_ret);
int watdb_qitem_set_name(struct watdb_qitem *q, char *name);
int watdb_qitem_set_data(struct watdb_qitem *q, void *data, ssize_t len);
void watdb_qlist_append(struct watdb_qitem *z, struct watdb_qitem *q);
void watdb_qlist_prepend(struct watdb_qitem *z, struct watdb_qitem *q);
void watdb_qitem_print(struct watdb_qitem *z, FILE *fh, size_t depth);

// api
// ----

extern const struct watdb_config WATDB_CONFIG_DEFAULT;

typedef int (watdb_filter_cb)(struct watdb_item *x, void *filter_arg);

int watdb_open(char *name, struct watdb_config *config,
               struct watdb_ctx **ctx_ret, struct watdb_item **root_ret);
int watdb_access(struct watdb_item *x, char *path, int type, bool create,
                 struct watdb_item **item_ret);
int watdb_sync(struct watdb_ctx *ctx);
int watdb_close(struct watdb_ctx *ctx);

size_t watdb_list_len(struct watdb_item *z);
size_t watdb_list_max(struct watdb_item *z);
int watdb_list_get(struct watdb_item *z, ssize_t n, ssize_t i, int rel,
                   watdb_filter_cb *filter, void *filter_arg,
                   struct watdb_qitem **results_ret, ssize_t *next_ret);
int watdb_list_set(struct watdb_item *z, ssize_t i, struct watdb_qitem *q);
int watdb_list_insert(struct watdb_item *z,
                      ssize_t i, int rel, struct watdb_qitem *q);
int watdb_list_remove(struct watdb_item *z, ssize_t i, ssize_t n, int rel,
                      watdb_filter_cb *filter, void *filter_arg, size_t *count_ret);

#endif
