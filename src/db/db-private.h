#ifndef WAT_DB_PRIVATE_H
#define WAT_DB_PRIVATE_H

#define _FILE_OFFSET_BITS 64

#include "db.h"
#include <stdio.h>

#ifdef DEBUG
    #define watdb_debug(...) fprintf(stderr, __VA_ARGS__)
#else
    #define watdb_debug(...)
#endif

#define KB(n) (   1024ULL  * n)
#define MB(n) (KB(1024ULL) * n)
#define GB(n) (MB(1024ULL) * n)

#define PAGE_LEN(p)       (p->meta.len / sizeof(struct watdb_item_meta))
#define PAGE_REM(p)       (p->max - p->len)
#define PAGE_MAX(size)    ((size - sizeof(struct watdb_page_meta)) \
                           / sizeof(struct watdb_item_meta))

#define REF_IS_NULL(r)    (r.slice == 0 && r.offset == 0)
#define REFP_IS_NULL(rp)  (rp->slice == 0 && rp->offset == 0)

#define QITEM_IS_CHAIN(x) (x->type == WATDB_DICT || x->type == WATDB_LIST)
#define ITEM_IS_CHAIN(x)  (x->meta->type == WATDB_DICT || x->meta->type == WATDB_LIST)

// const
// ------

extern const char *watdb_log_level_str[];
extern const struct watdb_ref WATDB_REF_NULL;

// context
// --------

int ctx_create_db(struct watdb_ctx *ctx, char *name);
int ctx_load_db(struct watdb_ctx *ctx, char *name);
void ctx_close_db(struct watdb_ctx *ctx);

int ctx_read_data(struct watdb_ctx *ctx, void *data, size_t len, struct watdb_ref ref);
int ctx_load_data(struct watdb_ctx *ctx, bool null_term, void **data_ret,
                  struct watdb_data_meta *meta);

int ctx_push_slice(struct watdb_ctx *ctx, size_t *slice_ret, bool select);
int ctx_push_page(struct watdb_ctx *ctx, struct watdb_ref *ref);
int ctx_push_data(struct watdb_ctx *ctx, bool pad, void *data, size_t len,
                  size_t *max_ptr, struct watdb_ref *ref);

int ctx_wal_init(struct watdb_ctx *ctx);
void ctx_wal_clear(struct watdb_ctx *ctx);
int ctx_wal_push(struct watdb_ctx *ctx, int op, size_t n, size_t i,
                 char *path, char *data, size_t len);
int ctx_wal_read(struct watdb_ctx *ctx);
int ctx_wal_write(struct watdb_ctx *ctx);

// page
// -----

int page_new(struct watdb_item *z, size_t size, struct watdb_page **p_ret);
void page_destroy(struct watdb_page *p);
int page_dup(struct watdb_page **p_ret, struct watdb_page *src,
             struct watdb_ctx *ctx, struct watdb_item *parent);
void page_set_dirty(struct watdb_item *z, struct watdb_page *p);
int page_sync(struct watdb_item *z, struct watdb_page *p);
// int page_update_keys(struct watdb_item *z, struct watdb_page *p);
void page_attach_meta(struct watdb_item *z, struct watdb_page *p,
                      int rel, struct watdb_page *s);
void page_attach(struct watdb_item *z, struct watdb_page *p,
                 int rel, struct watdb_page *s);
void page_detach(struct watdb_item *z, struct watdb_page *p);
void page_attach_clean(struct watdb_item *z, struct watdb_page *p);
void page_detach_clean(struct watdb_item *z, struct watdb_page *p);
void page_attach_sync(struct watdb_item *z, struct watdb_page *p);
void page_detach_sync(struct watdb_item *z, struct watdb_page *p);

// cursor
// -------

int cursor_reset(struct watdb_item *z, struct watdb_cursor *v, int rel);
int cursor_seek_name(struct watdb_item *z, struct watdb_cursor *v, char *name);
int cursor_seek_index(struct watdb_item *z, struct watdb_cursor *v, size_t index);
int cursor_load_page(struct watdb_item *z, struct watdb_cursor *v);
int cursor_prev_page(struct watdb_item *z, struct watdb_cursor *v);
int cursor_next_page(struct watdb_item *z, struct watdb_cursor *v);
int cursor_prev(struct watdb_item *z, struct watdb_cursor *v);
int cursor_next(struct watdb_item *z, struct watdb_cursor *v);
int cursor_set(struct watdb_item *z, struct watdb_cursor *v, struct watdb_qitem *q);
int cursor_insert(struct watdb_item *z, struct watdb_cursor *v, struct watdb_qitem *q);
int cursor_remove(struct watdb_item *z, struct watdb_cursor *v, size_t n);
void cursor_print(struct watdb_cursor *v, FILE *fh);

// item
// -----

enum watdb_item_sync_mask {
    WATDB_SYNC_PASS = 1,
    WATDB_SYNC_NAME = 2,
    WATDB_SYNC_DATA = 4,
    WATDB_SYNC_ALL = 7
};

int chain_init(struct watdb_item *z);
void chain_deinit(struct watdb_item *z);
void chain_destroy(struct watdb_item *z);

int item_init(struct watdb_item *x, struct watdb_ctx *ctx, struct watdb_item_meta *meta);
void item_clear(struct watdb_item *x);
void item_deinit(struct watdb_item *x);
int item_copy(struct watdb_item *x, struct watdb_item *src);
int item_from_q(struct watdb_item *x, struct watdb_qitem *q);
int item_to_q(struct watdb_item *x, struct watdb_qitem **q_ret);
int item_sync(struct watdb_item *x);
void item_print(struct watdb_item *x, FILE *fh, size_t depth);

// misc
// -----

enum watdb_log_level {
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG
};

int ref_cmp(struct watdb_ref a, struct watdb_ref b);
int read_all(int fd, void *data, size_t len);
int write_all(int fd, void *data, size_t len);
int watdb_log(int level, const char *fmt_in, ...);

#endif
