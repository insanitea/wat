#include "db-private.h"
#include "db.h"

#include <string.h>
#include <stdlib.h>

// name
// -----

int key_cmp_name(void *a, void *b) {
    return strcmp(a,b);
}

void * key_get_name(struct watdb_item *x, void *arg) {
    return x->name;
}

// id
// ---

// int key_cmp_id(void *a, void *b) {
//     uint64_t x,y;
// 
//     x = *(uint64_t *)a;
//     y = *(uint64_t *)b;
// 
//     if(x < y)
//         return -1;
//     else if(x > y)
//         return +1;
//     else
//         return 0;
// }
// 
// void * key_get_id(struct watdb_item *x, void *arg) {
//     return &x->meta->id;
// }

// item
// -----

int key_cmp_x(void *a, void *b) {
    struct watdb_item *x,*y;
    int64_t i;
    uint64_t ui;

    x = a;
    y = b;

    if(x->meta->type == WATDB_INT) {
        if(y->meta->type == WATDB_INT) {
            i = y->data.i;
        }
        else if(y->meta->type == WATDB_UINT) {
            if(y->data.ui > INT64_MAX)
                return -1;
            else if(x->data.i < 0)
                return +1;

            i = (int64_t)y->data.ui;
        }
        else if(y->meta->type == WATDB_STR) {
            i = strtoll(y->data.str, NULL, 0);
        }
        else {
            return -1;
        }

        if(x->data.i < i)
            return -1;
        else if(x->data.i > i)
            return +1;
        else
            return 0;
    }
    else if(x->meta->type == WATDB_UINT) {
        if(y->meta->type == WATDB_INT) {
            if(x->data.ui > INT64_MAX)
                return +1;
            else if(y->data.i < 0)
                return -1;

            ui = (uint64_t)y->data.i;
        }
        else if(y->meta->type == WATDB_UINT) {
            ui = y->data.ui;
        }
        else if(y->meta->type == WATDB_STR) {
            ui = strtoll(y->data.str, NULL, 0);
        }
        else {
            return -1;
        }

        if(x->data.ui < ui)
            return -1;
        else if(x->data.ui > ui)
            return +1;
        else
            return 0;
    }
    else if(x->meta->type == WATDB_STR) {
        if(y->meta->type == WATDB_STR) {
            return strcmp(x->data.str, y->data.str);
        }
        else if(y->meta->type == WATDB_INT) {
            i = strtoll(x->data.str, NULL, 0);
            if(i < y->data.i)
                return -1;
            else if(i > y->data.i)
                return +1;
            else
                return 0;
        }
        else if(y->meta->type == WATDB_UINT) {
            ui = (uint64_t)y->data.i;
            if(ui < y->data.ui)
                return -1;
            else if(ui > y->data.ui)
                return +1;
            else
                return 0;
        }
        else {
            return -1;
        }
    }
    else {
        return +1;
    }
}

void * key_get_x(struct watdb_item *z, void *arg) {
    int e;
    struct watdb_item *x;

    e = watdb_access(z, arg, -1, false, &x);
    if(e != WATDB_OK)
        return NULL;

    return x;
}
