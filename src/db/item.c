#include "db-private.h"
#include "db.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int chain_init(struct watdb_item *z) {
    int e;

    z->chain = malloc(sizeof(struct watdb_chain));
    if(z->chain == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    memset(z->chain, 0, sizeof(struct watdb_chain));

    e = cursor_reset(z, &z->chain->cursor, -1);
    if(e != WATDB_OK)
        goto e1;

//     if(z->meta->chain.sort_type == WATDB_SORT_NULL) {
//         z->chain->key_cmp = NULL;
//         z->chain->key_get = NULL;
//     }
//     else if(z->meta->chain.sort_type == WATDB_SORT_NAME) {
//         z->chain->key_cmp = key_cmp_name;
//         z->chain->key_get = key_get_name;
//     }
//     else if(u->meta->chain.sort_type == WATDB_SORT_ID) {
//         z->chain->key_cmp = key_cmp_id;
//         z->chain->key_get = key_get_id;
//     }
//     else if(u->meta->chain.sort_type == WATDB_SORT_DATA) {
//         z->chain->key_cmp = key_cmp_x;
//         z->chain->key_get = key_get_x;
//     }

    return WATDB_OK;

e1: free(z->chain);
e0: return e;
}

void chain_deinit(struct watdb_item *z) {
    struct watdb_page *p,
                      *pn;
    size_t i;

    for(p=z->chain->head; p != NULL; p=pn) {
        pn = p->next;

        for(i=0; i < p->len; ++i)
            item_deinit(&p->x_items[i]);

        page_destroy(p);
    }
}

void chain_destroy(struct watdb_item *z) {
    chain_deinit(z);
    free(z->chain);
}

int item_init(struct watdb_item *x, struct watdb_ctx *ctx, struct watdb_item_meta *meta) {
    int e;
    char *str,
         *end;

    x->ctx = ctx;
    x->meta = meta;

    if(REF_IS_NULL(x->meta->name.ref) == false) {
        e = ctx_load_data(ctx, true, (void **)&x->name, &x->meta->name);
        if(e != WATDB_OK)
            goto e0;
    }

    if(ITEM_IS_CHAIN(x)) {
        e = chain_init(x);
        if(e != WATDB_OK)
            goto e1;
    }
    else if(REF_IS_NULL(x->meta->data.ref) == false) {
        if(x->meta->type == WATDB_INT) {
            if(x->meta->data.len > 0) {
                e = ctx_read_data(ctx, (void *)&x->data.i,
                                  x->meta->data.len, x->meta->data.ref);

                if(e != WATDB_OK)
                    goto e1;
            }
            else {
                x->data.i = 0;
            }
        }
        else if(x->meta->type == WATDB_UINT) {
            if(x->meta->data.len > 0) {
                e = ctx_read_data(ctx, (void *)&x->data.ui,
                                  x->meta->data.len, x->meta->data.ref);

                if(e != WATDB_OK)
                    goto e1;
            }
            else {
                x->data.ui = 0;
            }
        }
        else if(x->meta->type == WATDB_FLOAT) {
            if(x->meta->data.len > 0) {
                e = ctx_load_data(ctx, true, (void **)&str, &x->meta->data);
                if(e != WATDB_OK)
                    goto e1;

                errno = 0;
                x->data.f = strtold(str, &end);
                free(str);

                if(errno != 0) {
                    e = WATDB_INVALID_VALUE;
                    goto e1;
                }
            }
            else {
                x->data.f = 0.0;
            }
        }
        else if(x->meta->type == WATDB_STR) {
            if(x->meta->data.len > 0) {
                e = ctx_load_data(ctx, true, (void **)&x->data.str, &x->meta->data);
                if(e != WATDB_OK)
                    goto e1;
            }
            else {
                x->data.str = NULL;
            }
        }
        else { // if(x->meta->type == WATDB_BIN)
            if(x->meta->data.len > 0) {
                e = ctx_load_data(ctx, false, (void **)&x->data.bin, &x->meta->data);
                if(e != WATDB_OK)
                    goto e1;
            }
            else {
                x->data.bin = NULL;
            }
        }
    }

    x->sync = 0;

    return WATDB_OK;

e1: if(x->name != NULL)
        free(x->name);

e0: return e;
}

inline
void item_clear(struct watdb_item *x) {
    if(x->name != NULL) {
        free(x->name);
        x->name = NULL;
    }

    x->meta->name.len = 0;

    if(ITEM_IS_CHAIN(x)) {
        chain_destroy(x);
        x->chain = NULL;

        x->meta->chain.len = 0;
        x->meta->chain.head = WATDB_REF_NULL;
        x->meta->chain.tail = WATDB_REF_NULL;
    }
    else {
        if(x->meta->type == WATDB_INT) {
            x->data.i = 0;
        }
        else if(x->meta->type == WATDB_UINT) {
            x->data.ui = 0;
        }
        else if(x->meta->type == WATDB_STR) {
            if(x->data.str != NULL) {
                free(x->data.str);
                x->data.str = NULL;
            }
        }
        else { // if(x->meta->type == WATDB_BIN)
            if(x->data.bin != NULL) {
                free(x->data.bin);
                x->data.bin = NULL;
            }
        }

        x->meta->data.len = 0;
    }

    x->sync = WATDB_SYNC_ALL;
}

inline
void item_deinit(struct watdb_item *x) {
    if(x->name != NULL)
        free(x->name);

    if(ITEM_IS_CHAIN(x)) {
        chain_destroy(x);
    }
    else if(x->meta->type == WATDB_STR) {
        if(x->data.str != NULL)
            free(x->data.str);
    }
    else if(x->meta->type == WATDB_BIN) {
        if(x->data.bin != NULL)
            free(x->data.bin);
    }
}

int item_copy(struct watdb_item *x, struct watdb_item *src) {
    int e;
    size_t len;
    struct watdb_page *p,
                      *pp,
                      *pn;

    x->meta->type = src->meta->type;

    if(src->name != NULL) {
        x->name = strndup(src->name, src->meta->name.len);
        if(x->name == NULL) {
            e = WATDB_MALLOC;
            goto e0;
        }

        x->meta->name.len = src->meta->name.len;
    }

    if(ITEM_IS_CHAIN(src)) {
        e = chain_init(x);
        if(e != WATDB_OK)
            goto e1;

        for(p=src->chain->head; p != NULL; p=p->next) {
            e = page_dup(&pp, p, src->ctx, x);
            if(e != WATDB_OK)
                goto e2;

            page_attach(x, pp, +1, NULL);
        }

        x->meta->chain.len = src->meta->chain.len;
    }
    else {
        if(x->meta->type == WATDB_INT) {
            len = sizeof(int64_t);
            x->data.i = src->data.i;
        }
        else if(x->meta->type == WATDB_UINT) {
            len = sizeof(uint64_t);
            x->data.ui = src->data.ui;
        }
        else if(x->meta->type == WATDB_STR) {
            len = src->meta->data.len;

            x->data.str = strndup(src->data.str, len);
            if(x->data.str == NULL) {
                e = WATDB_MALLOC;
                goto e1;
            }
        }
        else if(x->meta->type == WATDB_BIN) {
            len = src->meta->data.len;

            x->data.bin = malloc(len);
            if(x->data.bin == NULL) {
                e = WATDB_MALLOC;
                goto e1;
            }

            memcpy(x->data.bin, src->data.bin, len);
        }
        else {
            return WATDB_TYPE;
        }

        x->meta->data.len = len;
    }

    x->sync = WATDB_SYNC_ALL;

    return WATDB_OK;

e2: for(p=x->chain->head; p != NULL; p=pn) {
        pn = p->next;
        page_destroy(p);
    }

e1: if(x->name != NULL)
        free(x->name);

e0: return e;
}

int item_from_q(struct watdb_item *x, struct watdb_qitem *q) {
    int e;
    size_t len;

    x->meta->type = q->type;

    if(q->name != NULL) {
        len = strlen(q->name);

        x->name = strndup(q->name, len);
        if(x->name == NULL) {
            e = WATDB_MALLOC;
            goto e0;
        }

        x->meta->name.len = len;
    }

    if(QITEM_IS_CHAIN(q)) {
        e = chain_init(x);
        if(e != WATDB_OK)
            goto e1;

        if(q->len > 0) {
            e = cursor_insert(x, &x->chain->cursor, q->head);
            if(e != WATDB_OK)
                goto e2;
        }
    }
    else {
        if(q->type == WATDB_INT) {
            x->data.i = q->data.i;
            x->meta->data.len = sizeof(x->data.i);
        }
        else if(q->type == WATDB_UINT) {
            x->data.ui = q->data.ui;
            x->meta->data.len = sizeof(x->data.ui);
        }
        else if(q->type == WATDB_STR) {
            if(q->data.str != NULL) {
                x->data.str = strndup(q->data.str, q->len);
                if(x->data.str == NULL) {
                    e = WATDB_MALLOC;
                    goto e1;
                }

                x->meta->data.len = q->len;
            }
            else {
                x->data.str = NULL;
                x->meta->data.len = 0;
            }
        }
        else if(q->type == WATDB_BIN) {
            if(q->data.bin != NULL) {
                x->data.bin = malloc(q->len);
                if(x->data.bin == NULL) {
                    e = WATDB_MALLOC;
                    goto e1;
                }

                memcpy(x->data.bin, q->data.bin, q->len);
                x->meta->data.len = q->len;
            }
            else {
                x->data.str = NULL;
                x->meta->data.len = 0;
            }
        }
    }

    x->sync = WATDB_SYNC_ALL;

    return WATDB_OK;

e2: chain_destroy(x);
e1: if(x->name != NULL)
        free(x->name);

e0: return e;
}

int item_to_q(struct watdb_item *x, struct watdb_qitem **q_ret) {
    int e;
    struct watdb_qitem *q,
                       *qc;
    struct watdb_cursor *v;
    size_t i;

    q = malloc(sizeof(struct watdb_qitem));
    if(q == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    memset(q, 0, sizeof(struct watdb_qitem));

    q->type = x->meta->type;

    if(x->name != NULL) {
        q->name = strndup(x->name, x->meta->name.len);
        if(q->name == NULL ){
            e = WATDB_MALLOC;
            goto e1;
        }
    }

    if(ITEM_IS_CHAIN(x)) {
        v = &x->chain->cursor;
        cursor_reset(x,v,-1);

        while(true) {
            e = cursor_load_page(x,v);
            if(e == WATDB_NOT_FOUND)
                break;
            else if(e != WATDB_OK)
                goto e1;

            for(i=0; i < v->page->len; ++i) {
                x = &v->page->x_items[i];

                e = item_to_q(x,&qc);
                if(e != WATDB_OK)
                    goto e1;

                watdb_qlist_append(q,qc);
            }

            e = cursor_next_page(x,v);
            if(e == WATDB_NOT_FOUND)
                break;
            else if(e != WATDB_OK)
                goto e1;
        }
    }
    else if(x->meta->type == WATDB_INT) {
        q->data.i = x->data.i;
        q->len = sizeof(q->data.i);
    }
    else if(x->meta->type == WATDB_UINT) {
        q->data.ui = x->data.ui;
        q->len = sizeof(q->data.ui);
    }
    else if(x->meta->type == WATDB_STR) {
        if(x->data.str != NULL) {
            q->data.str = strndup(x->data.str, x->meta->data.len);
            if(q->data.str == NULL) {
                e = WATDB_MALLOC;
                goto e1;
            }

            q->len = x->meta->data.len;
        }
        else {
            q->data.str = NULL;
            q->len = 0;
        }
    }
    else if(x->meta->type == WATDB_BIN) {
        if(x->data.bin != NULL) {
            q->data.bin = malloc(x->meta->data.len);
            if(q->data.bin == NULL) {
                e = WATDB_MALLOC;
                goto e1;
            }

            memcpy(q->data.bin, x->data.bin, x->meta->data.len);
            q->len = x->meta->data.len;
        }
        else {
            q->data.str = NULL;
            q->len = 0;
        }
    }
    else {
        e = WATDB_TYPE;
        goto e1;
    }

    *q_ret = q;

    return WATDB_OK;

e1: watdb_qitem_destroy(q);

e0: return e;
}

int item_sync(struct watdb_item *x) {
    int e;
    struct watdb_page *p,
                      *pn;
    void *data;
    bool pad;

    if(x->sync & WATDB_SYNC_NAME) {
        if(x->name != NULL) {
            e = ctx_push_data(x->ctx, true, x->name, x->meta->name.len,
                              &x->meta->name.max, &x->meta->name.ref);

            if(e != WATDB_OK)
                return e;
        }
        else {
            x->meta->name = (struct watdb_data_meta) { 0, 0, WATDB_REF_NULL };
        }
    }

    if(ITEM_IS_CHAIN(x)) {
        if(x->sync & WATDB_SYNC_PASS) {
            for(p=x->chain->s_head; p != NULL; p=p->s_next) {
                e = page_sync(x,p);
                if(e != WATDB_OK)
                    return e;
            }

            for(p=x->chain->s_head; p != NULL; p=pn) {
                pn = p->s_next;
                p->s_prev = NULL;
                p->s_next = NULL;
            }

            x->chain->s_head = NULL;
            x->chain->s_tail = NULL;
        }
    }
    else if(x->sync & WATDB_SYNC_DATA) {
        if(x->meta->type == WATDB_INT) {
            data = &x->data.i;
            pad = false;
        }
        else if(x->meta->type == WATDB_UINT) {
            data = &x->data.ui;
            pad = false;
        }
        else if(x->meta->type == WATDB_STR) {
            data = x->data.str;
            pad = true;
        }
        else if(x->meta->type == WATDB_BIN) {
            data = x->data.bin;
            pad = true;
        }
        else {
            return WATDB_TYPE;
        }

        if(data != NULL) {
            e = ctx_push_data(x->ctx, pad, data, x->meta->data.len,
                              &x->meta->data.max, &x->meta->data.ref);

            if(e != WATDB_OK)
                return e;
        }
        else {
            x->meta->data = (struct watdb_data_meta) { 0, 0, WATDB_REF_NULL };
        }
    }

    x->sync = 0;

    return WATDB_OK;
}

void item_print(struct watdb_item *x, FILE *fh, size_t depth) {
    struct watdb_item_meta *m;
    struct watdb_page *p;
    size_t i;

    for(i=0; i < depth; ++i)
        fprintf(fh, "  ");

    m = x->meta;
    if(m == NULL) {
        fprintf(fh, "\e[0;91mNULL METADATA\e[0m\n");
        return;
    }

    fprintf(fh, "%s ", watdb_type_str[m->type]);

    if(x->name != NULL) {
        fprintf(fh, "\e[0;93m%s\e[0m ", x->name);
        if(REF_IS_NULL(m->name.ref) == false) {
            fprintf(fh, "(+\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m "
                        "\e[0;97m%zu\e[0m:\e[0;97m%zu\e[0m): ",
                        m->name.len, m->name.max,
                        m->name.ref.slice, m->name.ref.offset);
        }
    }

    if(ITEM_IS_CHAIN(x)) {
        fprintf(fh, "(+\e[0;97m%zu\e[0m)\n", m->chain.len);

        if(x->chain == NULL) {
            for(i=0; i < depth; ++i)
                fprintf(fh, "  ");

            fprintf(fh, "\e[0;91mNULL CHAIN\e[0m\n");
            return;
        }

        ++depth;

        for(p=x->chain->head; p != NULL; p=p->next) {
            for(i=0; i < depth; ++i)
                fprintf(fh, "  ");

            fprintf(fh, "PAGE (\e[0;97m%02zu\e[0m-\e[0;97m%02zu\e[0m "
                        "+\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m "
                        "+\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m "
                        "\e[0;97m%zu\e[0m:\e[0;97m%zu\e[0m)",
                        p->head_i, p->tail_i,
                        p->len, p->max,
                        p->meta.len, p->meta.max,
                        sizeof(struct watdb_page_meta)+p->meta.max,
                        p->ref.slice, p->ref.offset);

            if(p->dirty)
                fprintf(fh, " \e[0;95m*\e[0m\n");
            else
                fprintf(fh, "\n");

            for(i=0; i < p->len; ++i)
                item_print(&p->x_items[i], fh, depth+1);
        }

        --depth;
    }
    else {
        if(m->type == WATDB_INT) {
            fprintf(fh, "\e[0;96m%zi\e[0m ", x->data.i);
        }
        else if(m->type == WATDB_UINT) {
            fprintf(fh, "\e[0;96m%zu\e[0m ", x->data.ui);
        }
        else if(m->type == WATDB_STR) {
            if(x->data.str != NULL)
                fprintf(fh, "\e[0;96m\"%s\"\e[0m ", x->data.str);
            else
                fprintf(fh, "\e[0;96mnull\e[0m ");
        }
        else { // if(m->type == WATDB_BIN)
            if(x->data.bin != NULL)
                fprintf(fh, "\e[0;96m<binary>\e[0m ");
            else
                fprintf(fh, "\e[0;96mnull\e[0m ");
        }

        if(REF_IS_NULL(m->data.ref)) {
            fprintf(fh, "(+\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m)\n",
                        m->data.len, m->data.max);
        }
        else {
            fprintf(fh, "(+\e[0;97m%zu\e[0m/\e[0;97m%zu\e[0m "
                        "\e[0;97m%zu\e[0m:\e[0;97m%zu\e[0m)\n",
                        m->data.len, m->data.max,
                        m->data.ref.slice, m->data.ref.offset);
        }
    }
}
