#include "db.h"
#include "db-private.h"
#include "../string.h"

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int access_name(struct watdb_item *z, char *name, int type, bool create,
                struct watdb_item **item_ret)
{
    int e;
    struct watdb_cursor *v;
    struct watdb_qitem q;
    struct watdb_item *x;

    v = &z->chain->cursor;

    e = cursor_seek_name(z, v, name);
    if(e == WATDB_NOT_FOUND) {
        if(create) {
            e = watdb_qitem_init(&q, type, name, NULL, 0);
            if(e != WATDB_OK)
                goto e0;

            e = cursor_insert(z,v,&q);
            if(e != WATDB_OK)
                goto e1;

            watdb_qitem_deinit(&q);
        }
        else {
            goto e0;
        }
    }
    else if(e != WATDB_OK) {
        goto e0;
    }

    x = &v->page->x_items[v->offset];
    if(create == false && x->meta->type != type) {
        e = WATDB_TYPE;
        goto e1;
    }

    *item_ret = x;

    return WATDB_OK;

e1: watdb_qitem_deinit(&q);

e0: return e;
}

// context
// --------

int watdb_open(char *name, struct watdb_config *config,
               struct watdb_ctx **ctx_ret, struct watdb_item **root_ret)
{
    int e;
    struct watdb_ctx *ctx;
    char path[PATH_MAX];

    ctx = malloc(sizeof(struct watdb_ctx));
    if(ctx == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    memset(ctx, 0, sizeof(struct watdb_ctx));

    if(config != NULL) {
        if(config->aux_pad < 1.0)
            config->aux_pad = WATDB_CONFIG_DEFAULT.aux_pad;
    }
    else {
        config = (struct watdb_config *)&WATDB_CONFIG_DEFAULT;
    }

    memcpy(&ctx->config, config, sizeof(struct watdb_config));
    snprintf(path, sizeof(path), "%s.db.%zu", name, ctx->n_slices);

    if(access(path, F_OK) == -1) {
        e = ctx_create_db(ctx, name);
        if(e != WATDB_OK)
            goto e1;
    }
    else {
        e = ctx_load_db(ctx, name);
        if(e != WATDB_OK)
            goto e1;
    }

    *ctx_ret = ctx;
    *root_ret = &ctx->root;

    return WATDB_OK;

e1: free(ctx);

e0: return e;
}

int watdb_access(struct watdb_item *z, char *path, int type, bool create,
                 struct watdb_item **item_ret)
{
    int e;
    struct watdb_item *parent,
                      *x;
    struct strtoken_s s;
    char *name;

    parent = NULL;
    strtoken_init(&s, path, -1, 0);
    path = strdup(path);

    while(true) {
        z->chain->parent = parent;
        parent = z;

        e = strtoken(&s, ".", &name, NULL);
        if(e == 0) {
            e = access_name(z, name, WATDB_DICT, false, &x);
            if(e != WATDB_OK)
                goto e1;
        }
        else if(e == 1) {
            e = strtoken(&s, NULL, &name, NULL);
            if(e != 0) {
                e = WATDB_INPUT;
                goto e1;
            }

            e = access_name(z, name, type, true, &x);
            if(e != WATDB_OK)
                goto e1;

            break;
        }
    }

    if(x == NULL) {
        e = WATDB_NOT_FOUND;
        goto e1;
    }

    if(type >= 0 && type < WATDB_TYPE_MAX && x->meta->type != type) {
        watdb_log(LOG_ERROR, "access %s: wrong type (%s != %s)",
                             path, watdb_type_str[type], watdb_type_str[x->meta->type]);

        e = WATDB_TYPE;
        goto e1;
    }

    watdb_log(LOG_ERROR, "access \e[0;93m%s\e[0m: %s", path, watdb_type_str[x->meta->type]);
    free(path);

    *item_ret = x;

    return WATDB_OK;

e1: free(path);

    return e;
}

int watdb_sync(struct watdb_ctx *ctx) {
    int e;
    size_t i;

    if(ctx->root.sync == 0)
        return 0;

    if(ctx->root.sync != 0) {
        e = item_sync(&ctx->root);
        if(e != WATDB_OK)
            return e;
    }

    if(ctx->reuse.sync != 0) {
        e = item_sync(&ctx->reuse);
        if(e != WATDB_OK)
            return e;
    }

    if(lseek(ctx->slices[0].fd, 0, SEEK_SET) == -1)
        return WATDB_SEEK;

    e = write_all(ctx->slices[0].fd, &ctx->meta, sizeof(struct watdb_ctx_meta));
    if(e != WATDB_OK)
        return e;

    for(i=0; i < ctx->n_slices; ++i) {
        if(fsync(ctx->slices[i].fd) == -1) {
            watdb_log(LOG_ERROR, "fsync() failed on slice %zu [%i]", i, errno);
            return WATDB_WRITE;
        }
    }

    watdb_log(LOG_INFO, "sync database: \e[0;92m%s\e[0m", ctx->name);

    return WATDB_OK;
}

int watdb_close(struct watdb_ctx *ctx) {
    int e;

    e = watdb_sync(ctx);
    if(e != WATDB_OK)
        return e;

    ctx_close_db(ctx);

    return WATDB_OK;
}

// list
// -----

inline
size_t watdb_list_len(struct watdb_item *z) {
    return z->meta->chain.len;
}

inline
size_t watdb_list_max(struct watdb_item *z) {
    return z->meta->chain.len-1;
}

int watdb_list_get(struct watdb_item *z, ssize_t i, ssize_t n, int rel,
                   watdb_filter_cb *filter, void *filter_arg,
                   struct watdb_qitem **results_ret, ssize_t *next_ret)
{
    int e;
    struct watdb_item *x;
    struct watdb_qitem *r,
                       *q;
    struct watdb_cursor *v;
    int (*cursor_iter)(struct watdb_item *z, struct watdb_cursor *v);
    ssize_t next;

    if(z->meta->type != WATDB_LIST)
        return WATDB_TYPE;

    if(i < 0)
        i = z->meta->chain.len - (-i % z->meta->chain.len);
    else if(i >= z->meta->chain.len)
        return WATDB_RANGE;

    if(n < 0)
        n = z->meta->chain.len;

    watdb_log(LOG_INFO, "get from \e[0;93m%s\e[0m: i=\e[0;97m%zi\e[0m n=\e[0;97m%zi\e[0m %s",
                        z->name, i, n, rel == +1 ? "LTR" : "RTL");

    v = &z->chain->cursor;

    e = cursor_seek_index(z,v,i);
    if(e != WATDB_OK)
        goto e0;

    e = watdb_qitem_new(&r, WATDB_LIST, NULL, NULL, 0);
    if(e != WATDB_OK)
        goto e0;

    if(rel == -1)
        cursor_iter = cursor_prev;
    else
        cursor_iter = cursor_next;

    while(true) {
        if(r->len == n) {
            next = v->index;
            break;
        }

        if(v->page == NULL) {
            e = cursor_load_page(z,v);
            if(e == WATDB_NOT_FOUND)
                break;
            else if(e != WATDB_OK)
                goto e1;
        }

        x = &v->page->x_items[v->offset];
        if(filter == NULL || filter(x, filter_arg) == 0) {
            e = item_to_q(x,&q);
            if(e != WATDB_OK)
                goto e1;

            watdb_qlist_append(r,q);
        }

        e = cursor_iter(z,v);
        if(e == WATDB_NOT_FOUND) {
            next = -1;
            break;
        }
        else if(e != WATDB_OK) {
            goto e1;
        }
    }

    *results_ret = r;
    if(next_ret != NULL)
        *next_ret = next;

    return e;

e1: watdb_qitem_destroy(r);

e0: return e;
}

int watdb_list_set(struct watdb_item *z, ssize_t i, struct watdb_qitem *q) {
    int e;
    struct watdb_cursor *v;

    if(q == NULL)
        return WATDB_INPUT;

    if(i < 0) {
        if(-i >= z->meta->chain.len)
            return WATDB_RANGE;

        i = z->meta->chain.len+i;
    }
    else if(i >= z->meta->chain.len) {
        return WATDB_RANGE;
    }

    v = &z->chain->cursor;

    e = cursor_seek_index(z,v,i);
    if(e != WATDB_OK)
        return e;

    e = cursor_set(z,v,q);
    if(e != WATDB_OK)
        return e;

    return WATDB_OK;
}

int watdb_list_insert(struct watdb_item *z,
                      ssize_t i, int rel, struct watdb_qitem *q)
{
    int e;
    struct watdb_cursor *v;

    if(q == NULL)
        return WATDB_INPUT;

    if(i < 0) {
        if(-i > z->meta->chain.len)
            i = z->meta->chain.len;
        else
            i = z->meta->chain.len+(i+1);
    }
    else if(i > z->meta->chain.len) {
        i = z->meta->chain.len;
    }

    v = &z->chain->cursor;

    e = cursor_seek_index(z,v,i);
    if(WATDB_FATAL(e))
        return e;

    e = cursor_insert(z,v,q);
    if(e != WATDB_OK)
        return e;

    watdb_log(LOG_INFO, "insert into \e[0;93m%s\e[0m: i=%zi %s",
                        z->name, i, rel == +1 ? "LTR" : "RTL");

    return WATDB_OK;
}

int watdb_list_remove(struct watdb_item *z, ssize_t i, ssize_t n, int rel,
                      watdb_filter_cb *filter, void *filter_arg, size_t *count_ret)
{
    if(i < 0) {
        if(-i >= z->meta->chain.len)
            return WATDB_RANGE;

        i = z->meta->chain.len+i;
    }
    else if(i >= z->meta->chain.len) {
        return WATDB_RANGE;
    }

    watdb_log(LOG_INFO, "remove from \e[0;93m%s\e[0m: i=%zi n=%zu %s",
                        z->name, i, n, rel == +1 ? "LTR" : "RTL");

    return WATDB_OK;
}
