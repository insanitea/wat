#include "db-private.h"
#include "db.h"

const char * watdb_status_str[] = {
    [WATDB_OK] = "ok",
    [WATDB_NOT_FOUND] = "not found",
//  ----
    [WATDB_TYPE] = "bad type",
    [WATDB_INPUT] = "bad input",
    [WATDB_RANGE] = "bad range",
    [WATDB_CORRUPT] = "db corrupt",
//  ----
    [WATDB_MALLOC] = "out of memory",
    [WATDB_OPEN] = "file open",
    [WATDB_SEEK] = "file seek",
    [WATDB_READ] = "file read",
    [WATDB_WRITE] = "file write",
//  ----
    [WATDB_UNCLOSED_DICT] = "unclosed dict",
    [WATDB_UNCLOSED_LIST] = "unclosed list",
    [WATDB_UNCLOSED_STR] = "unclosed string",
    [WATDB_EXPECTED_COMMA] = "expected comma",
    [WATDB_EXPECTED_PAREN] = "expected parenthesis",
    [WATDB_UNEXPECTED_CHAR] = "unexpected char",
    [WATDB_INVALID_VALUE] = "invalid value",
    [WATDB_INVALID_BINARY_SEQ] = "invalid binary sequence"
};

const char * watdb_type_str[] = {
    [WATDB_DICT] = "DICT",
    [WATDB_LIST] = "LIST",
    [WATDB_INT] = "INT",
    [WATDB_UINT] = "UINT",
    [WATDB_STR] = "STR",
    [WATDB_BIN] = "BIN"
};

const struct watdb_config WATDB_CONFIG_DEFAULT = {
    .slice_size = GB(4),
    .page_size = KB(8),
    .aux_pad = 1.5
};

const char * watdb_log_level_str[] = {
    "error",
    "warning",
    "info",
    "debug"
};

const struct watdb_ref WATDB_REF_NULL = {
    .slice = 0,
    .offset = 0
};
