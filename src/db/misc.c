#include "db.h"
#include "db-private.h"

#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

int ref_cmp(struct watdb_ref a, struct watdb_ref b) {
    if(a.slice < b.slice)
        return -1;
    else if(a.slice > b.slice)
        return +1;
    else if(a.offset < b.offset)
        return -1;
    else if(a.offset > b.offset)
        return +1;
    else
        return 0;
}

int read_all(int fd, void *data, size_t len) {
    char *ptr;
    size_t rem;
    ssize_t n;

    ptr = data;
    rem = len;

    while(rem > 0) {
        n = read(fd, ptr, rem);
        if(n == -1)
            return WATDB_READ;

        ptr += n;
        rem -= n;
    }

    return WATDB_OK;
}

int write_all(int fd, void *data, size_t len) {
    char *ptr;
    size_t rem;
    ssize_t n;

    ptr = data;
    rem = len;

    while(rem > 0) {
        n = write(fd, ptr, rem);
        if(n == -1)
            return WATDB_WRITE;

        ptr += n;
        rem -= n;
    }

    return WATDB_OK;
}

int log_level = LOG_ERROR;

int watdb_log(int level, const char *fmt_in, ...) {
    time_t t;
    struct tm *tm;
    char dt[256],
         fmt[1024];
    va_list ap;

    if(level < log_level)
        return 1;

    t = time(NULL);
    tm = localtime(&t);
    strftime(dt, sizeof(dt), "%y.%m.%d %H:%M:%S", tm);
    snprintf(fmt, sizeof(fmt), "%s \e[0;37m│\e[0m %s\n",
                               dt, fmt_in);

    va_start(ap, fmt_in);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    return 0;
}
