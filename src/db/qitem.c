#include "db.h"
#include "db-private.h"

#include "../string.h"

#include <ctype.h>
#include <errno.h>

#define BIN_SERIAL_PREFIX "bin("

size_t accum_serial_len(struct watdb_qitem *q, size_t depth, size_t indent_size) {
    size_t len;
    struct watdb_qitem *qc;

    len = indent_size*depth;

    if(q->name != NULL)
        len += strlen(q->name)+4; // quote + name + quote + ": "

    if(QITEM_IS_CHAIN(q)) {
        if(q->head != NULL) {
            len += 2; // "{\n" or "{\n"

            for(qc=q->head; qc != NULL; qc=qc->next) {
                len += accum_serial_len(qc, depth+1, indent_size);
                if(qc->next != NULL)
                    len += 2; // ",\n"
                else
                    len += 1; // "\n"
            }

            len += (indent_size*depth)+1; // indent + "]\n" or "}\n"
        }
        else {
            len += 2; // "[]" or "{}"
        }
    }
    else if(q->type == WATDB_BOOL) {
        if(q->data.b)
            len += 4; // "true"
        else
            len += 5; // "false"
    }
    else if(q->type == WATDB_INT) {
        len += snprintf(NULL, 0, "%zi", q->data.i);
    }
    else if(q->type == WATDB_UINT) {
        len += snprintf(NULL, 0, "%zu", q->data.ui);
    }
    else if(q->type == WATDB_FLOAT) {
        len += snprintf(NULL, 0, "%Lg", q->data.f);
    }
    else if(q->type == WATDB_STR) {
        if(q->data.str != NULL)
            len += q->len + strstrc(q->data.str, "\"") + 2;
        else
            len += 4; // "null"
    }
    else { // if(q->type == WATDB_BIN)
        if(q->data.bin != NULL)
            len += (strlen(BIN_SERIAL_PREFIX)+1) + (q->len*2); // "bin(" + <hex> + ")"
        else
            len += 4; // "null"
    }

    return len;
}

void serialize(struct watdb_qitem *q, char *ptr, char **ptr_ret, size_t depth, size_t indent) {
    static char dict_braces[2] = { '{', '}' };
    static char list_braces[2] = { '[', ']' };
    static struct strrepl_map enc[] = {
        { "\"", "\\\"" },
        { "\n", "\\n" }
    };

    size_t i,
           alloc_len;
    struct watdb_qitem *qc;
    char *braces,
         *str;

    for(i=0; i < depth; ++i)
        ptr += sprintf(ptr, "%*c", (int)indent, ' ');

    if(q->name != NULL)
        ptr += sprintf(ptr, "\"%s\": ", q->name);

    if(QITEM_IS_CHAIN(q)) {
        if(q->type == WATDB_DICT)
            braces = dict_braces;
        else // if(q->type == WATDB_LIST)
            braces = list_braces;

        if(q->head != NULL) {
            ptr += sprintf(ptr, "%c\n", braces[0]);

            ++depth;
            for(qc=q->head; qc != NULL; qc=qc->next) {
                serialize(qc, ptr, &ptr, depth, indent);
                if(qc->next != NULL)
                    ptr += sprintf(ptr, ",\n");
                else
                    *(ptr++) = '\n';
            }
            --depth;

            for(i=0; i < depth; ++i)
                ptr += sprintf(ptr, "%*c", (int)indent, ' ');

            *(ptr++) = braces[1];
        }
        else {
            ptr += sprintf(ptr, "%c%c", braces[0], braces[1]);
        }
    }
    else if(q->type == WATDB_BOOL) {
        if(q->data.b)
            ptr += sprintf(ptr, "true");
        else
            ptr += sprintf(ptr, "false");
    }
    else if(q->type == WATDB_INT) {
        ptr += sprintf(ptr, "%zi", q->data.i);
    }
    else if(q->type == WATDB_UINT) {
        ptr += sprintf(ptr, "%zu", q->data.ui);
    }
    else if(q->type == WATDB_FLOAT) {
        ptr += sprintf(ptr, "%Lg", q->data.f);
    }
    else if(q->type == WATDB_STR) {
        if(q->data.str != NULL) {
            alloc_len = q->len + strstrc(q->data.str, "\"");

            str = malloc(alloc_len+1);
            if(str == NULL)
                return;

            memcpy(str, q->data.str, q->len);
            str[q->len] = '\0';

            strrepl(&str, -1, &alloc_len, sizeof(enc)/sizeof(enc[0]), enc, 0, -1);
            ptr += sprintf(ptr, "\"%s\"", str);

            free(str);
        }
        else {
            ptr += sprintf(ptr, "null");
        }
    }
    else { // if(q->type == WATDB_BIN)
        if(q->data.bin != NULL) {
            ptr += sprintf(ptr, BIN_SERIAL_PREFIX);
            for(i=0; i < q->len; ++i)
                ptr += sprintf(ptr, "%02hhx", ((char *)q->data.bin)[i]);

            ptr += sprintf(ptr, ")");
        }
        else {
            ptr += sprintf(ptr, "null");
        }
    }

    *ptr = '\0';

    if(ptr_ret != NULL)
        *ptr_ret = ptr;
}

// deserialize helpers

void skip_space(char *ptr, char **ptr_ret, struct watdb_err_cursor *v) {
    while(true) {
        if(*ptr == '\n')
            v->line = ++ptr;
        else if(isspace(*ptr))
            ++ptr;
        else
            break;
    }

    *ptr_ret = ptr;
}

int extract_name(char *ptr, char **str_ret, char **ptr_ret, struct watdb_err_cursor *v) {
    int e;
    char *pre,
         *str;
    size_t len;

    pre = ptr++;
    str = pre;

    while(*ptr != '\0') {
        if(*ptr == ':' || isspace(*ptr))
            break;

        ++ptr;
    }

    len = ptr-str;
    skip_space(ptr, &ptr, v);

    if(*ptr != ':') {
        e = WATDB_EXPECTED_COLON;
        goto e1;
    }

    str = strndup(str, len);
    if(str == NULL) {
        e = WATDB_MALLOC;
        ptr = pre;
        goto e1;
    }

    ++ptr;

    *str_ret = str;
    *ptr_ret = ptr;

    return WATDB_OK;

e1: *ptr_ret = ptr;

    return e;
}

int extract_str(char *ptr, char **str_ret, size_t *len_ret, char **ptr_ret,
                struct watdb_err_cursor *v)
{
    static struct strrepl_map r_enc[] = {
        { "\\\"", "\"" },
        { "\\n", "\n" }
    };

    int e;
    char *pre,
         *str;
    size_t len;

    pre = ptr++;
    str = ptr;

    while(*ptr != '\0') {
        if(*ptr == '\"') {
            len = ptr-str;

            str = strndup(str, len);
            if(str == NULL) {
                e = WATDB_MALLOC;
                goto e1;
            }

            strrepl(&str, -1, NULL, sizeof(r_enc)/sizeof(r_enc[0]), r_enc, 0, -1);

            ++ptr; // skip end quote

            *str_ret = str;
            *len_ret = len;
            *ptr_ret = ptr;

            return WATDB_OK;
        }
        else if(*ptr == '\\') {
            ++ptr;
            if(*ptr == '\0')
                break;
            else if(*ptr == '\n')
                goto s1;

            ++ptr;
        }
        else if(*ptr == '\n') {
        s1: v->line = ++ptr;
        }
        else {
            ++ptr;
        }
    }

    *ptr_ret = pre;

    return WATDB_UNCLOSED_STR;

e1: *ptr_ret = ptr;

    return e;
}

int extract_bin(char *ptr, char **bin_ret, size_t *len_ret, char **ptr_ret,
                struct watdb_err_cursor *v)
{
    int e;
    char *pre,
         *hex,
         byte[3],
         *bin;
    size_t hex_len,
           len,
           i,j;

    pre = ptr;

    hex = ptr;
    while(isxdigit(*ptr))
        ++ptr;

    hex_len = ptr-hex;
    if((hex_len % 2) == 1) {
        e = WATDB_INVALID_BINARY_SEQ;
        ptr = pre;
        goto e1;
    }

    len = hex_len/2;

    bin = malloc(len);
    if(bin == NULL) {
        e = WATDB_MALLOC;
        ptr = pre;
        goto e1;
    }

    byte[2] = '\0';
    for(i=0,j=0; i < hex_len; i+=2,++j) {
        memcpy(byte, hex+i, 2);
        bin[j] = strtoul(byte, NULL, 16);
    }

    *bin_ret = bin;
    *len_ret = len;
    *ptr_ret = ptr;

    return WATDB_OK;

e1: *ptr_ret = ptr;

    return e;
}

// predeclaration
int deserialize_body(struct watdb_qitem *parent, char *ptr, char end,
                     char **ptr_ret, struct watdb_err_cursor *v);

int deserialize_item(struct watdb_qitem *parent, char *ptr,
                     struct watdb_qitem **q_ret, char **ptr_ret, struct watdb_err_cursor *ec)
{
    int e;
    char *end,
         *name;
    size_t i,
           n,
           len;
    union watdb_data data;
    struct watdb_qitem *q;

    name = NULL;
s1: skip_space(ptr, &ptr, ec);

    // dictionary
    if(*ptr == '{') {
        ++ptr;

        e = watdb_qitem_new(&q, WATDB_DICT, name, NULL, 0);
        if(e != WATDB_OK)
            goto e1;

        e = deserialize_body(q, ptr, '}', &ptr, ec);
        if(e != WATDB_OK)
            goto e2;
    }
    // list
    else if(*ptr == '[') {
        ++ptr;

        e = watdb_qitem_new(&q, WATDB_LIST, name, NULL, 0);
        if(e != WATDB_OK)
            goto e1;

        e = deserialize_body(q, ptr, ']', &ptr, ec);
        if(e != WATDB_OK)
            goto e2;
    }
    // boolean
    else if(strncasecmp(ptr, "true", 4) == 0) {
        data.b = true;

        e = watdb_qitem_new(&q, WATDB_BOOL, name, &data.b, -1);
        if(e != WATDB_OK)
            goto e1;

        ptr += 4;
    }
    else if(strncasecmp(ptr, "false", 5) == 0) {
        data.b = false;

        e = watdb_qitem_new(&q, WATDB_BOOL, name, &data.b, -1);
        if(e != WATDB_OK)
            goto e1;

        ptr += 5;
    }
    // number
    else if(*ptr == '-') {
        i = 1;
        goto s2;
    }
    else if(isdigit(*ptr)) {
        i = 0;
    s2: while(isdigit(ptr[i]))
            ++i;

        if(ptr[i] == '.') {
            errno = 0;
            data.f = strtold(ptr, &end);
            if(errno != 0) {
                e = WATDB_INVALID_VALUE;
                goto e1;
            }

            e = watdb_qitem_new(&q, WATDB_FLOAT, name, &data.f, -1);
            if(e != WATDB_OK)
                goto e1;
        }
        else {
            errno = 0;
            data.i = strtoll(ptr, &end, 0);
            if(errno != 0) {
                e = WATDB_INVALID_VALUE;
                goto e1;
            }

            e = watdb_qitem_new(&q, WATDB_INT, name, &data.i, -1);
            if(e != WATDB_OK)
                goto e1;
        }

        ptr = end;
    }
    // unquoted name
    else if(name == NULL && isalpha(*ptr)) {
        e = extract_name(ptr, &name, &ptr, ec);
        if(e != WATDB_OK)
            goto e1;

        goto s1;
    }
    // string
    else if(*ptr == '\"') {
        e = extract_str(ptr, &data.str, &len, &ptr, ec);
        if(e != WATDB_OK)
            goto e1;

        // name
        if(*ptr == ':') {
            if(name != NULL) {
                e = WATDB_UNEXPECTED_CHAR;
                goto e1;
            }

            ++ptr;

            name = data.str;
            goto s1;
        }

        e = watdb_qitem_new(&q, WATDB_STR, name, data.str, len);
        free(data.str);

        if(e != WATDB_OK)
            goto e1;
    }
    // null (string)
    else if(strscancmp(ptr, "null", &n) == 0) {
        e = watdb_qitem_new(&q, WATDB_STR, name, NULL, 0);
        if(e != WATDB_OK)
            goto e1;

        ptr += n;
    }
    // binary
    else if(strscancmp(ptr, BIN_SERIAL_PREFIX, &n) == 0) {
        ptr += n;

        // null
        if(strscancmp(ptr, "null", &n) == 0) {
            ptr += n;

            e = watdb_qitem_new(&q, WATDB_BIN, name, NULL, 0);
            if(e != WATDB_OK)
                goto e1;
        }
        else {
            e = extract_bin(ptr, (char **)&data.bin, &len, &ptr, ec);
            if(e != WATDB_OK)
                goto e1;

            e = watdb_qitem_new(&q, WATDB_BIN, name, data.bin, len);
            free(data.bin);

            if(e != WATDB_OK)
                goto e1;

            if(*ptr != ')') {
                e = WATDB_EXPECTED_PAREN;
                goto e1;
            }

            ++ptr;
        }
    }
    else {
        e = WATDB_UNEXPECTED_CHAR;
        goto e1;
    }

    if(name != NULL)
        free(name);

    ec->offset = ptr - ec->line;

    *q_ret = q;
    *ptr_ret = ptr;

    return WATDB_OK;

e2: watdb_qitem_destroy(q);
e1: if(name != NULL)
        free(name);

    ec->offset = ptr - ec->line;

    *ptr_ret = ptr;

    return e;
}

int deserialize_body(struct watdb_qitem *parent, char *ptr, char end, char **ptr_ret,
                     struct watdb_err_cursor *v)
{
    int e;
    char *pre;
    struct watdb_qitem *q;

    while(true) {
        pre = ptr;
        skip_space(ptr, &ptr, v);

        if(*ptr == end) {
            ++ptr;
            e = WATDB_OK;

            break;
        }
        else if(*ptr == '\0') {
            ptr = pre;
            if(parent->type == WATDB_DICT)
                e = WATDB_UNCLOSED_DICT;
            else // parent->type == WATDB_LIST
                e = WATDB_UNCLOSED_LIST;

            break;
        }
        // enforce comma seperation
        else if(parent->tail != NULL) {
            if(*ptr != ',') {
                ptr = pre;
                e = WATDB_EXPECTED_COMMA;

                break;
            }

            ++ptr;
        }

        e = deserialize_item(parent, ptr, &q, &ptr, v);
        if(e != WATDB_OK)
            break;

        watdb_qlist_append(parent, q);
    }

    *ptr_ret = ptr;

    return e;
}

// ----

int watdb_serialize(struct watdb_qitem *q, size_t indent_size, char **str_ret) {
    size_t len;
    char *str;

    len = accum_serial_len(q, 0, indent_size);

    str = malloc(len+1);
    if(str == NULL)
        return WATDB_MALLOC;

    serialize(q, str, NULL, 0, indent_size);

    *str_ret = str;

    return WATDB_OK;
}

int watdb_deserialize(char *str, struct watdb_qitem **q_ret, struct watdb_err_cursor *ec_ret) {
    int e;
    char *ptr;
    struct watdb_err_cursor ec;

    ptr = str;
    memset(&ec, 0, sizeof(ec));
    ec.line = str;

    e = deserialize_item(NULL, ptr, q_ret, &ptr, &ec);

    ec.offset = ptr - ec.line;
    if(ec_ret != NULL)
        *ec_ret = ec;

    return e;
}

int watdb_qitem_init(struct watdb_qitem *q, int type, char *name, void *data, ssize_t len) {
    int e;

    memset(q, 0, sizeof(struct watdb_qitem));

    q->type = type;

    if(name != NULL) {
        e = watdb_qitem_set_name(q, name);
        if(e != WATDB_OK)
            goto e0;
    }

    if(data != NULL) {
        e = watdb_qitem_set_data(q, data, len);
        if(e != WATDB_OK)
            goto e1;
    }

    return WATDB_OK;

e1: watdb_qitem_set_name(q, NULL);

e0: return e;
}

int watdb_qitem_new(struct watdb_qitem **q_ret, int type, char *name, void *data, ssize_t len)
{
    int e;
    struct watdb_qitem *q;

    q = malloc(sizeof(struct watdb_qitem));
    if(q == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    e = watdb_qitem_init(q, type, name, data, len);
    if(e != WATDB_OK)
        goto e1;

    *q_ret = q;

    return WATDB_OK;

e1: free(q);

e0: return e;
}

void watdb_qitem_deinit(struct watdb_qitem *q) {
    struct watdb_qitem *c,
                       *cn;

    if(q->name != NULL)
        free(q->name);

    if(QITEM_IS_CHAIN(q)) {
        for(c=q->head; c != NULL; c=cn) {
            cn = c->next;
            watdb_qitem_destroy(c);
        }
    }
    else if(q->type == WATDB_STR) {
        if(q->data.str != NULL)
            free(q->data.str);
    }
    else if(q->type == WATDB_BIN) {
        if(q->data.bin != NULL)
            free(q->data.bin);
    }
}

inline
void watdb_qitem_destroy(struct watdb_qitem *q) {
    watdb_qitem_deinit(q);
    free(q);
}

inline
size_t watdb_qitem_len(struct watdb_qitem *x) {
    return x->len;
}

void watdb_qitem_detach(struct watdb_qitem *q) {
    if(q->prev != NULL)
        q->prev->next = q->next;
    if(q->next != NULL)
        q->next->prev = q->prev;

    q->parent = NULL;
    q->prev = NULL;
    q->next = NULL;
}

int watdb_qitem_get_name(struct watdb_qitem *q, char **name_ret) {
    char *name;

    if(q->name != NULL) {
        name = strdup(q->name);
        if(name == NULL)
            return WATDB_MALLOC;
    }
    else {
        name = NULL;
    }

    *name_ret = name;

    return WATDB_OK;
}

int watdb_qitem_get_data(struct watdb_qitem *q, void *data_ret, size_t *len_ret) {
    if(q->type == WATDB_BOOL)
        *((bool *)data_ret) = q->data.b;
    else if(q->type == WATDB_INT)
        *((int64_t *)data_ret) = q->data.i;
    else if(q->type == WATDB_UINT)
        *((uint64_t *)data_ret) = q->data.ui;
    else if(q->type == WATDB_FLOAT)
        *((long double *)data_ret) = q->data.f;
    else if(q->type == WATDB_STR)
        *((void **)data_ret) = q->data.str;
    else if(q->type == WATDB_BIN)
        *((void **)data_ret) = q->data.bin;
    else
        return WATDB_TYPE;

    if(len_ret != NULL)
        *len_ret = q->len;

    return WATDB_OK;
}

int watdb_qitem_set_name(struct watdb_qitem *q, char *name) {
    void *tmp;

    if(name != NULL) {
        tmp = strdup(name);
        if(tmp == NULL)
            return WATDB_MALLOC;
    }
    else {
        tmp = NULL;
    }

    if(q->name != NULL)
        free(q->name);

    q->name = tmp;

    return WATDB_OK;
}

int watdb_qitem_set_data(struct watdb_qitem *q, void *data, ssize_t len) {
    void *tmp;

    if(q->type == WATDB_BOOL) {
        q->data.b = *((bool *)data);
        q->len = sizeof(bool);
    }
    else if(q->type == WATDB_INT) {
        q->data.i = *((int64_t *)data);
        q->len = sizeof(int64_t);
    }
    else if(q->type == WATDB_UINT) {
        q->data.ui = *((uint64_t *)data);
        q->len = sizeof(uint64_t);
    }
    else if(q->type == WATDB_FLOAT) {
        q->data.f = *((long double *)data);
        q->len = sizeof(long double);
    }
    else if(q->type == WATDB_STR) {
        if(data != NULL) {
            if(len < 0)
                len = strlen(data);

            tmp = strndup(data, len);
            if(tmp == NULL)
                return WATDB_MALLOC;

            q->data.str = tmp;
            q->len = len;
        }
        else {
            q->data.str = NULL;
            q->len = 0;
        }
    }
    else if(q->type == WATDB_BIN) {
        if(data != NULL) {
            tmp = malloc(len);
            if(tmp == NULL)
                return WATDB_MALLOC;

            memcpy(tmp, data, len);

            q->data.bin = tmp;
            q->len = len;
        }
        else {
            q->data.bin = NULL;
            q->len = 0;
        }
    }
    else {
        return WATDB_TYPE;
    }

    return WATDB_OK;
}

void watdb_qlist_append(struct watdb_qitem *z, struct watdb_qitem *q) {
    if(q->parent != NULL)
        --q->parent->len;

    q->parent = z;
    q->prev = z->tail;
    q->next = NULL;

    if(z->tail != NULL)
        z->tail->next = q;

    z->tail = q;
    if(z->head == NULL)
        z->head = q;

    ++z->len;
}

void watdb_qlist_prepend(struct watdb_qitem *z, struct watdb_qitem *q) {
    if(q->parent != NULL)
        --q->parent->len;

    q->parent = z;
    q->prev = NULL;
    q->next = z->head;

    if(z->head != NULL)
        z->head->prev = q;

    z->head = q;
    if(z->tail == NULL)
        z->tail = q;

    ++z->len;
}

void watdb_qitem_print(struct watdb_qitem *q, FILE *fh, size_t depth) {
    static struct strrepl_map enc[] = {
        { "\"", "\\\"" },
        { "\n", "\\n" }
    };

    struct watdb_qitem *qc;
    size_t i;
    char *str;

    for(i=0; i < depth; ++i)
        fprintf(fh, "  ");

    fprintf(fh, "%s ", watdb_type_str[q->type]);

    if(q->name != NULL) {
        str = strdup(q->name);
        strrepl(&str, -1, NULL, sizeof(enc)/sizeof(enc[0]), enc, 0, -1);
        fprintf(fh, "\e[0;93m%s\e[0m ", str);
        free(str);
    }

    if(QITEM_IS_CHAIN(q)) {
        fprintf(fh, "(+\e[0;97m%zu\e[0m)\n", q->len);

        if(q->head != NULL) {
            ++depth;

            for(qc=q->head; qc != NULL; qc=qc->next) {
                for(i=0; i < depth; ++i)
                    fprintf(fh, "  ");

                watdb_qitem_print(qc, fh, depth);
            }

            --depth;
        }
    }
    else {
        if(q->type == WATDB_BOOL) {
            fprintf(fh, "\e[0;96m%s\e[0m (+\e[0;97m%zu\e[0m)\n", q->data.b ? "true" : "false", q->len);
        }
        else if(q->type == WATDB_INT) {
            fprintf(fh, "\e[0;96m%zi\e[0m (+\e[0;97m%zu\e[0m)\n", q->data.i, q->len);
        }
        else if(q->type == WATDB_UINT) {
            fprintf(fh, "\e[0;96m%zu\e[0m (+\e[0;97m%zu\e[0m)\n", q->data.ui, q->len);
        }
        else if(q->type == WATDB_UINT) {
            fprintf(fh, "\e[0;96m%Lg\e[0m (+\e[0;97m%zu\e[0m)\n", q->data.f, q->len);
        }
        else if(q->type == WATDB_STR) {
            if(q->data.str != NULL) {
                str = strdup(q->data.str);
                strrepl(&str, -1, NULL, sizeof(enc)/sizeof(enc[0]), enc, 0, -1);
                fprintf(fh, "\e[0;96m\"%s\"\e[0m (+\e[0;97m%zu\e[0m)\n", str, q->len);
                free(str);
            }
            else {
                fprintf(fh, "\e[0;96mnull\e[0m (+\e[0;97m%zu\e[0m)\n", q->len);
            }
        }
        else { // if(m->type == WATDB_BIN)
            if(q->data.bin != NULL) {
                fprintf(fh, "\e[0;96mbin(");
                for(i=0; i < q->len; ++i)
                    fprintf(fh, "%02hhx", ((char *)q->data.bin)[i]);
                fprintf(fh, ")\e[0m (+\e[0;97m%zu\e[0m)\n", q->len);
            }
            else {
                fprintf(fh, "\e[0;96mnull\e[0m (+\e[0;97m%zu\e[0m)\n", q->len);
            }
        }
    }
}
