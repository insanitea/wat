#include "db-private.h"
#include "db.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int page_new(struct watdb_item *z, size_t size, struct watdb_page **p_ret) {
    int e;
    struct watdb_page *p;

    p = malloc(sizeof(struct watdb_page));
    if(p == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    memset(p, 0, sizeof(struct watdb_page));

    p->meta.max = size - sizeof(struct watdb_page_meta);
    p->max = p->meta.max / sizeof(struct watdb_item_meta);

    p->x_meta = calloc(p->max, sizeof(struct watdb_item_meta));
    if(p->x_meta == NULL) {
        e = WATDB_MALLOC;
        goto e1;
    }

    p->x_items = calloc(p->max, sizeof(struct watdb_item));
    if(p->x_items == NULL) {
        e = WATDB_MALLOC;
        goto e2;
    }

    *p_ret = p;

    return WATDB_OK;

e2: free(p->x_meta);
e1: free(p);

e0: return e;
}

void page_destroy(struct watdb_page *p) {
    size_t i;

    for(i=0; i < p->len; ++i)
        item_deinit(&p->x_items[i]);

    free(p->x_meta);
    free(p->x_items);
    free(p);
}

void page_set_dirty(struct watdb_item *z, struct watdb_page *p) {
    while(true) {
        p->dirty = true;
        z->sync |= WATDB_SYNC_PASS;

        page_attach_sync(z,p);

        z = z->chain->parent;
        if(z == NULL)
            break;

        p = z->chain->cursor.page;
    }
}

int page_sync(struct watdb_item *z, struct watdb_page *p) {
    int e;
    size_t i;
    struct watdb_item *x;
    struct watdb_slice *t;

    for(i=0; i < p->len; ++i) {
        x = &p->x_items[i];
        if(x->sync != 0) {
            e = item_sync(x);
            if(e != WATDB_OK)
                return e;
        }
    }

    if(p->dirty) {
        t = &z->ctx->slices[p->ref.slice];

        if(lseek(t->fd, p->ref.offset, SEEK_SET) == -1)
            return WATDB_SEEK;

        if(write_all(t->fd, &p->meta, sizeof(struct watdb_page_meta)) == -1)
            return WATDB_WRITE;

        if(write_all(t->fd, p->x_meta, p->meta.max) == -1)
            return WATDB_WRITE;

        p->dirty = false;
        watdb_log(LOG_INFO, "sync page: %zu:%zu +%zu/%zu/%zu", p->ref.slice, p->ref.offset,
                                                               p->meta.len, p->meta.max,
                                                               sizeof(struct watdb_page_meta) + p->meta.max);
    }

    return WATDB_OK;
}

int page_dup(struct watdb_page **p_ret, struct watdb_page *src,
             struct watdb_ctx *ctx, struct watdb_item *parent)
{
    int e;
    struct watdb_page *p;
    size_t meta_max,
           items_max,
           i,j;
    struct watdb_item_meta *x_meta;
    struct watdb_item *x_items,
                      *x;

    p = calloc(1, sizeof(struct watdb_page));
    if(p == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    meta_max = sizeof(struct watdb_item_meta) * src->max;

    x_meta = calloc(1, meta_max);
    if(x_meta == NULL) {
        e = WATDB_MALLOC;
        goto e1;
    }

    items_max = sizeof(struct watdb_item)*src->max;

    x_items = calloc(1, items_max);
    if(x_items == NULL) {
        e = WATDB_MALLOC;
        goto e2;
    }

    memcpy(x_meta, src->x_meta, src->meta.max);

    for(i=0; i < src->len; ++i) {
        x = &x_items[i];

        x->meta = &x_meta[i];
        x->ctx = ctx;

        e = item_copy(x, &src->x_items[i]);
        if(e != WATDB_OK)
            goto e3;
    }

    p->x_items = x_items;
    p->x_meta = x_meta;
    p->len = src->len;
    p->max = src->max;

    p->meta.len = src->meta.len;
    p->meta.max = src->meta.max;

    *p_ret = p;

    return WATDB_OK;

e3: for(j=0; j < i; ++j)
        item_deinit(&p->x_items[j]);

    free(x_items);
e2: free(x_meta);
e1: free(p);

e0: return e;
}

// int page_update_keys(struct watdb_item *z, struct watdb_page *p) {
//     struct watdb_cursor *c;
// 
//     c = &z->chain->cursor;
// 
//     if(z->chain->sort_type == WATDB_SORT_NULL)
//         return WATDB_TYPE;
// 
//     if(p->len > 1) {
//         p->key_head = c->key_get(&p->x_items[0]);
//         p->key_tail = c->key_get(&p->x_items[p->len-1]);
//     }
//     else {
//         p->key_head = c->key_get(&p->x_items[0]);
//         p->key_tail = p->key_head;
//     }
// 
//     return WATDB_OK;
// }

void page_attach_meta(struct watdb_item *z, struct watdb_page *p, int rel, struct watdb_page *s) {
    if(rel < 0) {
        if(s != NULL) {
            // before sibling

            p->meta.prev = s->meta.prev;
            p->meta.next = s->ref;
            page_set_dirty(z,p);

            if(s->prev != NULL) {
                s->prev->meta.next = p->ref;
                page_set_dirty(z, s->prev);
            }

            s->meta.prev = p->ref;
            page_set_dirty(z,s);

            if(ref_cmp(s->ref, z->meta->chain.head) == 0)
                z->meta->chain.head = p->ref;
        }
        else {
            // as head

            p->meta.prev = WATDB_REF_NULL;
            p->meta.next = z->meta->chain.head;
            page_set_dirty(z,p);

            s = z->chain->head;
            if(s != NULL) {
                s->meta.prev = p->ref;
                page_set_dirty(z,s);
            }

            z->meta->chain.head = p->ref;
            if(REF_IS_NULL(z->meta->chain.tail))
                z->meta->chain.tail = p->ref;
        }
    }
    else {
        if(s != NULL) {
            // after sibling

            p->meta.prev = s->ref;
            p->meta.next = s->meta.next;
            page_set_dirty(z,p);

            if(s->next != NULL) {
                s->next->meta.prev = p->ref;
                page_set_dirty(z, s->next);
            }

            s->meta.next = p->ref;
            page_set_dirty(z,s);

            if(ref_cmp(s->ref, z->meta->chain.tail) == 0)
                z->meta->chain.tail = p->ref;
        }
        else {
            // as tail

            p->meta.prev = z->meta->chain.tail;
            p->meta.next = WATDB_REF_NULL;
            page_set_dirty(z,p);

            s = z->chain->tail;
            if(s != NULL) {
                s->meta.next = p->ref;
                page_set_dirty(z,s);
            }

            z->meta->chain.tail = p->ref;
            if(REF_IS_NULL(z->meta->chain.head))
                z->meta->chain.head = p->ref;
        }
    }
}

void page_attach(struct watdb_item *z, struct watdb_page *p, int rel, struct watdb_page *s) {
    if(rel < 0) {
        if(s != NULL) {
            // before sibling

            p->prev = s->prev;
            p->next = s;

            if(s->prev != NULL)
                s->prev->next = p;

            s->prev = p;

            if(s == z->chain->head)
                z->chain->head = p;
        }
        else {
            // as head

            p->prev = NULL;
            p->next = z->chain->head;

            z->chain->head = p;
            if(z->chain->tail == NULL)
                z->chain->tail = p;
        }
    }
    else { // if(rel > 0)
        if(s != NULL) {
            // after sibling

            p->prev = s;
            p->next = s->next;

            if(s->next != NULL)
                s->next->prev = p;

            s->next = p;

            if(s == z->chain->tail)
                z->chain->tail = p;
        }
        else {
            // as tail

            p->prev = z->chain->tail;
            p->next = NULL;

            z->chain->tail = p;
            if(z->chain->head == NULL)
                z->chain->head = p;
        }
    }

    page_attach_clean(z,p);
}

inline
void page_detach(struct watdb_item *z, struct watdb_page *p) {
    if(p->prev != NULL)
        p->prev->next = p->next;
    else if(p == z->chain->head)
        z->chain->head = p->next;

    if(p->next != NULL)
        p->next->prev = p->prev;
    else if(p == z->chain->tail)
        z->chain->tail = p->prev;

    p->prev = NULL;
    p->next = NULL;

    page_detach_clean(z,p);
}

inline
void page_attach_clean(struct watdb_item *z, struct watdb_page *p) {
    struct watdb_page *s;

    s = z->chain->c_head;
    if(s != NULL)
        s->c_prev = p;
    else
        z->chain->c_tail = p;

    p->c_prev = NULL;
    p->c_next = s;

    z->chain->c_head = p;
}

inline
void page_detach_clean(struct watdb_item *z, struct watdb_page *p) {
    if(p->c_prev != NULL)
        p->c_prev->c_next = p->c_next;
    else if(p == z->chain->c_head)
        z->chain->c_head = p->c_next;

    if(p->c_next != NULL)
        p->c_next->c_prev = p->c_prev;
    else if(p == z->chain->c_tail)
        z->chain->c_tail = p->c_prev;

    p->c_prev = NULL;
    p->c_next = NULL;
}

inline
void page_attach_sync(struct watdb_item *z, struct watdb_page *p) {
    struct watdb_page *s;

    s = z->chain->s_tail;
    if(s != NULL)
        s->s_next = p;
    else
        z->chain->s_head = p;

    p->s_prev = s;
    p->s_next = NULL;

    z->chain->s_tail = p;
}

inline
void page_detach_sync(struct watdb_item *z, struct watdb_page *p) {
    if(p->s_prev != NULL)
        p->s_prev->s_next = p->s_next;
    else if(p == z->chain->s_head)
        z->chain->s_head = p->s_next;

    if(p->s_next != NULL)
        p->s_next->s_prev = p->s_prev;
    else if(p == z->chain->s_tail)
        z->chain->s_tail = p->s_prev;

    p->s_prev = NULL;
    p->s_next = NULL;
}
