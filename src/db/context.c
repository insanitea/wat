#include "db-private.h"
#include "db.h"

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define SLICE_POOL 4

int ctx_create_db(struct watdb_ctx *ctx, char *name) {
    int e,
        fd;
    char path[PATH_MAX];
    struct watdb_slice *t;

    ctx->name = strdup(name);
    if(ctx->name == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    snprintf(path, sizeof(path), "%s.db.0", name);

    fd = open(path, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if(fd == -1) {
        e = WATDB_OPEN;
        goto e1;
    }

    ctx->n_slices = 1;

    ctx->slices = calloc(ctx->n_slices, sizeof(struct watdb_slice));
    if(ctx->slices == NULL) {
        e = WATDB_MALLOC;
        goto e2;
    }

    t = &ctx->slices[0];
    t->fd = fd;

    // initial setup

    ctx->meta.root.type = WATDB_DICT;
    ctx->meta.reuse.type = WATDB_LIST;
    ctx->meta.aux.max = ctx->config.page_size;

    e = ctx_push_page(ctx, &ctx->meta.aux.ref);
    if(e != WATDB_OK)
        goto e3;

    e = ctx_push_data(ctx, false, &ctx->meta, sizeof(struct watdb_ctx_meta), NULL, &ctx->meta.aux.ref);
    if(e != WATDB_OK)
        goto e3;

    e = item_init(&ctx->root, ctx, &ctx->meta.root);
    if(e != WATDB_OK)
        goto e3;

    e = item_init(&ctx->reuse, ctx, &ctx->meta.reuse);
    if(e != WATDB_OK)
        goto e4;

    watdb_log(LOG_INFO, "create database: \e[0;92m%s\e[0m", name);

    return WATDB_OK;

e4: item_deinit(&ctx->root);
e3: free(ctx->slices);
e2: close(fd);
e1: free(ctx->name);

e0: return e;
}

int ctx_load_db(struct watdb_ctx *ctx, char *name) {
    int e,
        fd;
    char path[PATH_MAX];
    struct stat s;
    size_t n,i,j;
    struct watdb_slice *t;

    ctx->name = strdup(name);
    if(ctx->name == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    // allocate slice array

    while(true) {
        snprintf(path, sizeof(path), "%s.db.%zu", name, ctx->n_slices);
        if(access(path, F_OK) == -1) {
            if(errno == ENOENT)
                break;

            e = WATDB_READ;
            goto e1;
        }

        ++ctx->n_slices;
    }

    n = (ctx->n_slices + (SLICE_POOL-1)) / SLICE_POOL;

    ctx->slices = calloc(n, sizeof(struct watdb_slice));
    if(ctx->slices == NULL) {
        e = WATDB_MALLOC;
        goto e1;
    }

    // open slices

    for(i=0; i < ctx->n_slices; ++i) {
        snprintf(path, sizeof(path), "%s.db.%zu", name, i);

        fd = open(path, O_RDWR);
        if(fd == -1) {
            if(errno == EACCES)
                watdb_log(LOG_ERROR, "wrong permissions on slice: %s", path);
            else
                watdb_log(LOG_ERROR, "cannot open slice: %s", path);

            e = WATDB_OPEN;
            goto e2;
        }

        if(fstat(fd, &s) == -1) {
            e = WATDB_READ;
            goto e2;
        }

        ctx->slices[i].fd = fd;
        ctx->slices[i].tail = s.st_size;
    }

    ctx->slice_i = ctx->n_slices-1;

    // load context metadata

    e = ctx_read_data(ctx, &ctx->meta, sizeof(struct watdb_ctx_meta), WATDB_REF_NULL);
    if(e != WATDB_OK)
        goto e2;

    e = item_init(&ctx->root, ctx, &ctx->meta.root);
    if(e != WATDB_OK)
        goto e2;

    e = item_init(&ctx->reuse, ctx, &ctx->meta.reuse);
    if(e != WATDB_OK)
        goto e3;

    t = &ctx->slices[ctx->slice_i];

    watdb_log(LOG_INFO, "load database: \e[0;92m%s\e[0m [%zu]", ctx->name, t->tail);

    return WATDB_OK;

e3: item_deinit(&ctx->root);
e2: for(j=0; j < i; ++j)
        close(ctx->slices[j].fd);

    free(ctx->slices);
e1: free(ctx->name);

e0: return e;
}

void ctx_close_db(struct watdb_ctx *ctx) {
    size_t i;

    for(i=0; i < ctx->n_slices; ++i)
        close(ctx->slices[i].fd);

    watdb_log(LOG_INFO, "close database: \e[0;92m%s\e[0m", ctx->name);

    free(ctx->slices);
    free(ctx->name);
    free(ctx);
}

inline
int ctx_read_data(struct watdb_ctx *ctx, void *data, size_t len, struct watdb_ref ref) {
    struct watdb_slice *t;

    t = &ctx->slices[ref.slice];

    if(lseek(t->fd, ref.offset, SEEK_SET) == -1)
        return WATDB_SEEK;

    return read_all(t->fd, data, len);
}

int ctx_load_data(struct watdb_ctx *ctx, bool null_term, void **data_ret,
                  struct watdb_data_meta *meta)
{
    int e;
    size_t max;
    char *data;

    if(null_term)
        max = meta->max+1;
    else
        max = meta->max;

    data = malloc(max);
    if(data == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    e = ctx_read_data(ctx, data, meta->len, meta->ref);
    if(e != WATDB_OK)
        goto e1;

    memset(data+meta->len, 0, max-meta->len);

    *data_ret = data;

    return WATDB_OK;

e1: free(data);

e0: return e;
}

int ctx_push_slice(struct watdb_ctx *ctx, size_t *slice_ret, bool select) {
    int fd;
    char path[PATH_MAX];
    size_t n;
    struct watdb_slice *tmp,
                       *t;

    snprintf(path, sizeof(path), "%s.db.%zu", ctx->root.name, ctx->n_slices);

    fd = open(path, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if(fd == -1) {
        watdb_log(LOG_ERROR, "failed to create slice; file already exists.");
        return WATDB_OPEN;
    }

    if(ctx->n_slices == ctx->n_slices_alloc) {
        n = ctx->n_slices + SLICE_POOL;

        tmp = realloc(ctx->slices, sizeof(struct watdb_slice)*n);
        if(tmp == NULL)
            return WATDB_MALLOC;

        memset(&tmp[ctx->n_slices], 0, sizeof(struct watdb_slice)*(n-ctx->n_slices));

        ctx->slices = tmp;
        ctx->n_slices_alloc = n;
    }

    n = ctx->n_slices++;
    if(select)
        ctx->slice_i = n;

    t = &ctx->slices[n];
    t->fd = fd;

    *slice_ret = n;

    return WATDB_OK;
}

int ctx_push_data(struct watdb_ctx *ctx, bool pad, void *data, size_t len,
                  size_t *max_ptr, struct watdb_ref *ref)
{
    int e;
    struct watdb_slice *t;
    size_t max,
           new_tail;
    uint64_t si,
             offset;

    if(max_ptr != NULL)
        max = *max_ptr;
    else
        max = 0;

    if(REFP_IS_NULL(ref) || len > max) {
        si = ctx->slice_i;
        t = &ctx->slices[si];

        if(pad)
            max = len * ctx->config.aux_pad;
        else
            max = len;

        if(len >= ctx->config.slice_size) {
            // "huge" data

            e = ctx_push_slice(ctx, &si, false);
            if(e != WATDB_OK)
                return e;

            t = &ctx->slices[si];
            max += ctx->config.page_size - (max % ctx->config.page_size);
            new_tail = max;

            offset = t->tail;
            if(lseek(t->fd, offset, SEEK_SET) == -1)
                return WATDB_SEEK;

            e = write_all(t->fd, data, len);
            if(e != WATDB_OK)
                return e;

            t->tail = len;
        }
        else if(len >= ctx->config.page_size) {
            // "large" data

            max += ctx->config.page_size - (max % ctx->config.page_size);
            new_tail = t->tail + max;

            if(new_tail > ctx->config.slice_size) {
                e = ctx_push_slice(ctx, &si, false);
                if(e != WATDB_OK)
                    return e;

                t = &ctx->slices[si];
                new_tail = max;
            }

            offset = t->tail;
            if(lseek(t->fd, offset, SEEK_SET) == -1)
                return WATDB_SEEK;

            e = write_all(t->fd, data, len);
            if(e != WATDB_OK)
                return e;

            t->tail = new_tail;
        }
        else {
            // "small" data

            new_tail = ctx->meta.aux.len + max;
            if(new_tail > ctx->meta.aux.max) {
                e = ctx_push_page(ctx, &ctx->meta.aux.ref);
                if(e != WATDB_OK)
                    return e;

                si = ctx->slice_i;
                t = &ctx->slices[si];

                new_tail = max;
                ctx->meta.aux.len = 0;
            }

            offset = ctx->meta.aux.ref.offset + ctx->meta.aux.len;
            if(lseek(t->fd, offset, SEEK_SET) == -1)
                return WATDB_SEEK;
            
            e = write_all(t->fd, data, len);
            if(e != WATDB_OK)
                return e;

            ctx->meta.aux.len = new_tail;
        }

        *ref = (struct watdb_ref) { si, offset };
        if(max_ptr != NULL)
            *max_ptr = max;

        watdb_log(LOG_INFO, "push data: %zu:%zu +%zu/%zu",
                  ref->slice, ref->offset, len, max);
    }
    else {
        t = &ctx->slices[ref->slice];

        if(lseek(t->fd, ref->offset, SEEK_SET) == -1)
            return WATDB_SEEK;

        e = write_all(t->fd, data, len);
        if(e != WATDB_OK)
            return e;

        watdb_log(LOG_INFO, "update data: %zu:%zu +%zu/%zu",
                  ref->slice, ref->offset, len, max);
    }

    return WATDB_OK;
}

int ctx_push_page(struct watdb_ctx *ctx, struct watdb_ref *ref) {
    int e;
    size_t si,
           new_tail;
    struct watdb_slice *t;

    si = ctx->slice_i;
    t = &ctx->slices[si];

    new_tail = t->tail + ctx->config.page_size;
    if(new_tail > ctx->config.slice_size) {
        e = ctx_push_slice(ctx, &si, true);
        if(e != WATDB_OK)
            return e;

        t = &ctx->slices[si];
        new_tail = ctx->config.page_size;
    }

    ref->slice = si;
    ref->offset = t->tail;

    t->tail = new_tail;

    watdb_log(LOG_INFO, "push page: %zu:%zu +%zu [%zu]",
              ref->slice, ref->offset, ctx->config.page_size, t->tail);

    return WATDB_OK;
}

#define WAL_ALLOC_POOL 256

int ctx_wal_init(struct watdb_ctx *ctx) {
    int e,
        fd;
    char path[PATH_MAX+1];

    snprintf(path, sizeof(path), "%s.diff", ctx->name);

    fd = open(path, O_CREAT|O_CREAT);
    if(fd == -1) {
        if(fd == EEXIST) {
            fprintf(stderr, "Write-ahead log found; attempting recovery.");

            e = ctx_wal_read(ctx);
            if(e != WATDB_OK)
                return e;

            e = ctx_wal_write(ctx);
            if(e != WATDB_OK)
                return e;
        }
        else {
            return WATDB_OPEN;
        }
    }

    ctx->wal.fd = fd;

    return WATDB_OK;
}

void ctx_wal_clear(struct watdb_ctx *ctx) {
    size_t i;

    for(i=0; i < ctx->wal.len; ++i)
        free(ctx->wal.buf[i].data);

    memset(ctx->wal.buf, 0, sizeof(struct watdb_wal_entry) * ctx->wal.len);
    ctx->wal.len = 0;
}

int ctx_wal_push(struct watdb_ctx *ctx, int op, size_t n, size_t i,
                 char *path, char *data, size_t len)
{
    int e;
    struct watdb_wal_entry *w;
    size_t max;

    if(ctx->wal.len == ctx->wal.max) {
        max = ctx->wal.max + WAL_ALLOC_POOL;

        w = realloc(ctx->wal.buf, sizeof(struct watdb_wal_entry) * max);
        if(w == NULL)
            return WATDB_MALLOC;

        ctx->wal.max = max;
    }

    w = &ctx->wal.buf[ctx->wal.len];

    w->meta.op = op;
    w->meta.path_len = strlen(path);
    w->meta.data_len = len;

    w->path = strdup(path);
    if(w->path == NULL) {
        e = WATDB_MALLOC;
        goto e0;
    }

    w->data = malloc(len);
    if(w->data == NULL) {
        e = WATDB_MALLOC;
        goto e1;
    }

    memcpy(w->data, data, len);

    ++ctx->wal.len;

    return WATDB_OK;

e1: free(w->path);

e0: return e;
}

int ctx_wal_read(struct watdb_ctx *ctx) {
    int e;
    struct watdb_wal_entry *w;

    while(true) {
        w = &ctx->wal.buf[ctx->wal.len];

        e = read_all(ctx->wal.fd, &w->meta, sizeof(struct watdb_wal_meta));
        if(e != WATDB_OK)
            goto e1;

        w->path = malloc(w->meta.path_len+1);
        if(w->path == NULL) {
            e = WATDB_MALLOC;
            goto e1;
        }

        w->data = malloc(w->meta.data_len+1);
        if(w->data == NULL) {
            e = WATDB_MALLOC;
            goto e2;
        }

        e = read_all(ctx->wal.fd, w->path, w->meta.path_len);
        if(e != WATDB_OK)
            goto e3;

        w->path[w->meta.path_len] = '\0';

        e = read_all(ctx->wal.fd, w->data, w->meta.data_len);
        if(e != WATDB_OK)
            goto e3;

        w->data[w->meta.data_len] = '\0';

        ++ctx->wal.len;
    }

    return WATDB_OK;

e3: free(w->data);
e2: free(w->path);
e1: ctx_wal_clear(ctx);

    return e;
}

int ctx_wal_write(struct watdb_ctx *ctx) {
    size_t i;
    struct watdb_wal_entry *w;

    for(i=0; i < ctx->wal.len; ++i) {
        w = &ctx->wal.buf[ctx->wal.len++];
        // TODO
    }

    return WATDB_OK;
}
