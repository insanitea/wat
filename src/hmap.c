#include "hmap.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

size_t hash(char *key, size_t n) {
    size_t x,i;

    x=0;
    for(i=0; key[i] != '\0'; ++i) {
        x += (x << 5);
        x ^= key[i];
    }

    return x % n;
}

void remove_key(struct hmap_slot *s, size_t ki) {
    struct hmap_key *k;

    k = &s->keys[ki];
    free(k->name);

    memmove(&s->keys[ki], &s->keys[ki+1], sizeof(struct hmap_key) * (s->n_keys - ki));
    memset(&s->keys[s->n_keys], 0, sizeof(struct hmap_key));

    --s->n_keys;
}

// ----

int hmap_init(struct hmap *hm, size_t size, size_t pool) {
    hm->slots = calloc(size, sizeof(struct hmap_slot));
    if(hm->slots == NULL)
        return HMAP_MALLOC;

    hm->size = size;
    hm->pool = pool > 1 ? pool : 1;
    hm->n_items = 0;

    return HMAP_OK;
}

void hmap_deinit(struct hmap *hm) {
    hmap_clear(hm);
    free(hm->slots);
    memset(hm, 0, sizeof(struct hmap));
}

void hmap_clear(struct hmap *hm) {
    size_t si,ki;
    struct hmap_slot *s;
    struct hmap_key *k;

    for(si=0; si < hm->size; ++si) {
        s = &hm->slots[si];
        
        for(ki=0; ki < s->n_keys; ++ki) {
            k = &s->keys[ki];

            free(k->name);
            if(k->data != NULL && k->deref != NULL)
                k->deref(k->data);
        }

        free(s->keys);
    }

    hm->n_items = 0;
}

int hmap_resize(struct hmap *hm, size_t new_size) {
    size_t si,
           ki,
           ti;
    struct hmap_slot *s,
                     *t,
                     *tmp;
    struct hmap_key *k;

    if(new_size == hm->size)
        return HMAP_OK;
    else if(new_size == 0)
        return HMAP_INPUT;

    for(si=0; si < hm->size; ++si) {
        s = &hm->slots[si];

        for(ki=0; ki < s->n_keys; ++ki) {
            k = &s->keys[ki];

            ti = hash(k->name, new_size);
            if(ti != si) {
                t = &hm->slots[ti];
                t->keys[t->n_keys++] = *k;
                remove_key(s, ki);
            }
        }
    }

    tmp = realloc(hm->slots, sizeof(struct hmap_slot) * new_size);
    if(tmp == NULL)
        return HMAP_MALLOC;

    if(new_size > hm->size)
        memset(&tmp[hm->size], 0, sizeof(struct hmap_slot) * (new_size - hm->size));

    hm->slots = tmp;
    hm->size = new_size;

    return HMAP_OK;
}

int hmap_insert(struct hmap *hm, char *name, void *data, void *deref) {
    struct hmap_slot *s;
    size_t ki,
           n;
    struct hmap_key *k;

    s = &hm->slots[hash(name, hm->size)];
    k = NULL;

    for(ki=0; ki < s->n_keys; ++ki) {
        if(strcmp(s->keys[ki].name, name) == 0) {
            k = &s->keys[ki];
            if(k->data != NULL && k->deref != NULL)
                k->deref(k->data);

            break;
        }
    }

    if(k == NULL) {
        if(s->n_keys == s->n_keys_alloc) {
            n = s->n_keys_alloc + hm->pool;

            k = realloc(s->keys, sizeof(struct hmap_key) * n);
            if(k == NULL)
                return HMAP_MALLOC;

            memset(&k[s->n_keys_alloc], 0, sizeof(struct hmap_key) * hm->pool);

            s->keys = k;
            s->n_keys_alloc = n;
        }

        k = &s->keys[s->n_keys];

        k->name = strdup(name);
        if(k->name == NULL)
            return HMAP_MALLOC;

        ++s->n_keys;
        ++hm->n_items;
    }

    k->data = data;
    k->deref = deref;

    return HMAP_OK;
}

int hmap_merge(struct hmap *dest, struct hmap *src) {
    int e;
    struct hmap_iter v;

    hmap_for_each(src, v) {
        e = hmap_insert(dest, v.key->name, v.key->data, v.key->deref);
        if(e != 0)
            return e;
    }

    return HMAP_OK;
}

int hmap_remove(struct hmap *hm, char *name) {
    struct hmap_slot *s;
    struct hmap_key *k;
    size_t ki;

    s = &hm->slots[hash(name, hm->size)];

    for(ki=0; ki < s->n_keys; ++ki) {
        k = &s->keys[ki];
        if(strcmp(k->name, name) == 0) {
            remove_key(s,ki);
            --hm->n_items;
            return HMAP_OK;
        }
    }

    return HMAP_NOT_FOUND;
}

int hmap_update_key(struct hmap *hm, char *old_name, char *new_name) {
    struct hmap_slot *s;
    size_t ki;
    struct hmap_key *k;

    if(old_name == NULL || new_name == NULL)
        return HMAP_INPUT;

    s = &hm->slots[hash(new_name, hm->size)];

    for(ki=0; ki < s->n_keys; ++ki) {
        k = &s->keys[ki];
        if(strcmp(k->name, new_name) == 0)
            return HMAP_INPUT;
    }

    s = &hm->slots[hash(old_name, hm->size)];

    for(ki=0; ki < s->n_keys; ++ki) {
        k = &s->keys[ki];

        if(strcmp(k->name, old_name) == 0) {
            new_name = strdup(new_name);
            if(new_name == NULL)
                return HMAP_MALLOC;

            free(k->name);
            k->name = new_name;

            return HMAP_OK;
        }
    }

    return HMAP_NOT_FOUND;
}

int hmap_get(struct hmap *hm, char *name, void **data_ret) {
    struct hmap_slot *s;
    size_t ki;
    struct hmap_key *k;

    s = &hm->slots[hash(name, hm->size)];

    for(ki=0; ki < s->n_keys; ++ki) {
        k = &s->keys[ki];

        if(strcmp(k->name, name) == 0) {
            if(data_ret != NULL)
                *data_ret = k->data;

            return HMAP_OK;
        }
    }

    return HMAP_NOT_FOUND;
}

size_t hmap_item_count(struct hmap *hm) {
    return hm->n_items;
}
