#include "regex.h"
#include "string.h"
#include "utf8.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#ifdef DEBUG
    #define debug(...) fprintf(stderr, __VA_ARGS__)
#else
    #define debug(...)
#endif

#define STR_ALLOC_POOL 64

char * re_error_str[] = { "ok", "error", "out of memory" },
     * re_type_str[] = { "GROUP", "ANY", "SET", "SEQ", "LS", "LE", "WS", "WE" },

//   for set aliases
     *alphabetical =   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
     *alphanumerical = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
     *word =           "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_",
     *numerical =      "0123456789",
     *hexadecimal =    "0123456789ABCDEFabcdef",
     *punctuation = "`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?",
     *whitespace = "\t\n\v\f\r \xa0" \
                   "\xe1\x9a\x80\xe2\x80\x80\xe2\x80\x81\xe2\x80\x82\xe2\x80\x83\xe2" \
                   "\x80\x84\xe2\x80\x85\xe2\x80\x86\xe2\x80\x87\xe2\x80\x88\xe2\x80" \
                   "\x89\xe2\x80\x8a\xe2\x80\xa8\xe2\x80\xa9\xe2\x80\xaf\xe2\x81\x9f" \
                   "\xe3\x80\x80";

struct re_group {
    struct re_node node;
    bool capture;
    char *name;
};

struct re_buf {
    struct re_node node;
    char *str;
    size_t len,
           max;
};

struct re_state {
    size_t rc,
           len;
    struct re_state *prev,
                    *next;
};

struct re_block {
    size_t i;
    struct re_node *node;
    struct re_state s,
                    *s_head,
                    *s_tail;
    struct re_block *parent,
                    *prev;
};

//
// DEBUG
//

#ifdef DEBUG

#define debug_state(...) _debug_state(__VA_ARGS__)
#define debug_alt_state(...) _debug_alt_state(__VA_ARGS__)
#define debug_match(...) _debug_match(__VA_ARGS__)

#define DEBUG_FMT_BASE "\e[%um%-6s\e[0m  \e[97m%02zu\e[0m,\e[97m%02zi\e[0m │ " \
                       "r=\e[%um%02zu\e[0m i=\e[97m%02zu\e[0m len=\e[97m%02zu\e[0m │ \e[7;%im%.*s\e[0m\n"

void _debug_state(struct re_block *q, struct re_state *s, char *str, unsigned int colour) {
    debug("\e[37m%2zu\e[0m \e[95m%c\e[0m "
          DEBUG_FMT_BASE, q->node->id, q->node->negate ? '!' : ' ', colour, re_type_str[q->node->type],
                          q->node->rc_min, q->node->rc_max,
                          colour, s->rc, q->i, s->len,
                          colour, (int)s->len, str+q->i);
}

void _debug_alt_state(struct re_block *q, struct re_state *s, char *str, unsigned int colour) {
    debug("     " DEBUG_FMT_BASE, colour, re_type_str[q->node->type],
                                  q->node->rc_min, q->node->rc_max,
                                  colour, s->rc, q->i, s->len,
                                  colour, (int)s->len, str+q->i);
}

void _debug_match(struct re_block *q, char *str) {
    struct re_state *s;

    if(q->node->greedy) {
        _debug_state(q, &q->s, str, 92);
        for(s=q->s_tail; s != NULL; s=s->prev)
            _debug_alt_state(q, s, str, 93);
    }
    else {
        _debug_state(q, &q->s, str, 92);
        for(s=q->s_head; s != NULL; s=s->next)
            _debug_alt_state(q, s, str, 93);
    }
}

#else

#define debug_state(...)
#define debug_alt_state(...)
#define debug_match(...)

#endif

//
// NODES
//

struct re_node * new_node(size_t id, int type) {
    size_t size;
    struct re_node *n;

    if(type == RE_GROUP)
        size = sizeof(struct re_group);
    else if(type == RE_SET || type == RE_SEQ)
        size = sizeof(struct re_buf);
    else
        size = sizeof(struct re_node);

    n = malloc(size);
    if(n == NULL)
        return NULL;

    memset(n, 0, size);

    n->id = id;
    n->type = type;
    n->greedy = false;
    n->rc_min = 1;
    n->rc_max = 1;

    return n;
}

int buf_grow(struct re_buf *b, size_t n) {
    size_t p,
           new_max;
    char *tmp;

    if(++n > b->max) {
        p = n/STR_ALLOC_POOL;
        if(n%STR_ALLOC_POOL)
            ++p;

        new_max = p*STR_ALLOC_POOL;

        tmp = realloc(b->str, new_max);
        if(tmp == NULL)
            return RE_MALLOC;

        b->str = tmp;
        b->max = new_max;

        memset(b->str+b->len, 0, (b->max-b->len));
    }

    return RE_OK;
}

int buf_append(struct re_buf *b, char *ptr, ssize_t n) {
    int e;
    size_t new_len;

    if(n == -1)
        n = strlen(ptr);

    new_len = b->len+n;

    e = buf_grow(b, new_len);
    if(e != RE_OK)
        return e;

    memcpy(b->str+b->len, ptr, n);
    b->str[new_len] = '\0';
    b->len = new_len;

    return RE_OK;
}

int buf_append_char(struct re_buf *b, uint32_t u) {
    int e;
    size_t c,
           new_len;

    e = utf8_byte_count(u,&c);
    if(e != UTF8_OK)
        return RE_ERROR;

    new_len = b->len+c;

    e = buf_grow(b, new_len);
    if(e != RE_OK)
        return e;

    utf8_encode_char(u, b->str+b->len, NULL);
    b->len = new_len;

    return RE_OK;
}

//
// STATE
//

inline static
int push_state(struct re_block *q, size_t rc, size_t i) {
    struct re_state *s;

    s = malloc(sizeof(struct re_state));
    if(s == NULL)
        return RE_MALLOC;

    s->rc = rc;
    s->len = i-q->i;
    s->prev = q->s_tail;
    s->next = NULL;

    if(q->s_tail != NULL)
        q->s_tail->next = s;
    else
        q->s_head = s;

    q->s_tail = s;

    return RE_OK;
}

inline static
int pop_state(struct re_block *q) {
    struct re_state *s;

    // detach next candidate state
    if(q->node->greedy) {
        s = q->s_tail;
        if(s == NULL)
            return RE_ERROR;

        q->s_tail = s->prev;
        if(s->prev != NULL)
            s->prev->next = NULL;
        else
            q->s_head = NULL;
    }
    else {
        s = q->s_head;
        if(s == NULL)
            return RE_ERROR;

        q->s_head = s->next;
        if(s->next != NULL)
            s->next->prev = NULL;
        else
            q->s_tail = NULL;
    }

    // copy to active state
    memcpy(&q->s, s, sizeof(struct re_state));
    free(s);

    return RE_OK;
}

//
// STACK
//

inline static
int push_stack(struct re_block **q_ptr, struct re_block *p, size_t i, struct re_node *n) {
    struct re_block *q;

    q = malloc(sizeof(struct re_block));
    if(q == NULL)
        return RE_MALLOC;

    memset(q, 0, sizeof(struct re_block));

    q->node = n;
    q->i = i;
    q->prev = *q_ptr;
    q->parent = p;

    *q_ptr = q;

    return RE_OK;
}

inline static
int clear_group(struct re_block **q_ptr, struct re_block **p_ptr, size_t *i_ptr, struct re_node **n_ptr) {
    int e;
    struct re_block *q,
                    *p,
                    *qp;

    e = RE_ERROR;
    q = *q_ptr;
    p = *p_ptr;

    // rewind to group block and reset its active state

    while(q != NULL) {
        if(q == p) {
            e = RE_OK;

            p = p->parent;
            memset(&p->s, 0, sizeof(struct re_state));

            *i_ptr = q->i;
            *n_ptr = q->node;

            break;
        }

        qp = q->prev;
        free(q);
        q = qp;
    }

    *q_ptr = q;
    *p_ptr = p;

    return e;
}

inline static
int rewind_retry(struct re_block **q_ptr, struct re_block **p_ptr, size_t *i_ptr, struct re_node **n_ptr) {
    int e;
    struct re_block *q,
                    *p,
                    *qp;

    e = RE_ERROR;
    q = *q_ptr;
    p = *p_ptr;

    while(q != NULL) {
        // stop if this block has another state
        if(q->s_head != NULL) {
            e = RE_OK;

            *i_ptr = q->i;
            *n_ptr = q->node;

            break;
        }
        // handle group repeat blocks
        else if(q->node == NULL) {
            p = q->parent;

            if(p == NULL) {
                e = RE_ERROR;
                break;
            }

            // expand nongreedy groups
            if(p->node->greedy == false && (p->node->rc_max == -1 || p->s.rc < p->node->rc_max)) {
                e = RE_OK;

                *i_ptr = q->i;
                *n_ptr = NULL;

                break;
            }
            else {
                // restore previous group state and continue
                memcpy(&p->s, &q->s, sizeof(struct re_state));
            }
        }

        // delete node
        qp = q->prev;
        free(q);
        q = qp;
    }

    *q_ptr = q;
    *p_ptr = q->parent;

    return e;
}

int capture_stack(struct re_block *q, char *str, struct re_match **match_ret) {
    struct re_match *m;
    size_t n,
           i,
           j;
    struct re_block *x,
                    *qp;
    struct re_group *gn;

    m = malloc(sizeof(struct re_match));
    if(m == NULL)
        goto e0;

    n=0;
    for(x=q; x != NULL; x=x->prev) {
        if(x->node != NULL && x->node->type == RE_GROUP) {
            gn = (struct re_group *)x->node;
            if(gn->capture)
                ++n;
        }
    }

    j = sizeof(char *) * n;

    m->names = malloc(j);
    if(m->names == NULL)
        goto e1;

    m->results = malloc(j);
    if(m->results == NULL)
        goto e2;

    memset(m->names, 0, j);
    memset(m->results, 0, j);

    m->n_captures = n--;
    m->next = NULL;

    i = 0;

    while(q != NULL) {
        if(q->node != NULL && q->node->type == RE_GROUP) {
            gn = (struct re_group *)q->node;
            if(gn->capture) {
                j = n-(i++);

                if(gn->name != NULL) {
                    m->names[j] = strdup(gn->name);
                    if(m->names[j] == NULL)
                        goto e3;
                }

                m->results[j] = strndup(str+q->i, q->s.len);
                if(m->results[j] == NULL)
                    goto e4;
            }
        }

        qp = q->prev;
        free(q);
        q = qp;
    }

    *match_ret = m;

    return RE_OK;

e4: if(m->names[j] != NULL)
        free(m->names[j]);

e3: for(j=0; j < i; ++j) {
        if(m->names[j] != NULL)
            free(m->names[j]);

        free(m->results[j]);
    }

    free(m->results);
e2: free(m->names);
e1: free(m);

e0: return RE_MALLOC;
}

void destroy_stack(struct re_block *q) {
    struct re_block *qp;

    while(q != NULL) {
        qp = q->prev;
        free(q);
        q = qp;
    }
}

// 
// MATCHING
//

bool str_has_char(char *str, uint32_t ch) {
    int e;
    char *ptr;
    uint32_t u;
    size_t c;

    for(ptr=str; *ptr != '\0'; ptr+=c) {
        e = utf8_decode_char(ptr, &u, &c);
        if(e != RE_OK) {
            u = *ptr;
            c = 1;
        }

        if(u == ch)
            return true;
    }

    return false;
}

bool match_any(struct re_block *q, char *str, size_t *i_ptr) {
    int e;
    size_t i,
           c;

    i = *i_ptr;

    e = utf8_decode_char(str+i, NULL, &c);
    if(e != UTF8_OK)
        c = 1;

    *i_ptr = i+c;

    return true;
}

bool match_set(struct re_block *q, char *str, size_t *i_ptr) {
    int e;
    struct re_buf *b;
    size_t i,
           c;
    uint32_t u;

    b = (struct re_buf *)q->node;
    i = *i_ptr;

    e = utf8_decode_char(str+i, &u, &c);
    if(e != UTF8_OK) {
        u = str[i];
        c = 1;
    }

    if(str_has_char(b->str, u)) {
        *i_ptr = i+c;
        return true;
    }

    return false;
}

bool match_seq(struct re_block *q, char *str, size_t *i_ptr) {
    struct re_buf *b;
    size_t i;

    b = (struct re_buf *)q->node;
    i = *i_ptr;

    if(strncmp(str+i, b->str, b->len) == 0) {
        *i_ptr = i+b->len;
        return true;
    }

    return false;
}

bool match_line_start(struct re_block *q, char *str, size_t *i_ptr) {
    size_t i;
    i = *i_ptr;
    return i == 0 || str[i-1] == '\n';
}

bool match_line_end(struct re_block *q, char *str, size_t *i_ptr) {
    size_t i;

    i = *i_ptr;

    if(strncmp(str+i, "\r\n", 2) == 0) {
        *i_ptr = i+2;
        return true;
    }
    else if(str[i] == '\n') {
        *i_ptr = i+1;
        return true;
    }
    else if(str[i] == '\0') {
        return true;
    }

    return false;
}

void chars_3(char *str, size_t i, uint32_t *t_ret, uint32_t *u_ret, uint32_t *v_ret) {
    int e;
    size_t j,tc,uc,vc;
    uint32_t t,u,v;

    if(t_ret != NULL) {
        for(j=i-1; j > 0 && (str[j] & UTF8_C2) == UTF8_C1; --j);

        e = utf8_decode_char(str+j, &t, &tc);
        if(e != RE_OK) {
            t = str[j];
            tc = 1;
        }

        *t_ret = t;
    }

    e = utf8_decode_char(str+i, &u, &uc);
    if(e != RE_OK) {
        u = str[i];
        uc = 1;
    }

    *u_ret = u;

    if(v_ret != NULL) {
        j = i+uc;

        e = utf8_decode_char(str+j, &v, &vc);
        if(e != UTF8_OK) {
            v = str[j];
            v = 1;
        }

        *v_ret = v;
    }
}

bool match_word_start(struct re_block *q, char *str, size_t *i_ptr) {
    size_t i;
    uint32_t t,u;

    i = *i_ptr;
    chars_3(str, i, &t, &u, NULL);

    return str_has_char(whitespace, t) && str_has_char(word, u);
}

bool match_word_end(struct re_block *q, char *str, size_t *i_ptr) {
    size_t i;
    uint32_t u,v;

    i = *i_ptr;
    chars_3(str, i, NULL, &u, &v);

    return v == '\0' || str_has_char(whitespace, v);
}

int match(struct re_node *re, char *str, size_t *i_ret, struct re_match **match_ret) {
    int e;
    struct re_block *q,
                    *p;
    size_t i;
    struct re_node *n;
    bool (*match_func)(struct re_block *x, char *str, size_t *i_ptr);

    static bool (*match_func_map[])(struct re_block *x, char *str, size_t *i_ptr) = {
        NULL,
        match_any,
        match_set,
        match_seq,
        match_line_start,
        match_line_end,
        match_word_start,
        match_word_end
    };

    q = NULL;
    p = NULL;
    i = 0;
    n = re;

c0: while(n != NULL) {
        e = push_stack(&q,p,i,n);
        if(e != RE_OK)
            goto e1;

        if(n->type == RE_GROUP) {
            debug_state(q, &q->s, str, 96);

            p = q;
            n = n->child;

            continue;
        }
        else if(n->type > RE_MAX_TYPE) {
            e = RE_ERROR;
            goto e1;
        }

        if(n->rc_min == 0) {
            // push 0-length state for optional nodes
            e = push_state(q, q->s.rc, q->i);
            if(e != RE_OK)
                goto e1;
        }

        match_func = match_func_map[n->type];

        while(n->rc_max == -1 || q->s.rc < n->rc_max) {
            if(match_func(q, str, &i) == q->node->negate)
                break;

            if(++q->s.rc >= n->rc_min) {
                e = push_state(q, q->s.rc, i);
                if(e != RE_OK)
                    goto e1;
            }

            if(str[i] == '\0')
                break;
        }

        // select next state
    c1: e = pop_state(q);

        if(e == RE_ERROR) {
            debug_state(q, &q->s, str, 91);

            if(p != NULL && p->node->negate && p->s.rc < p->node->rc_min) {
                // treat as match if group is negated and rc < rc_min
                e = clear_group(&q,&p,&i,&n);
                if(e != RE_OK)
                    goto e1;
            }
            else {
                // find 
                e = rewind_retry(&q,&p,&i,&n);
                if(e != RE_OK) {
                    debug("     \e[91m%-6s\e[0m                           │\n", "FAIL");
                    goto e1;
                }

                if(n != NULL) {
                    // rewind to child node if non-null
                    debug("     \e[97m%-6s\e[0m                           │\n", "REWIND");
                    goto c1;
                }
                else {
                    // rewind to beginning of group if null
                    debug("     \e[97m%-6s\e[0m                           │\n", "EXPAND");
                    n = p->node->child;
                    goto c0;
                }
            }
        }
        else if(e != RE_OK) {
            goto e1;
        }

        debug_match(q, str);

        i = q->i+q->s.len;
        n = n->next;
    }

    if(p != NULL) {
        // push group repeat block with base group state

        e = push_stack(&q,p,i,NULL);
        if(e != RE_OK)
            goto e1;

        q->s.rc = p->s.rc;
        q->s.len = p->s.len;

        ++p->s.rc;
        p->s.len = i-p->i;

        if(p->node->negate && p->s.rc >= p->node->rc_min) {
            // fail match if negated and >= rc_min
            e = RE_ERROR;
            debug_state(p, &p->s, str, 96);
            goto e1;
        }

        if((p->node->greedy || p->s.rc < p->node->rc_min) &&
           (p->node->rc_max == -1 || p->s.rc < p->node->rc_max))
        {
            // repeat group if greedy or < rc_min and < rc_max
            debug_state(p, &p->s, str, 96);
            n = p->node->child;
        }
        else {
            // exit group
            debug_state(p, &p->s, str, 92);
            n = p->node->next;
            p = p->parent;
        }

        goto c0; // keep going
    }

    debug("\n");

    *i_ret = i;

    e = capture_stack(q, str, match_ret);
    if(e != RE_OK)
        goto e1;

    return RE_OK;

e1: debug("\n");
    destroy_stack(q);

    return e;
}

//
// GENERAL
//

int re_compile(char *expr, struct re_node **re_ret) {
    int e;
    char *ptr,
         *tk,
         *tmp;
    struct re_node *re,
              *parent,
              *prev,
              **target,
              *n;
    bool negate;
    struct re_group *g;
    struct re_buf *b;
    uint32_t u,
             u_max;
    size_t c;

    static size_t idc=0;

    ptr = expr;
    tk = NULL;

    re = NULL;
    parent = NULL;
    prev = NULL;
    target = &re;
    negate = false;

    while(*ptr != '\0') {
        n = NULL;

        // group open
        if(*ptr == '(') {
            n = new_node(idc++, RE_GROUP);
            n->prev = prev;
            n->parent = parent;

            *target = n;
            parent = n;
            prev = n;
            target = &n->child;

            ++ptr;

            g = (struct re_group *) n;

            // captured
            if(*ptr != '!') {
                g->capture = true;

                if(*ptr == '#') {
                    tk = ++ptr;

                    while(true) {
                        if(*ptr == ':') {
                            c = (ptr++)-tk;

                            g->name = strndup(tk, c);
                            if(g->name == NULL) {
                                e = RE_MALLOC;
                                goto e1;
                            }

                            break;
                        }
                        else if(*ptr == '\0' || *ptr == ')') {
                            e = RE_ERROR;
                            goto e1;
                        }

                        ++ptr;
                    }
                }
            }
            // noncaptured
            else {
                g->capture = false;
                ++ptr;
            }
        }
        // group close
        else if(*ptr == ')') {
            if(parent == NULL) {
                e = RE_ERROR;
                goto e1;
            }

            prev = parent;
            target = &parent->next;
            parent = parent->parent;

            ++ptr;
        }
        // set
        else if(*ptr == '[') {
            n = new_node(idc++, RE_SET);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            b = (struct re_buf *)n;

            ++ptr;

            while(*ptr != '\0') {
                if(*ptr == ']') {
                    ++ptr;
                    break;
                }

                if(ptr[0] == '/' && isalpha(ptr[1])) {
                    if(ptr[1] == 'u') {
                        ptr += 2;

                        u = strtoull(ptr, &tmp, 16);
                        buf_append_char(b,u);

                        ptr += tmp-ptr;
                    }
                    else {
                        if(ptr[1] == 'a') {
                            buf_append(b, alphabetical, -1);
                        }
                        else if(ptr[1] == 'b') {
                            buf_append(b, alphanumerical, -1);
                        }
                        else if(ptr[1] == 'n') {
                            buf_append(b, numerical, -1);
                        }
                        else if(ptr[1] == 'w') {
                            buf_append(b, word, -1);
                        }
                        else if(ptr[1] == 'x') {
                            buf_append(b, hexadecimal, -1);
                        }
                        else if(ptr[1] == 's') {
                            buf_append(b, whitespace, -1);
                        }
                        else {
                            e = RE_ERROR;
                            goto e1;
                        }

                        ptr += 2;
                    }
                }
                else {
                    e = utf8_decode_char(ptr, &u, &c);
                    if(e != UTF8_OK) {
                        e = RE_ERROR;
                        goto e1;
                    }

                    ptr += c;

                    if(*ptr == '-') {
                        ++ptr;

                        e = utf8_decode_char(ptr, &u_max, &c);
                        if(e != UTF8_OK) {
                            e = RE_ERROR;
                            goto e1;
                        }

                        ptr += c;

                        while(u <= u_max) {
                            buf_append_char(b,u);
                            ++u;
                        }
                    }
                    else {
                        buf_append_char(b,u);
                    }
                }
            }
        }
        // any
        else if(*ptr == '.') {
            n = new_node(idc++, RE_ANY);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            ++ptr;
        }
        // set alias
        else if(ptr[0] == '/') {
            n = new_node(idc++, RE_SET);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            b = (struct re_buf *)n;

            if(ptr[1] == 'u') {
                ptr += 2;

                u = strtoull(ptr, &tmp, 16);
                buf_append_char(b,u);

                ptr += tmp-ptr;
            }
            else {
                if(ptr[1] == 'a') {
                    buf_append(b, alphabetical, -1);
                }
                else if(ptr[1] == 'b') {
                    buf_append(b, alphanumerical, -1);
                }
                else if(ptr[1] == 'n') {
                    buf_append(b, numerical, -1);
                }
                else if(ptr[1] == 'w') {
                    buf_append(b, word, -1);
                }
                else if(ptr[1] == 'x') {
                    buf_append(b, hexadecimal, -1);
                }
                else if(ptr[1] == 's') {
                    buf_append(b, whitespace, -1);
                }
                else {
                    e = RE_ERROR;
                    goto e1;
                }

                ptr += 2;
            }
        }
        // repeat >= 0
        else if(*ptr == '*') {
            if(prev == NULL) {
                e = RE_ERROR;
                goto e1;
            }

            prev->rc_min = 0;
            prev->rc_max = -1;

            ++ptr;
            continue;
        }
        // repeat <= 1
        else if(*ptr == '?') {
            if(prev == NULL) {
                e = RE_ERROR;
                goto e1;
            }

            prev->rc_min = 0;
            prev->rc_max = 1;

            ++ptr;
            continue;
        }
        // repeat >= 1
        else if(*ptr == '+') {
            if(prev == NULL) {
                e = RE_ERROR;
                goto e1;
            }

            prev->rc_min = 1;
            prev->rc_max = -1;

            ++ptr;
            continue;
        }
        // repeat n-m
        else if(*ptr == '{') {
            if(prev == NULL) {
                e = RE_ERROR;
                goto e1;
            }

            c = strtoull(++ptr, &tmp, 10);
            if(tmp != ptr) {
                prev->rc_min = c;
                ptr = tmp;
            }

            if(*ptr == ',') {
                c = strtoll(++ptr, &tmp, 10);
                if(tmp != ptr) {
                    prev->rc_max = c;
                    ptr = tmp;
                }
            }
            else {
                prev->rc_max = prev->rc_min;
            }

            if(*ptr != '}') {
                e = RE_ERROR;
                goto e1;
            }

            ++ptr;
            continue;
        }
        // greedy match
        else if(*ptr == '&') {
            prev->greedy = true;

            ++ptr;
            continue;
        }
        // negated match
        else if(*ptr == '!') {
            negate = true;

            ++ptr;
            continue;
        }
        // logical or
        else if(*ptr == '|') {
            if(prev == NULL) {
                e = RE_ERROR;
                goto e1;
            }

            prev->or_next = true;

            ++ptr;
            continue;
        }
        // line start
        else if(*ptr == '^') {
            n = new_node(idc++, RE_LINE_START);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            ++ptr;
        }
        // line end
        else if(*ptr == '$') {
            n = new_node(idc++, RE_LINE_END);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            ++ptr;
        }
        // word start
        else if(*ptr == '<') {
            n = new_node(idc++, RE_WORD_START);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            ++ptr;
        }
        // word end
        else if(*ptr == '>') {
            n = new_node(idc++, RE_WORD_END);
            n->prev = prev;
            n->parent = parent;

            prev = n;
            *target = n;
            target = &n->next;

            ++ptr;
        }
        // sequence
        else {
            if(ptr[0] == '\\' && ptr[1] != '\0')
                ++ptr;

            e = utf8_decode_char(ptr, &u, &c);
            if(e != UTF8_OK) {
                e = RE_ERROR;
                goto e1;
            }

            ptr += c;

            if(prev != NULL && prev->type == RE_SEQ) {
                b = (struct re_buf *)prev;
            }
            else {
                n = new_node(idc++, RE_SEQ);
                n->parent = parent;

                prev = n;
                *target = n;
                target = &n->next;

                b = (struct re_buf *)n;
            }

            buf_append_char(b,u);
        }

        if(n != NULL && negate) {
            n->negate = true;
            negate = false;
        }
    }

    *re_ret = re;

    return 0;

e1: re_destroy(re);

    return e;
}

int re_exec(struct re_node *re, char *str, bool recurse, struct re_match **match_ret) {
    int e;
    size_t i;
    struct re_match *m_first,
                    **target,
                    *m;

    i = 0;
    m_first = NULL;

    if(recurse == false) {
        e = match(re, str, &i, &m_first);
        if(e != RE_OK)
            return e;
    }
    else {
        e = RE_ERROR;
        target = &m_first;

        while(str[i] != '\0') {
            e = match(re, str, &i, &m);
            if(e != RE_OK) {
                ++i;
                continue;
            }

            *target = m;
            target = &m->next;
        }

        if(m_first == NULL)
            return RE_ERROR;
    }

    if(match_ret != NULL)
        *match_ret = m_first;
    else
        re_destroy_match(m_first);

    return RE_OK;
}

bool re_get_named_match(struct re_match *m, char *name, char **result_ret) {
    size_t i;

    for(i=0; i < m->n_captures; ++i) {
        if(m->names[i] != NULL && strcmp(m->names[i], name) == 0) {
            *result_ret = m->results[i];
            return true;
        }
    }

    return false;
}

void re_destroy(struct re_node *re) {
    struct re_node *n,
              *nn;
    struct re_group *g;
    struct re_buf *b;

    n=re;
    while(n != NULL) {
        if(n->child != NULL)
            re_destroy(n->child);

        if(n->type == RE_GROUP) {
            g = (struct re_group *)n;
            if(g->name != NULL)
                free(g->name);
        }
        else if(n->type == RE_SET ||
                n->type == RE_SEQ)
        {
            b = (struct re_buf *)n;
            free(b->str);
        }

        nn = n->next;
        free(n);
        n = nn;
    }
}

void re_destroy_match(struct re_match *m) {
    size_t i;
    struct re_match *mn;

    while(m != NULL) {
        for(i=0; i < m->n_captures; ++i) {
            if(m->names[i] != NULL)
                free(m->names[i]);

            free(m->results[i]);
        }

        mn = m->next;
        free(m);
        m = mn;
    }
}
