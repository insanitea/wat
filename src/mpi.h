#ifndef MPI_H
#define MPI_H

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>

#ifndef MPI_DIGIT_ALLOC_POOL
    #define MPI_DIGIT_ALLOC_POOL 4
#endif
#ifndef MPI_DIGIT_ALLOC_DEFAULT
    #define MPI_DIGIT_ALLOC_DEFAULT 8
#endif

#if defined(MPI_8BIT)
    typedef uint8_t mpi_digit_t;
    typedef uint16_t mpi_word_t;
#elif defined(MPI_16BIT)
    typedef uint16_t mpi_digit_t;
    typedef uint32_t mpi_word_t;
#elif defined(MPI_32BIT)
    typedef uint32_t mpi_digit_t;
    typedef uint64_t mpi_word_t;
#elif defined(MPI_64BIT) && defined(__SIZEOF_INT128__)
    typedef uint64_t mpi_digit_t;
    typedef __uint128_t mpi_word_t;
#else
    typedef unsigned int mpi_digit_t;
    typedef unsigned long int mpi_word_t;
#endif

#define MPI_DIGIT_BYTES sizeof(mpi_digit_t)
#define MPI_DIGIT_BITS (MPI_DIGIT_BYTES*8)
#define MPI_DIGIT_MAX ((mpi_digit_t)-1)
#define MPI_DIGIT_MSB1 ((mpi_word_t)1 << (MPI_DIGIT_BITS-1))

#define MPI_KARATSUBA_THRES 256 // in bits

enum mpi_status {
    MPI_OK,
    MPI_MALLOC,
    MPI_INPUT,
    MPI_RANDOM
};

enum mpi_random_flags {
    MPI_NONZERO    = 0x01,
    MPI_PRIME_SAFE = 0x02,
    MPI_PRIME_2MSB = 0x04,
    MPI_PRIME_BBS  = 0x08
};

typedef struct {
    bool neg;
    mpi_digit_t *buf;
    size_t top,
           size,
           max;
} mpi_t;

int mpi_init(mpi_t *x);
int mpi_init_multi(mpi_t *x, ...);
void mpi_clear(mpi_t *x);
void mpi_clear_multi(mpi_t *x, ...);
int mpi_realloc(mpi_t *x, size_t n_digits);
int mpi_grow(mpi_t *x, size_t n_digits);
int mpi_copy(mpi_t *dest, mpi_t *src);
void mpi_swap(mpi_t *a, mpi_t *b);
void mpi_truncate(mpi_t *x, size_t n_bits);
void mpi_zero(mpi_t *x);

size_t mpi_digit_size(mpi_t *x);
size_t mpi_byte_size(mpi_t *x);
#define mpi_bit_size(x) (mpi_byte_size(x)*MPI_DIGIT_BITS)
size_t mpi_bit_count(mpi_t *x);

int mpi_from_uint(mpi_t *x, uintmax_t v);
int mpi_to_uint(mpi_t *x, uintmax_t *v_ret);
int mpi_from_str(mpi_t *x, int base, char *str, char **ptr_ret);
int mpi_to_str(mpi_t *x, int base, char *dest, char **ptr_ret);
int mpi_radix_len(mpi_t *x, int base, size_t *len_ret);
int mpi_import(mpi_t *x, void *bin, size_t count, int digit_order, size_t size, int byte_order);
int mpi_export(mpi_t *x, void *bin, size_t count, int digit_order, size_t size, int byte_order);

int mpi_random(mpi_t *x, size_t n_bits, unsigned int flags);
int mpi_random_range(mpi_t *x, mpi_t *a, mpi_t *b, unsigned int flags);
int mpi_random_prime(mpi_t *x, unsigned int t, size_t size, unsigned int flags);

int mpi_abs(mpi_t *x, mpi_t *a);
int mpi_neg(mpi_t *x, mpi_t *a);
int mpi_diff(mpi_t *x, mpi_t *a, mpi_t *b);
#define mpi_incr(x) mpi_add_digit(x,x,1)
#define mpi_decr(x) mpi_sub_digit(x,x,1)
int mpi_add(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_add_digit(mpi_t *x, mpi_t *a, mpi_digit_t b);
int mpi_uadd(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_uadd_digit(mpi_t *x, mpi_t *a, mpi_digit_t b);
int mpi_sub(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_sub_digit(mpi_t *x, mpi_t *a, mpi_digit_t b);
int mpi_usub(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_usub_digit(mpi_t *x, mpi_t *a, mpi_digit_t b);
int mpi_mul(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_mul_digit(mpi_t *x, mpi_t *a, mpi_digit_t b);
int mpi_div(mpi_t *x1, mpi_t *x2, mpi_t *a, mpi_t *b);
int mpi_div_digit(mpi_t *x1, mpi_digit_t *x2, mpi_t *a, mpi_digit_t b);
#define mpi_mod(x,a,b) mpi_div(NULL,x,a,b)
int mpi_pow(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_powm(mpi_t *x, mpi_t *a, mpi_t *b, mpi_t *c);
int mpi_pow2(mpi_t *x, uintmax_t n);
#define mpi_sqr(x,a) mpi_mul(x,a,a)
int mpi_sqrt(mpi_t *x, mpi_t *a);
int mpi_log(mpi_digit_t *x, mpi_t *a, mpi_digit_t b);
int mpi_log_digit(mpi_digit_t *x, mpi_digit_t a, mpi_digit_t b);
int mpi_inv_mod(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_ext_gcd(mpi_t *x1, mpi_t *x2, mpi_t *x3, mpi_t *a, mpi_t *b);

int mpi_and(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_or(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_xor(mpi_t *x, mpi_t *a, mpi_t *b);
int mpi_lshift(mpi_t *x, mpi_t *a, uintmax_t b);
int mpi_lshift_digit(mpi_t *x, mpi_t *a, uintmax_t b);
int mpi_rshift(mpi_t *x, mpi_t *a, uintmax_t b);
int mpi_rshift_digit(mpi_t *x, mpi_t *a, uintmax_t b);

int mpi_cmp(mpi_t *a, mpi_t *b);
int mpi_ucmp(mpi_t *a, mpi_t *b);
int mpi_cmp_uint(mpi_t *a, uintmax_t b);
bool mpi_is_neg(mpi_t *x);
bool mpi_is_zero(mpi_t *x);
#define mpi_is_nonzero(x) (mpi_is_zero(x) == false)
bool mpi_is_one(mpi_t *x);
bool mpi_is_even(mpi_t *x);
bool mpi_is_odd(mpi_t *x);
int mpi_is_pow2(mpi_t *x, bool *result);
int mpi_is_prime(mpi_t *x, unsigned int t, bool *result);
bool mpi_digit_is_pow2(mpi_digit_t x);

#define mpi_printf(fmt, ...) mpi_fprintf(stdout, __VA_ARGS__)
ssize_t mpi_fprintf(FILE *fh, const char *fmt, ...);

void mpi_debug_digit(mpi_digit_t d);
void mpi_debug_fixed(uintmax_t x);
void mpi_debug(mpi_t *x);

#endif
