#ifndef WAT_TREE_H
#define WAT_TREE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

enum {
    TREE_NODE_DICT,
    TREE_NODE_LIST,
    TREE_NODE_BOOL,
    TREE_NODE_INT,
    TREE_NODE_FLOAT,
    TREE_NODE_STR,
    TREE_NODE_PTR
};

enum {
    TREE_OK,
    TREE_NOT_FOUND,
    TREE_BAD_PATH,
    TREE_BAD_TYPE,
    TREE_MALLOC,
    TREE_INVALID_NUMBER,
    TREE_UNEXPECTED_CHAR,
    TREE_UNEXPECTED_DELIM,
    TREE_EXPECTED_DELIM,
    TREE_EXPECTED_COLON,
    TREE_EXPECTED_DIGIT,
    TREE_UNCLOSED_COMMENT,
    TREE_UNCLOSED_QUOTE,
    TREE_UNCLOSED_LIST,
    TREE_UNCLOSED_DICT,
};

extern char *tree_error_str[];

struct tree_node {
    int type;
    char *name;

    union {
        bool b;
        long long i;
        long double f;
        char *s;
        void *ptr;
    } data;

    struct tree_node *parent,
                     *child,
                     *last_child,
                     *prev,
                     *next;
};

typedef int (tree_qsort_func)(const void *a, const void *b);

struct tree_node_mapping {
    char *path;
    int type;
    void *data_ret;
    bool found;
};

struct tree_error_pos {
    size_t line,
           offset;
};

#define tree_node_for_each(n,c) for((c)=(n)->child; (c) != NULL; (c)=(c)->next)

int  tree_node_new(struct tree_node **n_ret, int type, char *name);
void tree_node_destroy(struct tree_node *n);
void tree_node_attach(struct tree_node *n, struct tree_node *p);
void tree_node_attach_before(struct tree_node *n, struct tree_node *s);
void tree_node_attach_after(struct tree_node *n, struct tree_node *s);
void tree_node_detach(struct tree_node *n);
int  tree_node_set_name(struct tree_node *n, char *name);
int  tree_node_set_data(struct tree_node *n, void *data);

int  tree_descend(struct tree_node *n, char *path, int type, bool touch, struct tree_node **n_ret);
int  tree_set(struct tree_node *m, char *path, int type, void *data);
int  tree_set_multi(struct tree_node *n, struct tree_node_mapping *map, size_t len);
int  tree_get(struct tree_node *n, char *path, int type, void *data_ret);
int  tree_get_multi(struct tree_node *n, struct tree_node_mapping *map, size_t len);

int  tree_serialize(struct tree_node *n, bool pretty, char *indent_str, char **str_ret, size_t *len_ret);
int  tree_deserialize(struct tree_node **root_ret, char *buf, struct tree_error_pos *ep_out);
void tree_write(struct tree_node *n, bool pretty, char *indent_str, FILE *fh);
int  tree_read(struct tree_node **root_ret, FILE *fh, struct tree_error_pos *ep_out);

int  tree_qsort(struct tree_node *r, tree_qsort_func *sort_func);

#endif
