#include "time.h"
#include "string.h"

#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define debug(...) fprintf(stderr, __VA_ARGS__)
#else
#define debug(...)
#endif

char *time_units[] =    { "y","M","d","h","m","s","ms" },
     *time_unit_fmt[] = { "%u%s","%u%s","%u%s", "%u%s","%u%s","%u%s",".%03u%s" };

char *day_names[7] = {
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
};

char *month_names[12] = {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
};

uint8_t mday_counts[12] = {
    31, 28, 31, 30, // jan feb mar apr
    31, 30, 31, 31, // may jun jul aug
    30, 31, 30, 31  // sep oct nov dec
};

int sec_map[MSECONDS] = {
    // number of seconds in each offset unit
    [DAYS] = 86400,
    [HOURS] = 3600,
    [MINUTES] = 60,
    [SECONDS] =  1
};

int mul_map[SECONDS] = {
    // equivilent value for following offset unit
    [DAYS] =      24,  // hours
    [HOURS] =     60,  // minutes
    [MINUTES] =   60,  // seconds
};

unsigned int day_of_week(unsigned int year, unsigned int month, unsigned int day) {
    unsigned int y,m,x;

    y = 1900+year;
    m = month+1;
    x = m < 3 ? day+y-- : day+(y-2);

    return (23*m/9+x+4+y/4-y/100+y/400) % 7;
}

unsigned int day_of_year(unsigned int year, unsigned int month, unsigned int day) {
    unsigned int m,
                 d;
    bool ly;

    d = day;
    ly = is_leap_year(1900+year);

    for(m=0; m < month; ++m) {
        d += mday_counts[m];
        if(ly && m == FEBRUARY)
            ++d;
    }

    return d;
}

// --

int time_cmp(struct tm *t1, struct tm *t2) {
    int *u1,
        *u2,
        u;

    u1 = (int *)&t1->tm_sec;
    u2 = (int *)&t2->tm_sec;

    for(u=5; u >= 0; --u) {
        if(u1[u] > u2[u])
            return -1;
        else if(u1[u] < u2[u])
            return +1;
    }

    return 0;
}

ptime_t time_now(clockid_t clock) {
    struct timespec tv;
    memset(&tv, 0, sizeof(struct timespec));
    clock_gettime(clock, &tv);
    return tv.tv_sec + ((tv.tv_nsec / 1.0e6) / 1000);
}

int time_parse_offset(char *offset, struct tm *ref, ptime_t *offset_ret) {
    char *ptr,
         *end;
    int sign;
    ptime_t d;
    unsigned int v,
                 ny,y,
                 nM,M;
    int rM;
    size_t n;

    debug("\e[37m> \e[1;97mtime_parse_offset():\e[0m\n");

    ptr = offset;
    while(isspace(*ptr))
        ++ptr;

    // parse sign

    if(*ptr == '-') {
        ++ptr;
        sign = -1;
    }
    else if(*ptr == '+') {
        ++ptr;
        sign = 1;
    }
    else {
        sign = 1;
    }

    // get seconds

    d = 0;
    ny = 0;
    nM = 0;

    while(*ptr != '\0') {
        errno = 0;
        v = strtoull(ptr, &end, 10);
        if(errno != 0 || end == ptr) {
            debug("  .. \e[%u\e[0m\n", v);
            return 1;
        }

        ptr = end;
        for(n=0; ptr[n] != '\0' && isdigit(ptr[n]) == false; ++n);

        if(strncmp(ptr, time_units[YEARS], n) == 0) {
            ny += v;
        }
        else if(strncmp(ptr, time_units[MONTHS], n) == 0) {
            nM += v;
        }
        else if(strncmp(ptr, time_units[DAYS], n) == 0) {
            d += v*sec_map[DAYS];
        }
        else if(strncmp(ptr, time_units[HOURS], n) == 0) {
            d += v*sec_map[HOURS];
        }
        else if(strncmp(ptr, time_units[MINUTES], n) == 0) {
            d += v*sec_map[MINUTES];
        }
        else if(strncmp(ptr, time_units[SECONDS], n) == 0) {
            d += v;
        }
        else if(strncmp(ptr, time_units[MSECONDS], n) == 0) {
            d += v/1000.0;
        }
        else {
            debug("  .. %u\e[91m%.*s\e[0m\n", v, (int)n, ptr);
            return 1;
        }

        debug("  .. \e[97m%u\e[0m%.*s\n", v, (int)n, ptr);
        ptr += n;
    }

    // years/months handling

    if(ny > 0 || nM > 0) {
        // reference time required for years/months
        if(ref == NULL)
            return 1; 

        if(nM >= 12) {
            ny += nM/12;
            nM %= 12;
        }

        if(sign == -1) {
            // subtract years
            for(y=0; y < ny; ++y)
                d += days_in_year(ref->tm_year-y) * sec_map[DAYS];

            // subtract months
            for(M=0; M < nM; ++M) {
                rM = ref->tm_mon-M;
                if(rM < 0)
                    rM += 12;

                d += days_in_month(rM, ref->tm_year-y) * sec_map[DAYS];
                if(rM == 0)
                    ++y;
            }
        }
        else {
            // add years
            for(y=0; y < ny; ++y)
                d += days_in_year(ref->tm_year+y) * sec_map[DAYS];

            // add months
            for(M=0; M < nM; ++M) {
                rM = (ref->tm_mon+M)%12;
                d += days_in_month(rM, ref->tm_year+y) * sec_map[DAYS];
                if(rM == 11)
                    ++y;
            }
        }
    }

    if(sign == -1)
        d *= -1;

    debug("  offset=\e[97m%.03Lf\e[0m\n", d);

    *offset_ret = d;

    return 0;
}

void time_parse_diff(ptime_t diff, struct tm *ref, unsigned int base_unit, struct time_diff *d) {
    ptime_t f,i;
    unsigned long long int s,
                           ms,
                           days,
                           M,mM,wM,
                           diy,diM;
    long long int y;

    debug("\e[37m> \e[1;97mtime_parse_diff():\e[0m\n");
    debug("  diff=\e[97m%Lf\e[0m\n", diff);

    memset(d, 0, sizeof(struct time_diff));

    if(diff < 0) {
        diff = fabsl(diff);
        d->sign = -1;
    }
    else {
        d->sign = 1;
    }

    d->base = base_unit;

    f = modfl(diff,&i);

    s = i;
    ms = round(f*1000);
    if(ms == 1000) {
        ++s;
        ms = 0;
    }

    debug("  s/ms=\e[97m%llu.%03llu\e[0m\n", s, ms);

    days = s/sec_map[DAYS];

    if(base_unit == YEARS) {
        if(ref != NULL) {
            y = ref->tm_year;

            while(true) {
                diy = days_in_year(y);
                if(days < diy)
                    break;

                ++d->years;
                days -= diy;

                y += d->sign;
            }

            goto M;
        }
    }
    else if(base_unit == MONTHS) {
        if(ref != NULL) {
            y = ref->tm_year;
        M:  M = ref->tm_mon;

            if(d->sign == -1) {
                mM = 0;
                wM = 11;
            }
            else {
                mM = 11;
                wM = 0;
            }

            while(true) {
                diM = days_in_month(M,y);
                if(days < diM)
                    break;

                ++d->months;
                days -= diM;

                if(M == mM) {
                    y += d->sign;
                    M = wM;
                }
                else {
                    M += d->sign;
                }
            }
        }
    }
    else if(base_unit == DAYS) {
        goto d;
    }
    else if(base_unit == HOURS) {
        goto h;
    }
    else if(base_unit == MINUTES) {
        goto m;
    }
    else if(base_unit == SECONDS) {
        goto s;
    }
    else if(base_unit == MSECONDS) {
        goto ms;
    }

d:  d->days = days;
    s = s%sec_map[DAYS];
h:  d->hours = s/sec_map[HOURS];
    s = s%sec_map[HOURS];
m:  d->minutes = s/sec_map[MINUTES];
    s = s%sec_map[MINUTES];
s:  d->seconds = s;
ms: d->mseconds = ms;

    debug("  y=\e[97m%u\e[0m M=\e[97m%u\e[0m d=\e[97m%u\e[0m\n", d->years, d->months, d->days);
    debug("  h=\e[97m%u\e[0m m=\e[97m%u\e[0m s=\e[97m%u\e[0m ms=\e[97m%u\e[0m\n", d->hours, d->minutes, d->seconds, d->mseconds);
}

ssize_t time_snprint_diff(char *buf, size_t n, char *delim, struct time_diff *d, bool ltrim, bool rtrim) {
    unsigned int *units,
                 u1,
                 u2,
                 u;
    char *ptr;
    size_t nr,
           npt,
           np;

    ptr = buf;
    nr = n;
    npt = 0;

    // print sign if negative

    if(d->sign == -1) {
        ++npt;

        if(ptr != NULL) {
            if(npt+1 > nr)
                return npt;

            if(snprintf(ptr, nr, "-") < 0)
                return -1;

            ++ptr;
            --nr;
        }
    }

    // trim

    units = (unsigned int *) &d->years;

    u1 = d->base;
    u2 = MSECONDS;

    if(ltrim)
        for(; u1 < SECONDS && units[u1] == 0; ++u1);
    if(rtrim)
        for(; u2 > u1 && units[u2] == 0; --u2);

    // print units

    u = u1;
    while(true) {
        np = snprintf(NULL, 0, time_unit_fmt[u], units[u], time_units[u]);
        npt += np;

        if(ptr != NULL) {
            if(npt+1 > nr)
                return npt;

            if(snprintf(ptr, nr, time_unit_fmt[u], units[u], time_units[u]) < 0)
                return -1;

            ptr += np;
            nr -= np;
        }

        if(u == u2)
            break;

        ++u;

        np = snprintf(NULL, 0, "%s", delim);
        npt += np;

        if(ptr != NULL) {
            if(npt+1 > nr)
                return npt;

            if(snprintf(ptr, nr, "%s", delim) < 0)
                return -1;

            ptr += np;
            nr -= np;
        }
    }

    return npt;
}

int time_round(ptime_t *t, unsigned int precision, ptime_t threshold) {
    ptime_t s;
    time_t rem,
           val,
           max;

    if(precision < DAYS || precision > SECONDS)
        return 1;

    modfl(*t,&s);
    // remaining seconds past precision unit
    rem = fmod(s, sec_map[precision]);
    // native value of following unit
    val = precision < MINUTES ? rem/sec_map[precision+1] : rem;
    // max value of following unit
    max = mul_map[precision]*threshold;

    if(val >= max)
        s += sec_map[precision] - rem;
    else
        s -= rem;

    *t = s;

    return 0;
}

// STRPTIME

#ifdef WAT_STRPTIME
inline static
char * get_value_by_abbr_name(int *dest, char *str, char **names, size_t n, unsigned int base) {
    size_t i;

    for(i=0; i < n; ++i) {
        if(strncasecmp(str, names[i], 3) == 0) {
            *dest = base+i;
            return str+3;
        }
    }

    return NULL;
}

inline static
char * get_value_by_full_name(int *dest, char *str, char **names, size_t n, unsigned int base) {
    size_t i,
           len;

    for(i=0; i < n; ++i) {
        len = strlen(names[i]);
        if(strncasecmp(str, names[i], len) == 0) {
            *dest = base+i;
            return str+len;
        }
    }

    return NULL;
}

inline static
char * get_value(int *dest, char *str, uint8_t max_digits, int min_valid, int max_valid) {
    char vstr[max_digits+1],
         *endptr;
    unsigned int x;

    memset(vstr, 0, sizeof(vstr));
    strncpy(vstr, str, sizeof(vstr));

    errno = 0;
    x = strtoul(vstr, &endptr, 10);
    if(errno != 0 || x < min_valid || (max_valid > -1 && x > max_valid))
        return NULL;

    *dest = x;
    return str+(endptr-vstr);
}

char * strptime(char *str, char *fmt, struct tm *t) {
    int century = 0,
        short_year = -1,
        yweek = -1,
        yday = -1,
        wday = -1,
        fdoy,m,d,dim;
    bool monday_first = false,
         is_12h = false,
         is_pm = false;

    while(*fmt != '\0') {
        // format specifier
        if(*fmt == '%') {
            ++fmt;

            if(*fmt == 'F')
                str = strptime(str, "%y-%m-%d", t);
            else if(*fmt == 'D')
                str = strptime(str, "%m/%d/%y", t);
            else if(*fmt == 'T')
                str = strptime(str, "%H:%M:%S", t);
            else if(*fmt == 'R')
                str = strptime(str, "%H:%M", t);
            else if(*fmt == 'r')
                str = strptime(str, "%I:%M:%S %p", t);

#if defined _linux_ || defined _unix_
            // locale-specific (unix/linux only)
            else if(*fmt == 'c')
                str = strptime(str, nl_langinfo(D_T_FMT), t);
            else if(*fmt == 'x')
                str = strptime(str, nl_langinfo(D_FMT), t);
            else if(*fmt == 'X')
                str = strptime(str, nl_langinfo(T_FMT), t);
#endif

            // day of week name (abbreviated)
            else if(*fmt == 'a')
                str = get_value_by_abbr_name(&wday, str, day_names, DAY_MAX, 1);
            // day of week name (full)
            else if(*fmt == 'A')
                str = get_value_by_full_name(&wday, str, day_names, DAY_MAX, 1);
            // month name (abbreviated)
            else if(*fmt == 'b' || *fmt == 'h')
                str = get_value_by_abbr_name(&t->tm_mon, str, month_names, MONTH_MAX, 0);
            // month name (full)
            else if(*fmt == 'B')
                str = get_value_by_full_name(&t->tm_mon, str, month_names, MONTH_MAX, 0);

            // century
            else if(*fmt == 'C')
                str = get_value(&century, str, 2, 1,-1);
            // short year
            else if(*fmt == 'y')
                str = get_value(&short_year, str, 2, 0,99);
            // full year
            else if(*fmt == 'Y')
                str = get_value(&t->tm_year, str, 4, 1,-1);
            // month
            else if(*fmt == 'm') {
                str = get_value(&t->tm_mon, str, 2, 1,12);
                --t->tm_mon;
            }

            // day of year
            else if(*fmt == 'j')
                str = get_value(&yday, str, 3, 1,366);
            // day of month
            else if(*fmt == 'd' || *fmt == 'e')
                str = get_value(&t->tm_mday, str, 2, 1,-1);

            // week of year (1=sunday)
            else if(*fmt == 'U') {
                str = get_value(&yweek, str, 2, 1,53);
            }
            // day of week (1=sunday)
            else if(*fmt == 'u') {
                str = get_value(&wday, str, 1, 1,7);
            }
            // week of year (1=monday)
            else if(*fmt == 'W') {
                monday_first = true;
                str = get_value(&yweek, str, 2, 1,53);
            }
            // day of week (1=monday)
            else if(*fmt == 'w') {
                monday_first = true;
                str = get_value(&wday, str, 1, 1,7);
            }

            // hour (24h)
            else if(*fmt == 'H') {
                is_12h = false;
                str = get_value(&t->tm_hour, str, 2, 0,23);
            }
            // hour (12h)
            else if(*fmt == 'I') {
                is_12h = true;
                str = get_value(&t->tm_hour, str, 2, 1,12);
            }
            // minute
            else if(*fmt == 'M')
                str = get_value(&t->tm_min, str, 2, 0,59);
            // second
            else if(*fmt == 'S')
                str = get_value(&t->tm_sec, str, 2, 0,59);
            // am/pm
            else if(*fmt == 'p' || *fmt == 'P') {
                if(strncasecmp(str, "AM", 2) == 0) {
                    is_pm = false;
                    str += 2;
                }
                else if(strncasecmp(str, "PM", 2) == 0) {
                    is_pm = true;
                    str += 2;
                }
                else {
                    str = NULL;
                }
            }
            // literal %
            else if(*fmt == '%') {
                if(*str != *fmt)
                    return NULL;

                ++str;
            }

            if(str == NULL)
                return NULL; // invalid input
        }
        // literal
        else {
            if(*str != *fmt)
                return NULL;

            ++str;
        }

        ++fmt;
    }

    // convert short year
    if(short_year >= 0) {
        if(century == 0) {
            if(short_year >= 69)
                century = 20;
            else
                century = 21;
        }

        t->tm_year = (((century-1)*100)+short_year)-1900;
    }

    // set month+day from yday
    if(yweek >= 0) {
        // set yday from yweek+wday
        if(yday == -1) {
            if(wday == -1)
                wday = 1;

            if(monday_first) {
                if(yday < 7)
                    ++wday;
                else
                    wday = 1;
            }

            fdoy = day_of_week(t->tm_year, 0, 1);
            yday = ((yweek*7)+wday)-fdoy;
        }

        m = 0;
        d = yday;

        while(true) {
            dim = days_in_month(m, t->tm_year);
            if(d <= dim)
                break;

            ++m;
            d -= dim;
        }

        t->tm_mon = m;
        t->tm_mday = d;
    }

    if(wday >= 0)
        t->tm_wday = wday;
    if(yday >= 0)
        t->tm_yday = yday;

    if(is_12h) {
        // finalise to 24 hour format
        if(is_pm) {
            if(t->tm_hour < 12)
                t->tm_hour += 12;
        }
        else {
            if(t->tm_hour == 12)
                t->tm_hour = 0;
        }
    }

    return str;
}

#endif
