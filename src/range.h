#ifndef CLIP_RANGE_H
#define CLIP_RANGE_H

#define convert_range(val, src_min, src_max, dest_min, dest_max) \
  ( ( ((val - src_min) * (dest_max - dest_min)) / (src_max - src_min) ) + dest_min )
#define perc_to_range(perc, min, max) (min + (perc * (max - min)))
#define range_to_perc(val, min, max)  ((val - min) / (max - min))

#endif
