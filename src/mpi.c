#include "mpi.h"

#include <ctype.h>
#include <limits.h>
#include <sys/random.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

char *base32_lc = "abcdefghijklmnopqrstuvwxyz234567",
     *base32_uc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",
     *base36_lc = "0123456789abcdefghijklmnopqrstuvwxyz",
     *base36_uc = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
     *base64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz+/";

inline
int base_to_radix(int *base_ptr, char **radix_ret) {
    int base;
    bool uppercase;
    char *radix;

    base = *base_ptr;
    if(base < 0) {
        uppercase = true;
        base = llabs(base);
    }
    else {
        uppercase = false;
    }

    *base_ptr = base;

    if(base < 2 || base > 64)
        return MPI_INPUT;

    // specific radix just for base32
    if(base == 32) {
        if(uppercase)
            radix = base32_uc;
        else
            radix = base32_lc;
    }
    // base36 radix for bases <= 36
    else if(base <= 36) {
        if(uppercase)
            radix = base36_uc;
        else
            radix = base36_lc;
    }
    // base64 radix for bases <= 64
    else { // if(base <= 64)
        radix = base64;
    }

    *radix_ret = radix;

    return MPI_OK;
}

inline
int radix_value(char *radix, char ch, mpi_digit_t *val_ret) {
    size_t i;

    for(i=0; radix[i] != '\0'; ++i) {
        if(radix[i] == ch) {
            *val_ret = i;
            return MPI_OK;
        }
    }

    return MPI_INPUT;
}

inline
void trim(mpi_t *x, size_t size) {
    if(size < x->size)
        memset(x->buf+size, 0, (x->size-size)*MPI_DIGIT_BYTES);

    x->top = size > 0 ? size-1 : 0;
    x->size = size;
}

inline
void clamp(mpi_t *x, size_t size) {
    while(size > 0 && x->buf[size-1] == 0)
        --size;

    trim(x, size);
}

// GENERAL
// --------

int mpi_init(mpi_t *x) {
    memset(x, 0, sizeof(mpi_t));
    return mpi_realloc(x, MPI_DIGIT_ALLOC_DEFAULT);
}

int mpi_init_multi(mpi_t *x1, ...) {
    int e;
    va_list args,
            cu_args;
    mpi_t *x;
    size_t n;

    va_start(args, x1);

    for(x=x1,n=0; x != NULL; ++n) {
        e = mpi_init(x);
        if(e != MPI_OK)
            goto e1;

        x = va_arg(args, mpi_t *);
    }

    va_end(args);

    return MPI_OK;

e1: va_start(cu_args, x1);

    for(x=x1; n > 0; --n) {
        mpi_clear(x);
        x = va_arg(cu_args, mpi_t *);
    }

    va_end(cu_args);

    return e;
}

void mpi_clear(mpi_t *x) {
    if(x->buf != NULL)
        free(x->buf);

    memset(x, 0, sizeof(mpi_t));
}

void mpi_clear_multi(mpi_t *x, ...) {
    va_list args;

    va_start(args, x);

    while(x != NULL) {
        mpi_clear(x);
        x = va_arg(args, mpi_t *);
    }

    va_end(args);
}

int mpi_realloc(mpi_t *x, size_t n) {
    mpi_digit_t *tmp;

    if(n == 0) {
        mpi_clear(x);
        return MPI_OK;
    }

    n = ((n+(MPI_DIGIT_ALLOC_POOL-1))/MPI_DIGIT_ALLOC_POOL)*MPI_DIGIT_ALLOC_POOL;
    if(n == x->max)
        return MPI_OK;

    tmp = realloc(x->buf, n*MPI_DIGIT_BYTES);
    if(tmp == NULL)
        return MPI_MALLOC;

    if(n > x->max)
        memset(tmp+x->max, 0, (n-x->max)*MPI_DIGIT_BYTES);
    else
        trim(x,n);

    x->buf = tmp;
    x->max = n;

    return MPI_OK;
}

int mpi_grow(mpi_t *x, size_t n) {
    if(n > x->max)
        return mpi_realloc(x,n);

    return MPI_OK;
}

int mpi_copy(mpi_t *dest, mpi_t *src) {
    int e;

    e = mpi_grow(dest, src->size);
    if(e != MPI_OK)
        return e;

    dest->neg = src->neg;
    dest->size = src->size;
    dest->top = src->top;

    memcpy(dest->buf, src->buf, src->size*MPI_DIGIT_BYTES);
    memset(dest->buf+src->size, 0, (dest->max-src->size)*MPI_DIGIT_BYTES);

    return MPI_OK;
}

void mpi_swap(mpi_t *a, mpi_t *b) {
    mpi_t s;

    memcpy(&s, a, sizeof(mpi_t));
    memcpy(a, b, sizeof(mpi_t));
    memcpy(b, &s, sizeof(mpi_t));
}

void mpi_truncate(mpi_t *x, uintmax_t b) {
    size_t n_digits,
           n_bits;

    if(b > mpi_bit_size(x)) {
        mpi_zero(x);
        return;
    }

    n_digits = b/MPI_DIGIT_BITS;
    if(n_digits >= x->size) {
        mpi_zero(x);
        return;
    }

    x->size -= n_digits;
    x->top -= n_digits;

    n_bits = b%MPI_DIGIT_BITS;
    if(n_bits > 0)
        x->buf[x->top] &= ((mpi_digit_t)1 << n_bits)-1;

    clamp(x, x->size);
}

void mpi_zero(mpi_t *x) {
    x->neg = false;
    memset(x->buf, 0, x->size*MPI_DIGIT_BYTES);
    x->top = 0;
    x->size = 0;
}

// SIZES AND COUNTS
// -----------------

size_t mpi_digit_size(mpi_t *x) {
    return x->size;
}

size_t mpi_byte_size(mpi_t *x) {
    return x->size*MPI_DIGIT_BYTES;
}

size_t mpi_bit_count(mpi_t *x) {
    size_t c;
    mpi_digit_t d;

    if(mpi_is_zero(x))
        return 0;

    c = x->top*MPI_DIGIT_BITS;
    for(d=x->buf[x->top]; d != 0; d >>= 1)
        ++c;

    return c;
}

// CONVERSIONS
// ------------

int mpi_from_uint(mpi_t *x, uintmax_t ui) {
    int e;
    size_t max;
    ssize_t i;

    max = sizeof(ui)/MPI_DIGIT_BYTES;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    x->top = max-1;
    x->size = max;

    if(sizeof(mpi_digit_t) == sizeof(ui)) {
        x->buf[0] = ui;
    }
    else {
    #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        for(i=0; i < x->size; ++i) {
            x->buf[i] = ui;
            ui >>= MPI_DIGIT_BITS;
        }
    #else
        for(i=x->top; i >= 0; --i) {
            x->buf[i] = ui;
            ui >>= MPI_DIGIT_BITS;
        }
    #endif
    }

    clamp(x, x->size);

    return MPI_OK;
}

int mpi_to_uint(mpi_t *x, uintmax_t *ui_ret) {
    int e;
    uintmax_t ui;
    size_t n_bits,
           max_bits;
    ssize_t i;

    ui = 0;
    n_bits = mpi_bit_count(x);
    max_bits = sizeof(ui)*8;

    if(n_bits > max_bits) {
        e = MPI_INPUT;
        goto r1;
    }

    if(sizeof(mpi_digit_t) == sizeof(ui)) {
        ui = x->buf[0];
    }
    else {
        for(i=x->top; i >= 0; --i) {
            ui <<= MPI_DIGIT_BITS;
            ui |= x->buf[i];
        }
    }

    e = MPI_OK;
r1: *ui_ret = ui;

    return e;
}

int mpi_from_str(mpi_t *x, int base, char *src, char **ptr_ret) {
    int e;
    char *ptr,
         *radix;
    bool neg;
    size_t i;
    mpi_t t;
    mpi_digit_t v;

    ptr = src;

    // parse sign (if any)

    if(*ptr == '-') {
        neg = true;
        ++ptr;
    }
    else {
        neg = false;
        if(*ptr == '+')
            ++ptr;
    }

    // determine base if unspecified

    if(base == 0) {
        if(*ptr == '0') {
            ++ptr;
            if(*ptr == 'b') {
                base = 2;
                ++ptr;
            }
            else if(*ptr == 'x') {
                base = 16;
                ++ptr;
            }
            else {
                base = 8;
            }
        }
        else {
            base = 10;
        }
    }

    e = base_to_radix(&base, &radix);
    if(e != MPI_OK)
        goto r0;

    // parse digits

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    for(i=0; isalnum(ptr[i]); ++i) {
        mpi_mul_digit(&t, &t, base);

        e = radix_value(radix, ptr[i], &v);
        if(e != MPI_OK)
            goto r1;

        mpi_uadd_digit(&t,&t,v);
    }

    if(i == 0) {
        e = MPI_INPUT;
        goto r1;
    }

    if(mpi_is_zero(&t) == false)
        t.neg = neg;

    ptr += i;
    if(ptr_ret != NULL)
        *ptr_ret = ptr;

    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_to_str(mpi_t *x, int base, char *dest, char **ptr_ret) {
    int e;
    bool alloc;
    char *radix,
         *ptr,
         *head,
         c;
    mpi_t t;
    size_t n,i,j;
    mpi_digit_t v;

    if((alloc=(dest==NULL))) {
        if(ptr_ret == NULL)
            return MPI_INPUT;

        e = mpi_log(&v, x, base);
        if(e != MPI_OK)
            goto r0;

        dest = malloc(v);
        if(dest == NULL) {
            e = MPI_MALLOC;
            goto r1;
        }
    }

    e = base_to_radix(&base, &radix);
    if(e != MPI_OK)
        goto r0;

    if(mpi_is_zero(x)) {
        sprintf(dest, "%c", radix[0]);
        goto r0;
    }

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    e = mpi_copy(&t,x);
    if(e != MPI_OK)
        goto r1;

    ptr = dest;

    if(mpi_is_neg(&t))
        *(ptr++) = '-';

    head = ptr;

    for(n=0; mpi_is_zero(&t) == false; ++n) {
        e = mpi_div_digit(&t, &v, &t, base);
        if(e != MPI_OK)
            goto r1;

        if(v >= base) {
            e = MPI_INPUT;
            goto r1;
        }

        *(ptr++) = radix[v];
    }

    *ptr = '\0';
    for(i=0,j=n-1; i < j; ++i,--j) {
        c = head[i];
        head[i] = head[j];
        head[j] = c;
    }

    if(ptr_ret != NULL)
        *ptr_ret = alloc ? dest : ptr;

r1: mpi_clear(&t);
r0: if(alloc && e != MPI_OK)
        free(dest);

    return e;
}

int mpi_radix_len(mpi_t *x, int base, size_t *len_ret) {
    int e;
    mpi_digit_t t;
    size_t len;

    e = mpi_log(&t, x, base);
    if(e != MPI_OK)
        return e;

    len = x->neg+t+1;

    *len_ret = len;

    return MPI_OK;
}

int mpi_import(mpi_t *x, void *bin, size_t count, int digit_order, size_t size, int byte_order) {
    int e;
    size_t i,j;

    mpi_zero(x);

    for(i=0; i < count; ++i) {
        for(j=0; j < size; ++j) {
            e = mpi_lshift(x,x,8);
            if(e != MPI_OK)
                return e;

            x->buf[0] |= *( ((uint8_t *)bin) +
                            ((  digit_order < 0 ? i : (count-1-i)) * size) +
                             (   byte_order > 0 ? j : (size-1-j) ) );
        }
    }

    return MPI_OK;
}

int mpi_export(mpi_t *x, void *bin, size_t count, int digit_order, size_t size, int byte_order) {
    int e;
    mpi_t t;
    ssize_t i,j;
    uint8_t *byte;

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    e = mpi_copy(&t,x);
    if(e != MPI_OK)
        goto r1;

#define export_iter() { \
    byte = ((uint8_t *)bin)+((i*size)+j); \
    *byte = t.buf[0]; \
    mpi_rshift(&t,&t,8); \
}

    if(digit_order == -1) {
        if(byte_order > 0) {
            for(i=0; i < count; ++i) {
                for(j=size-1; j >= 0; --j)
                    export_iter();
            }
        }
        else {
            for(i=0; i < count; ++i) {
                for(j=0; j < size; ++j)
                    export_iter();
            }
        }
    }
    else {
        if(byte_order > 0) {
            for(i=count-1; i >= 0; --i) {
                for(j=size-1; j >= 0; --j)
                    export_iter();
            }
        }
        else {
            for(i=count-1; i >= 0; --i) {
                for(j=0; j < size; ++j)
                    export_iter();
            }
        }
    }

    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

// RANDOM
// -------

int mpi_random(mpi_t *x, size_t size, unsigned int flags) {
    int e;
    mpi_t t;
    size_t n_digits,
           n_bytes;
    ssize_t nr;

    if(size == 0) {
        mpi_zero(x);
        return MPI_OK;
    }

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    n_bytes = (size+7)/8;
    n_digits = (n_bytes+(MPI_DIGIT_BYTES-1))/MPI_DIGIT_BYTES;

    e = mpi_grow(&t, n_digits);
    if(e != MPI_OK)
        goto r1;

    // randomize with at least 1 or 1 too many bytes
    nr = getrandom(t.buf, n_bytes, GRND_RANDOM);
    if(nr < n_bytes)
        goto r1;

    if((flags & MPI_NONZERO) && mpi_is_zero(&t))
        t.buf[0] |= 1;

    clamp(&t, n_digits);
    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_random_range(mpi_t *x, mpi_t *a, mpi_t *b, unsigned int flags) {
    int e;
    mpi_t *s,u,t;
    size_t min_bits,
           max_bits,
           diff_bits;
    uintmax_t z;
    ssize_t nr;

    if(mpi_cmp(a,b) > 0) { // if(a > b)
        s = a;
        a = b;
        b = s;
    }

    e = mpi_init_multi(&u,&t, NULL);
    if(e != MPI_OK)
        goto r0;

    e = mpi_sub(&u,b,a);
    if(e != MPI_OK)
        goto r1;

    min_bits = mpi_bit_count(a);
    max_bits = mpi_bit_count(b);
    diff_bits = max_bits-min_bits;

    nr = getrandom(&z, sizeof(z), GRND_RANDOM);
    if(nr < sizeof(z)) {
        e = MPI_RANDOM;
        goto r1;
    }

    e = mpi_random(&t, diff_bits, flags);
    if(e != MPI_OK)
        goto r1;

    if(mpi_cmp(&t,a) < 0) {
        while(true) {
            e = mpi_add(&t,&t,&u);
            if(e != MPI_OK)
                goto r1;

            if(mpi_cmp(&t,a) >= 0)
                break;
        }
    }
    else if(mpi_cmp(&t,b) > 0) {
        while(true) {
            e = mpi_sub(&t,&t,&u);
            if(e != MPI_OK)
                goto r1;

            if(mpi_cmp(&t,b) <= 0)
                break;
        }
    }

    e = mpi_copy(x,&t);

r1: mpi_clear_multi(&u,&t, NULL);

r0: return e;
}

int mpi_random_prime(mpi_t *x, unsigned int t, size_t size, unsigned int flags) {
    int e;
    uint8_t *tmp,
            and,
            or_msb,
            or_msb_offset,
            or_lsb;
    size_t tmp_size;
    ssize_t nr;
    bool res;
    
    if(size <= 1 || t <= 0) {
        e = MPI_INPUT;
        goto r0;
    }

    if(flags & MPI_PRIME_SAFE)
        flags |= MPI_PRIME_BBS;

    tmp_size = (size>>3)+((size&7)?1:0);
    tmp = malloc(tmp_size);
    if(tmp == NULL) {
        e = MPI_MALLOC;
        goto r0;
    }

    and = (size&7) == 0 ? 0xFF : (0xFF >> (8-(size&7)));
    or_msb = 0;
    or_msb_offset = ((size&7) == 1) ? 1 : 0;

    if(flags & MPI_PRIME_2MSB)
        or_msb |= 0x80 >> ((9-size)&7);

    or_lsb = 1u;
    if(flags & MPI_PRIME_BBS)
        or_lsb |= 3u;

    while(true) {
        nr = getrandom(tmp, tmp_size, GRND_RANDOM);
        if(nr < tmp_size) {
            e = MPI_RANDOM;
            goto r1;
        }

        tmp[0] &= and;
        tmp[0] |= 1 << ((size-1)&7);
        tmp[or_msb_offset] |= or_msb;
        tmp[tmp_size-1] |= or_lsb;

        e = mpi_import(x, tmp, tmp_size, -1, 1, -1); // previous args: -1,1,+1
        if(e != MPI_OK)
            goto r1;

        e = mpi_is_prime(x, t, &res);
        if(e != MPI_OK)
            goto r1;

        if(res == false)
            continue;

        if(flags & MPI_PRIME_SAFE) {
            // (x-1)/2 is also prime
        
            e = mpi_usub_digit(x,x,1);
            if(e != MPI_OK)
                goto r1;

            e = mpi_div_digit(x,NULL,x,2);
            if(e != MPI_OK)
                goto r1;

            e = mpi_is_prime(x, t, &res);
            if(e != MPI_OK)
                goto r1;

            if(res)
                break;
        }
    }

    if(flags & MPI_PRIME_SAFE) {
        e = mpi_mul_digit(x,x,2);
        if(e != MPI_OK)
            goto r1;

        e = mpi_uadd_digit(x,x,1);
        if(e != MPI_OK)
            goto r1;
    }

r1: free(tmp);

r0: return e;
}

// ARITHMETIC
// -----------

int mpi_abs(mpi_t *x, mpi_t *a) {
    int e;

    e = mpi_copy(x,a);
    if(e != MPI_OK)
        return e;

    x->neg = false;

    return MPI_OK;
}

int mpi_neg(mpi_t *x, mpi_t *a) {
    int e;

    e = mpi_copy(x,a);
    if(e != MPI_OK)
        return e;

    x->neg = mpi_is_nonzero(a) && mpi_is_neg(a) == false;

    return MPI_OK;
}

int mpi_diff(mpi_t *x, mpi_t *a, mpi_t *b) {
    mpi_t *s;

    if(mpi_cmp(a,b) > 0) { // if(a > b)
        s = a;
        a = b;
        b = s;
    }

    return mpi_usub(x,b,a);
}

int mpi_add(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s;

    if(a->neg == b->neg) {
        e = mpi_uadd(x,a,b);
        if(e != MPI_OK)
            return e;
    }
    else {
        if(mpi_cmp(a,b) < 0) { // if(a < b) | NOTE: ucmp -> cmp
            s = a;
            a = b;
            b = s;
        }

        e = mpi_usub(x,a,b);
        if(e != MPI_OK)
            return e;
    }

    x->neg = a->neg;

    return MPI_OK;
}

int mpi_add_digit(mpi_t *x, mpi_t *a, mpi_digit_t b) {
    int e;

    if(a->neg) {
        e = mpi_usub_digit(x,a,b);
        if(e != MPI_OK)
            return e;
    }
    else {
        e = mpi_uadd_digit(x,a,b);
        if(e != MPI_OK)
            return e;
    }

    x->neg = a->neg;

    return MPI_OK;
}

int mpi_uadd(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s;
    size_t max,
           i;
    mpi_digit_t u;
    mpi_word_t w;

    if(a->size < b->size) { // if(a < b)
        s = a;
        a = b;
        b = s;
    }

    max = a->size+1;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    u = 0;

    for(i=0; i < b->size; ++i) {
        w = a->buf[i] + b->buf[i] + u;
        x->buf[i] = w;
        u = w >> MPI_DIGIT_BITS;
    }

    for(; i < a->size; ++i) {
        w = a->buf[i] + u;
        x->buf[i] = w;
        u = w >> MPI_DIGIT_BITS;
    }

    x->buf[i] = u;
    clamp(x, max);

    return MPI_OK;
}

int mpi_uadd_digit(mpi_t *x, mpi_t *a, mpi_digit_t b) {
    int e;
    size_t max,
           i;
    mpi_digit_t u;
    mpi_word_t w;

    max = a->size+1;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    u = b;

    for(i=0; i < a->size; ++i) {
        w = a->buf[i] + u;
        x->buf[i] = w;
        u = w >> MPI_DIGIT_BITS;
    }

    x->buf[i] = u;
    clamp(x, max);

    return MPI_OK;
}

int mpi_sub(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s;
    bool neg;

    if(a->neg != b->neg) {
        e = mpi_uadd(x,a,b);
        if(e != MPI_OK)
            return e;

        x->neg = a->neg;
    }
    else {
        if(mpi_ucmp(a,b) < 0) { // if(a < b)
            neg = a->neg == false;

            s = a;
            a = b;
            b = s;
        }
        else {
            neg = a->neg;
        }

        e = mpi_usub(x,a,b);
        if(e != MPI_OK)
            return e;

        x->neg = neg;
    }

    return MPI_OK;
}

int mpi_sub_digit(mpi_t *x, mpi_t *a, mpi_digit_t b) {
    int e;

    if(a->neg) {
        e = mpi_uadd_digit(x,a,b);
        if(e != MPI_OK)
            return e;
    }
    else {
        e = mpi_usub_digit(x,a,b);
        if(e != MPI_OK)
            return e;
    }

    x->neg = a->neg;

    return MPI_OK;
}

int mpi_usub(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s;
    size_t max,
           i;
    mpi_digit_t u;
    mpi_word_t w;

    if(a->size < b->size) { // if(a < b)
        s = a;
        a = b;
        b = s;
    }

    max = a->size;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    u = 0;
    for(i=0; i < b->size; ++i) {
        w = (a->buf[i] - b->buf[i]) - u;
        x->buf[i] = w;
        u = (w >> MPI_DIGIT_BITS) & 1;
    }
    for(; i < a->size; ++i) {
        w = a->buf[i] - u;
        x->buf[i] = w;
        u = (w >> MPI_DIGIT_BITS) & 1;
    }

    clamp(x, max);

    return MPI_OK;
}

int mpi_usub_digit(mpi_t *x, mpi_t *a, mpi_digit_t b) {
    int e;
    size_t max,
           i;
    mpi_digit_t u;
    mpi_word_t w;

    max = a->size;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    u = b;
    for(i=0; i < a->size; ++i) {
        w = a->buf[i] - u;
        x->buf[i] = w;
        u = (w >> MPI_DIGIT_BITS) & 1;
    }

    clamp(x, max);

    return MPI_OK;
}

#define KARATSUBA_MIN 8

int karatsuba(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    size_t m,m2,
           x1_size,
           y1_size;
    mpi_t x0,x1,
          y0,y1,
          t1,t2,t3,
          z0,z1;

    e = mpi_init_multi(&x0,&x1,&y0,&y1,&t1,&t2,&t3,&z0,&z1, NULL);
    if(e != MPI_OK)
        goto r0;

    if(a->size < b->size)
        m = a->size/2;
    else
        m = b->size/2;

    m2 = m*2;
    x1_size = a->size-m;
    y1_size = b->size-m;

    if((e = mpi_grow(&x0, m)) != MPI_OK ||
       (e = mpi_grow(&x1, a->size-m)) != MPI_OK ||
       (e = mpi_grow(&y0, m)) != MPI_OK ||
       (e = mpi_grow(&y1, b->size-m)) != MPI_OK)
    {
        goto r1;
    }

    memcpy(x0.buf, a->buf, m);
    memcpy(y0.buf, b->buf, m);
    memcpy(x1.buf, a->buf+m, x1_size);
    memcpy(y1.buf, b->buf+m, y1_size);

    clamp(&x0, m);
    clamp(&y0, m);
    clamp(&x1, x1_size);
    clamp(&y1, y1_size);

    if(// t1 = (x0+x1)*(y0+y1)
       (e = mpi_add(&t1, &x0, &x1)) != MPI_OK ||
       (e = mpi_add(&t2, &y0, &y1)) != MPI_OK ||
       (e = mpi_mul(&t1, &t1, &t2)) != MPI_OK ||
       // z0 = x0*y0
       // z1 = x1*y1
       (e = mpi_mul(&z0, &x0, &y0)) != MPI_OK ||
       (e = mpi_mul(&z1, &x1, &y1)) != MPI_OK ||
       // t1 = t1-t2
       // t2 = z0+z1
       (e = mpi_sub(&t1, &t1, &t2)) != MPI_OK ||
       (e = mpi_add(&t2, &z0, &z1)) != MPI_OK ||
       // t1 = t1 << m
       // z1 = z1 << m2
       (e = mpi_lshift_digit(&t1, &t1, m)) != MPI_OK ||
       (e = mpi_lshift_digit(&z1, &z1, m2)) != MPI_OK ||
       // t1 = t1+z0+z1
       (e = mpi_add(&t2, &z0, &z1)) != MPI_OK ||
       (e = mpi_add(&t1, &t1, &t2)) != MPI_OK)
    {
        goto r1;
    }

    e = mpi_copy(x,&t1);

r1: mpi_clear_multi(&x0,&x1,&y0,&y1,&t1,&t2,&z0,&z1, NULL);

r0: return e;
}

int mpi_mul(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t t;
    size_t max,
           i,j,k;
    mpi_word_t w;
    mpi_digit_t u;

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    max = a->size+b->size+1;

    e = mpi_grow(&t, max);
    if(e != MPI_OK)
        goto r1;

    for(i=0; i < a->size; ++i) {
        u=0;
        for(j=0,k=i; j < b->size; ++j,++k) {
            w = (mpi_word_t)t.buf[k] +
                ((mpi_word_t)a->buf[i] * b->buf[j]) + u;

            t.buf[k] = w;
            u = w >> MPI_DIGIT_BITS;
        }

        t.buf[k] = u;
    }

    clamp(&t, max);
    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_mul_digit(mpi_t *x, mpi_t *a, mpi_digit_t b) {
    int e;
    mpi_t t;
    size_t max,
           i;
    mpi_digit_t u;
    mpi_word_t w;

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    max = a->size+1;

    e = mpi_grow(&t, max);
    if(e != MPI_OK)
        goto r1;

    u = 0;
    for(i=0; i < a->size; ++i) {
        w = (mpi_word_t)t.buf[i] +
            ((mpi_word_t)a->buf[i] * b) + u;

        t.buf[i] = w;
        u = w >> MPI_DIGIT_BITS;
    }

    t.buf[i] = u;

    clamp(&t, max);
    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_div(mpi_t *x1, mpi_t *x2, mpi_t *a, mpi_t *b) {
    int e,r;
    mpi_t ta,tb,tq,q;
    size_t n,i;
    bool neg;

    r = mpi_ucmp(a,b);
    if(r < 0) {
        if(x1 != NULL)
            mpi_zero(x1);

        if(x2 != NULL && x2 != a) {
            e = mpi_copy(x2,a);
            if(e != MPI_OK)
                goto r0;
        }

        e = MPI_OK;
        goto r0;
    }
    else if(r == 0) {
        if(x1 != NULL)
            mpi_from_uint(x1,1);
        if(x2 != NULL)
            mpi_zero(x2);

        e = MPI_OK;
        goto r0;
    }

    mpi_init_multi(&ta,&tb,&tq,&q, NULL);

    e = mpi_from_uint(&tq,1);
    if(e != MPI_OK)
        goto r1;

    if((e = mpi_abs(&ta,a)) != MPI_OK ||
       (e = mpi_abs(&tb,b)) != MPI_OK)
    {
        goto r1;
    }

    n = mpi_bit_count(&ta)-mpi_bit_count(&tb);

    if((e = mpi_lshift(&tb,&tb,n)) != MPI_OK ||
       (e = mpi_lshift(&tq,&tq,n)) != MPI_OK)
    {
        goto r1;
    }

    for(i=0; i <= n; ++i) {
        if(mpi_ucmp(&tb,&ta) <= 0) {
            if((e = mpi_usub(&ta,&ta,&tb) != MPI_OK) ||
               (e = mpi_uadd(&q,&tq,&q)) != MPI_OK)
            {
                goto r1;
            }
        }

        mpi_rshift(&tb,&tb,1);
        mpi_rshift(&tq,&tq,1);
    }

    neg = a->neg != b->neg;

    // quotient
    if(x1 != NULL) {
        q.neg = neg && mpi_is_zero(&q) == false;

        e = mpi_copy(x1,&q);
        if(e != MPI_OK)
            goto r1;
    }

    // remainder
    if(x2 != NULL) {
        ta.neg = (mpi_is_zero(&ta) ? false : a->neg);

        e = mpi_copy(x2,&ta);
        if(e != MPI_OK)
            goto r1;
    }

r1: mpi_clear_multi(&ta,&tb,&tq,&q, NULL);

r0: return e;
}

int mpi_div_digit(mpi_t *x1, mpi_digit_t *x2, mpi_t *a, mpi_digit_t b) {
    int e;
    ssize_t i;
    size_t n;
    mpi_digit_t m,d;
    mpi_t q;
    mpi_word_t w;

    if(b == 0)
        return MPI_INPUT;

    if(b == 1 || mpi_is_zero(a)) {
        if(x1 != NULL) {
            e = mpi_copy(x1,a);
            if(e != MPI_OK)
                return e;
        }

        if(x2 != NULL)
            *x2 = 0;

        return MPI_OK;
    }
    else if(b == 2) {
        if(x1 != NULL) {
            e = mpi_copy(x1,a);
            if(e != MPI_OK)
                return e;

            mpi_rshift(x1,x1,1);
        }

        if(x2 != NULL)
            *x2 = mpi_is_odd(a) ? 1 : 0;

        return MPI_OK;
    }
    else if(mpi_digit_is_pow2(b)) {
        for(n=2,m=4; n < MPI_DIGIT_BITS && m != b; ++n,m<<=1);
        d = a->buf[0] & (m-1);

        if(x1 != NULL){
            e = mpi_copy(x1,a);
            if(e != MPI_OK)
                return e;

            mpi_rshift(x1,x1,n);
        }

        if(x2 != NULL)
            *x2 = d;

        return MPI_OK;
    }
//    TODO
//    else if(b == 3) {
//        return mpi_div_3(x1, a, b);
//    }

    // typical division

    e = mpi_init(&q);
    if(e != MPI_OK)
        goto r0;

    e = mpi_grow(&q, a->size);
    if(e != MPI_OK)
        goto r1;

    w=0;

    for(i=a->top; i >= 0; --i) {
        w = (w << MPI_DIGIT_BITS) | a->buf[i];
        if(w >= b) {
            d = w/b;
            w -= (mpi_word_t)d*b;
        }
        else {
            d = 0;
        }

        q.buf[i] = d;
    }

    if(x1 != NULL) {
        q.neg = a->neg;
        clamp(&q, a->size);

        e = mpi_copy(x1,&q);
        if(e != MPI_OK)
            goto r1;
    }

    if(x2 != NULL)
        *x2 = w;

r1: mpi_clear(&q);

r0: return e;
}

int mpi_pow(mpi_t *r, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t t,ta,tb;

    e = mpi_init_multi(&t,&ta,&tb, NULL);
    if(e != MPI_OK)
        goto r0;

    if((e = mpi_from_uint(&t,1)) != MPI_OK ||
       (e = mpi_copy(&ta,a)) != MPI_OK ||
       (e = mpi_copy(&tb,b)) != MPI_OK)
    {
        goto r1;
    }

    while(mpi_is_zero(&tb) == false) {
        if(tb.buf[0] & 1) {
            e = mpi_mul(&t,&t,&ta);
            if(e != MPI_OK)
                goto r1;
        }

        e = mpi_mul(&ta,&ta,&ta);
        if(e != MPI_OK)
            goto r1;

        mpi_rshift(&tb,&tb,1);
    }

    e = mpi_copy(r,&t);

r1: mpi_clear_multi(&t,&ta,&tb, NULL);

r0: return e;
}

int mpi_powm(mpi_t *x, mpi_t *a, mpi_t *b, mpi_t *c) {
    int e;
    mpi_t t,ta,tb;

    e = mpi_init_multi(&t,&ta,&tb, NULL);
    if(e != MPI_OK)
        goto r0;

    if((e = mpi_from_uint(&t,1)) != MPI_OK ||
       (e = mpi_copy(&ta,a)) != MPI_OK ||
       (e = mpi_copy(&tb,b)) != MPI_OK) 
    {
        goto r1;
    }

    while(mpi_is_zero(&tb) == false) {
        if(tb.buf[0] & 1) {
            // t = t*ta mod c
            if((e = mpi_mul(&t,&t,&ta)) != MPI_OK ||
               (e = mpi_mod(&t,&t,c)) != MPI_OK)
            {
                goto r1;
            }
        }

        // ta = ta*ta mod c
        if((e = mpi_mul(&ta,&ta,&ta)) != MPI_OK ||
           (e = mpi_mod(&ta,&ta,c)) != MPI_OK)
        {
            goto r1;
        }

        // tb >>= 1
        mpi_rshift(&tb,&tb,1);
    }

    e = mpi_copy(x,&t);

r1: mpi_clear_multi(&t,&ta,&tb, NULL);

r0: return e;
}

int mpi_pow2(mpi_t *x, uintmax_t n) {
    int e;
    size_t i,j,
           max;

    i = n/MPI_DIGIT_BITS;
    j = n%MPI_DIGIT_BITS;

    max = i+1;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    mpi_zero(x);

    x->buf[i] = 1 << j;
    x->top = i;
    x->size = n;

    return MPI_OK;
}

int mpi_sqrt(mpi_t *x, mpi_t *a) {
    int e;
    mpi_t low,
          high,
          mid,
          tmp;

    e = mpi_init_multi(&low, &high, &mid, &tmp, NULL);
    if(e != MPI_OK)
        goto r0;

    if((e = mpi_copy(&high, a)) != MPI_OK ||
       (e = mpi_rshift(&mid, &high, 1)) != MPI_OK ||
       (e = mpi_incr(&mid)) != MPI_OK)
    {
        goto r1;
    }

    while(mpi_cmp(&high, &low) > 0) {
        e = mpi_mul(&tmp, &mid, &mid);
        if(e != MPI_OK)
            goto r1;

        if(mpi_cmp(&tmp, a) > 0) {
            if((e = mpi_copy(&high, &mid)) != MPI_OK ||
               (e = mpi_decr(&high)) != MPI_OK)
            {
                goto r1;
            }
        }
        else {
            e = mpi_copy(&low, &mid);
            if(e != MPI_OK)
                goto r1;
        }

        if((e = mpi_sub(&mid, &high, &low)) != MPI_OK ||
           (e = mpi_rshift(&mid, &mid, 1)) != MPI_OK ||
           (e = mpi_add(&mid, &low, &mid)) != MPI_OK ||
           (e = mpi_incr(&mid)) != MPI_OK)
        {
            goto r1;
        }
    }

    e = mpi_copy(x, &low);

r1: mpi_clear_multi(&low, &high, &mid, &tmp, NULL);

r0: return e;
}

int mpi_log(mpi_digit_t *x, mpi_t *a, mpi_digit_t b) {
    int e,r;
    unsigned int low,
                 high,
                 mid;
    mpi_t b_low,
          b_high,
          b_mid,
          base,
          t;

    if(mpi_is_neg(a) || mpi_is_zero(a) || b < 2 || b > MPI_DIGIT_MAX)
        return MPI_INPUT;

//    if(a->size == 1)
//        return mpi_log_digit(x, a->buf[0], b);

    r = mpi_cmp_uint(a,b);
    if(r <= 0) {
        *x = r == 0;
        return MPI_OK;
    }

    e = mpi_init_multi(&b_low, &b_high, &b_mid, &base, &t, NULL);
    if(e != MPI_OK)
        goto r0;

    low = 0;
    high = 1;
    mpi_from_uint(&b_low, 1);
    mpi_from_uint(&b_high, b);

    while(mpi_cmp(&b_high, a) < 0) {
        low = high;
        e = mpi_copy(&b_low, &b_high);
        if(e != MPI_OK)
            goto r1;

        high <<= 1;
        e = mpi_sqr(&b_high, &b_high);
        if(e != MPI_OK)
            goto r1;
    }

    mpi_from_uint(&base, b);

    while((high-low) > 1) {
        mid = (high+low)>>1;

        if((e = mpi_from_uint(&t, mid-low)) != MPI_OK ||
           (e = mpi_pow(&t, &base, &t)) != MPI_OK ||
           (e = mpi_mul(&b_mid, &b_low, &t)) != MPI_OK)
        {
            goto r1;
        }

        r = mpi_cmp(a, &b_mid);
        if(r < 0) {
            high = mid;
            mpi_swap(&b_mid, &b_high);
        }
        else if(r > 0) {
            low = mid;
            mpi_swap(&b_mid, &b_low);
        }
        else {
            *x = mid;
            goto r1;
        }
    }

    *x = mpi_cmp(&b_high, a) == 0 ? high : low;

r1: mpi_clear_multi(&b_low, &b_high, &b_mid, &base, &t, NULL);

r0: return e;
}

int mpi_log_digit(mpi_digit_t *x, mpi_digit_t a, mpi_digit_t b) {
    // TODO
    return MPI_OK;
}

int mpi_inv_mod(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t t1,t2,j,k,p,q,r,s;

    // no inverse if b <= 0
    if(b->neg || mpi_is_zero(b)) {
        fprintf(stderr, "b is negative or zero; no inverse\n");
        e = MPI_INPUT;
        goto r0;
    }

    e = mpi_init_multi(&t1,&t2,&j,&k,&p,&q,&r,&s, NULL);
    if(e != MPI_OK)
        goto r0;

    // (t1,t2)=(a%b,b)
    if((e = mpi_mod(&t1,a,b)) != MPI_OK ||
       (e = mpi_copy(&t2,b)) != MPI_OK)
    {
        goto r1;
    }

    // no inverse if t1 and t2 are even
    if(mpi_is_even(&t1) && mpi_is_even(&t2)) {
        fprintf(stderr, "x or y is even; no inverse\n");
        e = MPI_INPUT;
        goto r1;
    }

    // ----

    // (j,k)=(t1,t2)
    if((e = mpi_copy(&j,&t1)) != MPI_OK ||
       (e = mpi_copy(&k,&t2)) != MPI_OK)
    {
        goto r1;
    }

    // (p,q,r,s) = (1,0,0,1)
    mpi_from_uint(&p,1);
    mpi_from_uint(&q,0);
    mpi_from_uint(&r,0);
    mpi_from_uint(&s,1);

    while(true) {
        while(mpi_is_even(&j)) {
            mpi_rshift(&j,&j,1); // j = j/2

            if(mpi_is_odd(&p) || mpi_is_odd(&q)) {
                // (p,q) = (p+t2,q-t1)
                if((e = mpi_add(&p,&p,&t2)) != MPI_OK ||
                   (e = mpi_sub(&q,&q,&t1)) != MPI_OK)
                {
                    goto r1;
                }
            }

            // (p,q) = (p/2,q/2)
            mpi_rshift(&p,&p,1);
            mpi_rshift(&q,&q,1);
        }

        while(mpi_is_even(&k)) {
            // k = k/2
            mpi_rshift(&k,&k,1);

            // if r or s is odd ..
            if(mpi_is_odd(&r) || mpi_is_odd(&s)) {
                // (r,s) = (r+t2,s-t1)
                if((e = mpi_add(&r,&r,&t2)) != MPI_OK ||
                   (e = mpi_sub(&s,&s,&t1) != MPI_OK))
                {
                    goto r1;
                }
            }

            // (r,s) = (r/2,s/2)
            mpi_rshift(&r,&r,1);
            mpi_rshift(&s,&s,1);
        }

        // if(j >= k)
        if(mpi_cmp(&j,&k) >= 0) {
            // (j,p,q) = (j-k,p-r,q-s)
            if((e = mpi_sub(&j,&j,&k)) != MPI_OK ||
               (e = mpi_sub(&p,&p,&r)) != MPI_OK ||
               (e = mpi_sub(&q,&q,&s)) != MPI_OK)
            {
                goto r1;
            }
        }
        else {
            // (k,r,s) = (k-j,r-p,s-q)
            if((e = mpi_sub(&k,&k,&j)) != MPI_OK ||
               (e = mpi_sub(&r,&r,&p)) != MPI_OK ||
               (e = mpi_sub(&s,&s,&q)) != MPI_OK)
            {
                goto r1;
            }
        }

        // if(j == 0)
        if(mpi_is_zero(&j))
            break;
    }

    // no inverse if k != 1
    if(mpi_is_one(&k) == false) {
        fprintf(stderr, "\e[0;91mk != 1; no inverse\e[0m\n");
        e = MPI_INPUT;
        goto r1;
    }

    // too small
    while(mpi_cmp_uint(&r,0) < 0) {
        e = mpi_add(&r,&r,b);
        if(e != MPI_OK)
            goto r1;
    }

    // too big
    while(mpi_ucmp(&r,b) >= 0) {
        e = mpi_sub(&r,&r,b);
        if(e != MPI_OK)
            goto r1;
    }

    e = mpi_copy(x,&r);
    if(e != MPI_OK)
        goto r1;

r1: mpi_clear_multi(&t1,&t2,&j,&k,&p,&q,&r,&s, NULL);

r0: return e;
}

int mpi_ext_gcd(mpi_t *x1, mpi_t *x2, mpi_t *x3, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s,u1,u2,u3,v1,v2,v3,t1,t2,t3,q,tmp;

    if(mpi_ucmp(a,b) < 0) {
        s = a;
        a = b;
        b = s;
    }

    e = mpi_init_multi(&u1,&u2,&u3,&v1,&v2,&v3,&t1,&t2,&t3,&q,&tmp, NULL);
    if(e != MPI_OK)
        goto r0;

    if((e = mpi_from_uint(&u1,1)) != MPI_OK ||
       (e = mpi_copy(&u3,a)) != MPI_OK ||
       (e = mpi_from_uint(&v2,1)) != MPI_OK ||
       (e = mpi_copy(&v3,b)) != MPI_OK)
    {
        goto r1;
    }

    while(mpi_is_zero(&v3) == false) {
        // q = u3/v3
        if((e = mpi_div(&q,NULL,&u3,&v3)) != MPI_OK ||
           // (t1,t2,t3) = (u1,u2,u3) - (v1,v2,v3)q
           (e = mpi_mul(&tmp,&v1,&q)) != MPI_OK ||
           (e = mpi_sub(&t1,&u1,&tmp)) != MPI_OK ||
           (e = mpi_mul(&tmp,&v2,&q)) != MPI_OK ||
           (e = mpi_sub(&t2,&u2,&tmp)) != MPI_OK ||
           (e = mpi_mul(&tmp,&v3,&q)) != MPI_OK ||
           (e = mpi_sub(&t3,&u3,&tmp)) != MPI_OK ||
           // (u1,u2,u3) = (v1,v2,v3)
           (e = mpi_copy(&u1,&v1)) != MPI_OK ||
           (e = mpi_copy(&u2,&v2)) != MPI_OK ||
           (e = mpi_copy(&u3,&v3)) != MPI_OK ||
           // (v1,v2,v3) = (t1,t2,t3)
           (e = mpi_copy(&v1,&t1)) != MPI_OK ||
           (e = mpi_copy(&v2,&t2)) != MPI_OK ||
           (e = mpi_copy(&v3,&t3)) != MPI_OK)
        {
            goto r1;
        }
    }

    if(mpi_is_neg(&u3)) {
        mpi_neg(&u1,&u1);
        mpi_neg(&u2,&u2);
        mpi_neg(&u3,&u3);
    }

    if((e = mpi_copy(x1,&u3)) != MPI_OK ||
       (e = mpi_copy(x2,&u2)) != MPI_OK ||
       (e = mpi_copy(x3,&u1)) != MPI_OK)
    {
        goto r1;
    }

r1: mpi_clear_multi(&u1,&u2,&u3,&v1,&v2,&v3,&t1,&t2,&t3,&q,&tmp, NULL);

r0: return e;
}

// BITWISE
// --------

int mpi_and(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s,t;
    size_t i;

    // swap if a < b
    if(mpi_ucmp(a,b) == -1) {
        s = a;
        a = b;
        b = s;
    }

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    e = mpi_grow(&t, b->size);
    if(e != MPI_OK)
        goto r1;

    for(i=0; i < b->size; ++i)
        t.buf[i] = a->buf[i] & b->buf[i];

    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_or(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s,t;
    size_t i;

    // swap if a < b
    if(mpi_ucmp(a,b) == -1) {
        s = a;
        a = b;
        b = s;
    }

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    e = mpi_grow(&t, a->size);
    if(e != MPI_OK)
        goto r1;

    for(i=0; i < b->size; ++i)
        t.buf[i] = a->buf[i] | b->buf[i];
    for(; i < a->size; ++i)
        t.buf[i] = a->buf[i];

    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_xor(mpi_t *x, mpi_t *a, mpi_t *b) {
    int e;
    mpi_t *s,t;
    size_t i;

    // swap if a < b
    if(mpi_ucmp(a,b) < 0) {
        s = a;
        a = b;
        b = s;
    }

    e = mpi_init(&t);
    if(e != MPI_OK)
        goto r0;

    e = mpi_grow(&t, a->size);
    if(e != MPI_OK)
        goto r1;

    for(i=0; i < b->size; ++i)
        t.buf[i] = a->buf[i] ^ b->buf[i];
    for(; i < a->size; ++i)
        t.buf[i] = a->buf[i];

    clamp(&t, a->size);
    e = mpi_copy(x,&t);

r1: mpi_clear(&t);

r0: return e;
}

int mpi_lshift(mpi_t *x, mpi_t *a, uintmax_t b) {
    int e;
    size_t max,
           n_bits,
           us,i,
           n_digits;
    mpi_digit_t u,uu;

    if(b == 0)
        return MPI_OK;

    if(x != a) {
        e = mpi_copy(x,a);
        if(e != MPI_OK)
            return e;
    }

    max = (mpi_bit_count(x)+b+(MPI_DIGIT_BITS-1))/MPI_DIGIT_BITS;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    n_bits = b%MPI_DIGIT_BITS;
    if(n_bits > 0) {
        u = 0;
        us = MPI_DIGIT_BITS-n_bits;

        for(i=0; i < x->size; ++i) {
            uu = x->buf[i] >> us;
            x->buf[i] = (x->buf[i] << n_bits) | u;
            u = uu;
        }

        if(u != 0) {
            x->buf[i] = u;
            ++x->top;
            ++x->size;
        }
    }

    n_digits = b/MPI_DIGIT_BITS;
    if(n_digits > 0) {
        e = mpi_lshift_digit(x, x, n_digits);
        if(e != MPI_OK)
            return e;
    }

    return MPI_OK;
}

int mpi_lshift_digit(mpi_t *x, mpi_t *a, uintmax_t b) {
    int e;
    size_t max;

    if(b == 0)
        return MPI_OK;

    max = a->size+b;

    e = mpi_grow(x, max);
    if(e != MPI_OK)
        return e;

    memmove(x->buf+b, a->buf, a->size*MPI_DIGIT_BYTES);
    memset(x->buf, 0, b*MPI_DIGIT_BYTES);
    x->top = a->top+b;
    x->size = a->size+b;

    return MPI_OK;
}

int mpi_rshift(mpi_t *x, mpi_t *a, uintmax_t b) {
    int e;
    size_t n_digits,
           n_bits,
           us;
    ssize_t i;
    mpi_digit_t u,uu;

    if(b == 0)
        return MPI_OK;

    if(x != a) {
        e = mpi_copy(x,a);
        if(e != MPI_OK)
            return e;
    }

    n_digits = b/MPI_DIGIT_BITS;
    if(n_digits > 0)
        mpi_rshift_digit(x, x, n_digits);

    n_bits = b%MPI_DIGIT_BITS;
    if(n_bits > 0) {
        u = 0;
        us = MPI_DIGIT_BITS-n_bits;

        for(i=x->top; i >= 0; --i) {
            uu = x->buf[i] << us;
            x->buf[i] = u | (x->buf[i] >> n_bits);
            u = uu;
        }
    }

    clamp(x, x->size);

    return MPI_OK;
}

int mpi_rshift_digit(mpi_t *x, mpi_t *a, uintmax_t b) {
    int e;

    if(b == 0) {
        return MPI_OK;
    }
    else if(b > a->size) {
        mpi_zero(x);
        return MPI_OK;
    }

    if(x != a) {
        e = mpi_copy(x,a);
        if(e != MPI_OK)
            return e;
    }

    x->top = a->top-b;
    x->size = a->size-b;
    memmove(x->buf, a->buf+b, x->size*MPI_DIGIT_BYTES);
    memset(x->buf+x->size, 0, (x->max-x->size)*MPI_DIGIT_BYTES);

    return MPI_OK;
}

// COMPARISONS
// ------------

int mpi_cmp(mpi_t *a, mpi_t *b) {
    if(a->neg && b->neg == false)
        return -1;
    else if(a->neg == false && b->neg)
        return +1;
    else
        return mpi_ucmp(a,b);
}

int mpi_ucmp(mpi_t *a, mpi_t *b) {
    ssize_t i;

    if(a->size < b->size)
        return -1;
    else if(a->size > b->size)
        return +1;

    for(i=a->top; i >= 0; --i) {
        if(a->buf[i] < b->buf[i])
            return -1;
        else if(a->buf[i] > b->buf[i])
            return +1;
    }

    return 0;
}

int mpi_cmp_uint(mpi_t *a, uintmax_t b) {
    size_t i,j;
    mpi_digit_t *v;

    v = (mpi_digit_t *)&b;

    for(i=0; i < a->size; ++i) {
        j = a->top-i;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        if(a->buf[j] < v[j])
            return -1;
        else if(a->buf[j] > v[j])
            return +1;
#else
        if(a->buf[j] < v[i])
            return -1;
        else if(a->buf[j] > v[i])
            return +1;
#endif
    }

    return 0;
}

bool mpi_is_neg(mpi_t *x) {
    return x->neg;
}

bool mpi_is_zero(mpi_t *x) {
    return x->size == 0;
}

bool mpi_is_one(mpi_t *x) {
    return x->size == 1 && x->buf[0] == 1;
}

bool mpi_is_even(mpi_t *x) {
    return x->size == 0 || (x->buf[0] & 1) == 0;
}

bool mpi_is_odd(mpi_t *x) {
    return x->size > 0 && (x->buf[0] & 1) == 1;
}

int mpi_is_pow2(mpi_t *x, bool *result) {
    int e;
    mpi_t t1,t2;

    if(mpi_is_zero(x)) {
        *result = false;
        e = MPI_OK;
        goto r0;
    }

    e = mpi_init_multi(&t1,&t2, NULL);
    if(e != MPI_OK)
        goto r0;

    if((e = mpi_usub_digit(&t1,x,1)) != MPI_OK ||
       (e = mpi_and(&t2,x,&t1)) != MPI_OK)
    {
        goto r1;
    }

    *result = mpi_is_zero(&t2);
r1: mpi_clear_multi(&t1,&t2, NULL);

r0: return e;
}

int mpi_is_prime(mpi_t *x, unsigned int t, bool *result) {
    // TODO
    *result = true;
    return MPI_OK;
}

bool mpi_digit_is_pow2(mpi_digit_t d) {
    return d != 0 && (d&(d-1)) == 0;
}

// PRINTING / DEBUGGING
// ---------------------

ssize_t mpi_fprintf(FILE *fh, const char *fmt, ...) {
    return 0;
}

void mpi_debug_digit(mpi_digit_t d) {
    ssize_t i;
    mpi_digit_t mask;

    fprintf(stderr, "\e[0;97m");
    for(i=MPI_DIGIT_BITS-1,mask=MPI_DIGIT_MSB1; i >= 0; --i,mask>>=1)
        fwrite(d & mask ? "1":"0", 1, 1, stderr);

    fprintf(stderr, "\e[0m");
}

void mpi_debug_fixed(uintmax_t x) {
    mpi_digit_t *d;
    size_t n_bits,
           n_digits;
    mpi_digit_t mask;
    ssize_t i,j;

    d = (mpi_digit_t *)&x;
    n_bits = sizeof(x)*8;
    n_digits = n_bits/MPI_DIGIT_BITS;

    mask = (mpi_digit_t)1 << (MPI_DIGIT_BITS-1);
    for(j=0; j < MPI_DIGIT_BITS; ++j) {
        fwrite(d[n_digits-1] & mask ? "1":"0", 1, 1, stderr);
        mask >>= 1;
    }

    for(i=n_digits-2; i >= 0; --i) {
        fwrite(" ", 1, 1, stderr);
        mask = (mpi_digit_t)1 << (MPI_DIGIT_BITS-1);
        for(j=0; j < MPI_DIGIT_BITS; ++j) {
            fwrite(d[i] & mask ? "1":"0", 1, 1, stderr);
            mask >>= 1;
        }
    }

    fprintf(stderr, "\n");
}

void mpi_debug(mpi_t *x) {
    size_t last;
    ssize_t i,j;

    last = x->max-1;

    if(x->size > 0) {
#ifdef MPI_DEBUG_BITS
        fprintf(stderr, "\e[0;37m");
        if(x->max > x->size) {
            for(i=last; i >= x->size; --i) {
                if(i < last)
                    fwrite(" ", 1, 1, stderr);

                for(j=0; j < MPI_DIGIT_BITS; ++j)
                    fwrite("x", 1, 1, stderr);
            }

            fwrite(" ", 1, 1, stderr);
        }

        for(i=x->top; i >= 0; --i) {
            if(i < x->top)
                fwrite(" ", 1, 1, stderr);

            mpi_debug_digit(x->buf[i]);
        }

        fprintf(stderr, "\e[0m");
#else
        fprintf(stderr, "\e[0;37m");
        for(i=x->max-1; i > x->top; --i)
            fprintf(stderr, "%0.*lx", (int)MPI_DIGIT_BYTES*2, (long)x->buf[i]);

        fprintf(stderr, "\e[0;93m");
        for(; i >= 0; --i)
            fprintf(stderr, "%0.*lx", (int)MPI_DIGIT_BYTES*2, (long)x->buf[i]);

        fprintf(stderr, "\e[0m");
#endif
    }
    else {
        fprintf(stderr, "\e[0;37m");

        for(i=last; i >= 0; --i) {
            if(i < last)
                fwrite(" ", 1, 1, stderr);

            for(j=0; j < MPI_DIGIT_BITS; ++j)
                fwrite("x", 1, 1, stderr);
        }

        fprintf(stderr, "\e[0m");
    }

    fprintf(stderr, " \e[37m(\e[0m\e[0;97m%c\e[0m \e[97m%zu\e[0m/\e[97m%zu\e[0m\e[37m)\e[0m\n", x->neg ? '-':'+', x->size, x->max);
}
