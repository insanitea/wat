#ifndef WAT_MAP_H
#define WAT_MAP_H

#include <stdbool.h>
#include <sys/types.h>

enum hmap_status {
    HMAP_OK,
    HMAP_NOT_FOUND,
    HMAP_INPUT,
    HMAP_MALLOC
};

struct hmap_key {
    char *name;
    void *data;
    void (*deref)(void *item);
};

struct hmap_slot {
    struct hmap_key *keys;
    size_t n_keys,
           n_keys_alloc;
};

struct hmap {
    struct hmap_slot *slots;
    size_t size,
           pool,
           n_items;
};

struct hmap_iter {
    size_t si,ki;
    struct hmap_slot *slot;
    struct hmap_key *key;
};

#define hmap_for_each(hm, v) \
    for(v.si=0; v.si < (hm)->size && (v.slot = &(hm)->slots[v.si]) != NULL; ++v.si) \
        for(v.ki=0; v.ki < v.slot->n_keys && (v.key = &v.slot->keys[v.ki]) != NULL; ++v.ki)

size_t hmap_hash(char *name, size_t n);
int hmap_init(struct hmap *hm, size_t size, size_t pool);
void hmap_clear(struct hmap *hm);
int hmap_resize(struct hmap *hm, size_t size);
int hmap_insert(struct hmap *hm, char *name, void *data, void *deref);
int hmap_merge(struct hmap *dest, struct hmap *src);
int hmap_remove(struct hmap *hm, char *name);
int hmap_update_key(struct hmap *hm, char *old_name, char *new_name);
int hmap_get(struct hmap *hm, char *name, void **data_ret);
size_t hmap_item_count(struct hmap *hm);

#endif
