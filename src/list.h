#ifndef WAT_LIST_H
#define WAT_LIST_H

#include <stdbool.h>
#include <sys/types.h>

enum list_status {
    LIST_OK,
    LIST_MALLOC,
    LIST_INVALID,
    LIST_NOT_FOUND
};

typedef void (list_deref_func)(void *);

struct list_item {
    void *ptr;
    list_deref_func *deref;
};

typedef int (list_qsort_func)(const void *, const void *);

struct list {
    struct list_item *items;
    size_t used_size,
           alloc_size,
           pool_size;
};

struct list_iter {
    size_t i;
    void *ptr;
};

#define list_deinit(l) list_realloc(l,0)
#define list_append(l,ptr,deref) list_insert(l, (l)->used_size, ptr, deref)
#define list_append_multi(l,n,ptr,derefs) list_insert_multi(l, (l)->used_size, n, (void **)ptrs, derefs)
#define list_prepend(l,ptr,deref) list_insert(l, 0, ptr, deref)
#define list_prepend_multi(l,n,ptrs,deref) list_insert_multi(l, 0, n, ptrs, deref)

#define list_for_each(l,v) \
  for(v.i=0; v.i < (l)->used_size && (v.ptr=(l)->items[v.i].ptr) >= NULL; ++v.i)

int  list_init(struct list *l, ssize_t alloc_size, size_t pool_size);
int  list_realloc(struct list *l, size_t size);
int  list_grow(struct list *l, size_t size);
void list_clear(struct list *l, bool dealloc);
void list_size(struct list *l, size_t *used, size_t *alloc);

int  list_find(struct list *l, void *ptr, size_t *pos_ret);
int  list_get(struct list *l, size_t i, void **ptr_ret);

int  list_insert(struct list *l, size_t pos, void *item, list_deref_func *deref);
int  list_set(struct list *l, size_t pos, void *ptr, list_deref_func *deref);
int  list_remove(struct list *l, void *ptr);
int  list_splice(struct list *l, size_t pos, size_t nr, size_t ni, void **ptrs, list_deref_func **deref);

void list_qsort(struct list *l, list_qsort_func *qsort_func);
int  list_qsort_str_asc(const void *ap, const void *bp);
int  list_qsort_str_desc(const void *ap, const void *bp);
int  list_qsort_lli_asc(const void *ap, const void *bp);
int  list_qsort_lli_desc(const void *ap, const void *bp);

#endif
