#ifndef WAT_STRING_H
#define WAT_STRING_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef WAT_STRSPLIT_MEMPOOL
  #define WAT_STRSPLIT_MEMPOOL 8
#endif

//
// general
//

size_t strbincount(char *str, ssize_t len);
bool   strendswith(char *str, char *substr, ssize_t n1, ssize_t n2);
char * strjoin(char *a, char *b, char *delim);
#ifndef _POSIX_C_SOURCE
char * strndup(char *str, ssize_t n);
#endif
void   strrandlex(char *str, size_t n, char *lex, ssize_t lex_len);
#ifdef NO_OLDNAMES
void   strrev(char *str, ssize_t len);
#endif
int    strscancmp(char *str, char *substr, size_t *len_ret);
double strsimil(char *a, char *b);
size_t strstrc(char *str, char *substr);
char * strstrip(char *str, ssize_t len, char *chars, bool dup, size_t *len_ret);
char * strstripl(char *str, ssize_t len, char *chars, bool dup, size_t *len_ret);
char * strstripr(char *str, ssize_t len, char *chars, bool dup, size_t *len_ret);

//
// buffers
//

#define strv_cut(src,a,b) strv_copy((src)+(a),(b)-(a))
char ** strv_copy(char **buf, ssize_t len);
void    strv_free(char **buf, ssize_t len);
char *  strv_join(char **buf, ssize_t len, char *delim, char *prefix, char *postfix);
size_t  strv_len(char **buf);
int     strv_remove(char **buf, ssize_t len, size_t i, size_t r, size_t *len_ret);
char ** strv_slice(char **buf, ssize_t len, size_t a, ssize_t b);

//
// strrepl
//

struct strrepl_map {
    char *substr,
         *repl;
    size_t substr_len,
           repl_len;
    ssize_t diff;
};

size_t strrepl(char **str_ptr, ssize_t len, size_t *alloc_len_ptr,
               size_t replc, struct strrepl_map *replv, size_t start, ssize_t end);

//
// strsplit
//

#define strsplit(str, delim, count_ret)  strnsplit(str, -1, -1, delim, count_ret)
#define strsplitq(str, delim, count_ret) strnsplitq(str, -1, -1, delim, count_ret)
char ** strnsplit(char *str, ssize_t n, ssize_t max, char *delim, size_t *count_ret);
char ** strnsplitq(char *str, ssize_t n, ssize_t max, char *delim, size_t *count_ret);

// 
// strtoken
//

struct strtoken_s {
    char *str;
    ssize_t len;
    unsigned int flags;
    char save;
    size_t tail,
           next;
};

enum strtoken_flags {
    STRTOKEN_DUP = 1,
    STRTOKEN_STICKY = 2
};

void   strtoken_init(struct strtoken_s *s, char *str, ssize_t len, unsigned int flags);
void   strtoken_clear(struct strtoken_s *s);
int    strtoken(struct strtoken_s *s, char *delim, char **tk_ret, size_t *len_ret);

#endif
