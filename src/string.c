#include "string.h"
#include "list.h"
#include "hmap.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

inline static
int check_quote(char c) {
    if(c == '\'')
        return 1;
    else if(c == '\"')
        return 2;
    else
        return 0;
}

inline static
bool haschar(char *ptr, char c) {
    for(; *ptr != '\0'; ++ptr) {
        if(*ptr == c)
            return true;
    }

    return false;
}

//
// general
//

size_t strbincount(char *str, ssize_t len) {
    size_t count,
           i;
    char c;

    if(len == -1)
        len = strlen(str);

    count = 0;
    for(i=0; i < len; ++i) {
        c = str[i];
        // match control chars (except HT/LF/VT/CR) or DEL
        if((c <= 0x1F && c != 0x09 && c != 0x0A && c != 0x0B && c != 0x0D) || c == 0x7F)
            ++count;
    }

    return count;
}

bool strendswith(char *str, char *substr, ssize_t n1, ssize_t n2) {
    if(n1 == -1)
        n1 = strlen(str);
    if(n2 == -1)
        n2 = strlen(substr);

    return strncmp((str+n1)-1-n2, substr, n1) == 0;
}

char * strjoin(char *a, char *b, char *delim) {
    char *new;
    size_t a_len,
           b_len,
           delim_len,
           new_size;

    a_len = strlen(a);
    b_len = strlen(b);
    delim_len = strlen(delim);
    new_size = a_len + b_len + delim_len + 1;

    new = malloc(new_size);
    if(new == NULL)
        return NULL;

    snprintf(new, new_size, "%s%s%s", a, delim, b);
    return new;
}

#ifndef _POSIX_C_SOURCE

char * strndup(char *str, ssize_t n) {
    char *new;

    if(n < 0)
        n = strlen(str);

    new = malloc(n+1);
    if(new == NULL)
        return NULL;

    memcpy(new, str, n);
    new[n] = '\0';

    return new;
}

#endif

void strrandlex(char *ptr, size_t n, char *lex, ssize_t lex_len) {
    size_t i;

    if(lex_len < 0)
        lex_len = strlen(lex);

    for(i=0; i < n; ++i)
        ptr[i] = lex[rand()%lex_len];

    ptr[i] = '\0';
}

size_t strrepl(char **str_ptr, ssize_t len, size_t *alloc_len_ptr, size_t replc, struct strrepl_map *replv, size_t start, ssize_t end) {
    char *str,
         *ptr,
         *tmp;
    size_t alloc_len,
           needed_len,
           i,j,r,
           adv;

    str = *str_ptr;

    if(len < 0)
        len = strlen(str);
    if(end < 0)
        end = len;

    needed_len = 0;
    for(i=0; i < replc; ++i) {
        replv[i].substr_len = strlen(replv[i].substr);
        replv[i].repl_len = strlen(replv[i].repl);
        replv[i].diff = replv[i].repl_len - replv[i].substr_len;

        needed_len += len+(replv[i].diff * strstrc(str,replv[i].substr));
    }

    if(alloc_len_ptr != NULL)
        alloc_len = *alloc_len_ptr;
    else
        alloc_len = len;

    if(needed_len > alloc_len) {
        // grow string to accomodate difference
        tmp = realloc(str, needed_len+1);
        if(tmp == NULL)
            return -1;

        str = tmp;
        ptr = str+i;

        *str_ptr = str;
        if(alloc_len_ptr != NULL)
            *alloc_len_ptr = needed_len;
    }

    for(i=start,ptr=str+i; i < end; i+=adv,ptr+=adv) {
        adv = 1;
        for(j=0; j < replc; ++j) {
            if(strscancmp(ptr, replv[j].substr, NULL) != 0)
                continue;

            if(replv[j].diff > 0) {
                // push contents to the right
                memmove(ptr+replv[j].diff, ptr, (len+1)-replv[j].diff);
                // replace substring
                memcpy(ptr, replv[j].repl, replv[j].repl_len);
            }
            else if(replv[j].diff < 0) {
                // push contents to the left
                r = i+replv[j].substr_len;
                memmove(ptr+replv[j].repl_len, str+r, (len+1)-r);
                // replace substring
                memcpy(ptr, replv[j].repl, replv[j].repl_len);
            }

            len += replv[j].diff;
            end += replv[j].diff;
            adv = replv[j].repl_len;

            break;
        }
    }

    if(alloc_len_ptr != NULL)
        *alloc_len_ptr = alloc_len;

    return len;
}

#ifdef NO_OLDNAMES
void strrev(char *str, ssize_t len) {
    size_t half,
           i,j;
    char tmp;

    if(len < 0)
        len = strlen(str);

    half = len/2;

    for(i=0, j = len-1; i < half; ++i, --j) {
        tmp = str[i];
        str[i] = str[j];
        str[j] = tmp;
    }
}
#endif

int strscancmp(char *str, char *substr, size_t *len_ret) {
    size_t i;

    for(i=0; substr[i] != '\0'; ++i) {
        if(str[i] > substr[i])
            return str[i]-substr[i];
        else if(str[i] < substr[i])
            return substr[i]-str[i];
    }

    if(len_ret != NULL)
        *len_ret = i;

    return 0;
}

double strsimil(char *a, char *b) {
    size_t a_len,
           b_len,
           tmp_len,
           hits,
           i,j;
    char *tmp;

    a_len = strlen(a);
    b_len = strlen(b);

    if(a_len == 0 && b_len == 0)
        return 1.0;

    if(a_len > b_len) {
        tmp = a;
        a = b;
        b = tmp;

        tmp_len = a_len;
        a_len = b_len;
        b_len = tmp_len;
    }

    hits = 0;
    for(i=0; i < a_len; ++i) {
        for(j=0; j < b_len; ++j) {
            if(strncmp(a+i,b+j,2) == 0) {
                ++hits;
                break;
            }
        }
    }

    return (2.0 * hits) / (a_len + b_len);
}

size_t strstrc(char *str, char *substr) {
    size_t count,
           i,
           adv;

    if(*substr == '\0')
        return 1;

    count=0;
    for(i=0; str[i] != '\0'; i+=adv) {
        if(strscancmp(str+i, substr, &adv) == 0)
            ++count;
        else
            adv = 1;
    }

    return count;
}

char * strstrip(char *str, ssize_t len, char *chars, bool dup, size_t *len_ret) {
    size_t l,r,i;

    if(len < 0)
        len = strlen(str);
    if(chars == NULL)
        chars = " \t\n\v\f\r";

    l=0;
    for(i=0; i < len; ++i) {
        if(haschar(chars, str[i]))
            ++l;
        else
            break;
    }

    r=0;
    for(i=1; i < len; ++i) {
        if(haschar(chars, str[len-i]))
            ++r;
        else
            break;
    }

    len -= l+r;
    str += l;

    if(dup) {
        str = strndup(str, len);
        if(str == NULL)
            return NULL;
    }

    if(len_ret != NULL)
        *len_ret = len;

    return str;
}

char * strstripl(char *str, ssize_t len, char *chars, bool dup, size_t *len_ret) {
    size_t l,i;

    if(len < 0)
        len = strlen(str);
    if(chars == NULL)
        chars = " \t\n\v\f\r";

    l=0;
    for(i=0; i < len; ++i) {
        if(haschar(chars, str[i]))
            ++l;
        else
            break;
    }

    len -= l;
    str += l;

    if(dup) {
        str = strndup(str, len);
        if(str == NULL)
            return NULL;
    }

    if(len_ret != NULL)
        *len_ret = len;

    return str;
}

char * strstripr(char *str, ssize_t len, char *chars, bool dup, size_t *len_ret) {
    size_t r;
    ssize_t i;

    if(len < 0)
        len = strlen(str);
    if(chars == NULL)
        chars = " \t\n\v\f\r";

    r=0;
    for(i=1; i < len; ++i) {
        if(haschar(chars, str[len-i]))
            ++r;
        else
            break;
    }

    len -= r;

    if(dup) {
        str = strndup(str, len);
        if(str == NULL)
            return NULL;
    }

    if(len_ret != NULL)
        *len_ret = len;

    return str;
}

//
// string vectors
//

char ** strv_copy(char **v, ssize_t len) {
    char **copy;
    size_t i;

    if(len < 0)
        len = strv_len(v);

    copy = malloc(sizeof(char *) * (len+1));
    if(copy == NULL)
        return NULL;

    for(i=0; i < len; ++i)
        copy[i] = strdup(v[i]);

    copy[len] = NULL;

    return copy;
}

void strv_free(char **v, ssize_t len) {
    size_t i;

    if(len < 0) {
        for(i=0; v[i] != NULL; ++i)
            free(v[i]);
    }
    else {
        for(i=0; i < len; ++i)
            free(v[i]);
    }

    free(v);
}

char * strv_join(char **v, ssize_t len, char *delim, char *prefix, char *postfix) {
    size_t size,
           max,
           n,i;
    char *str,
         *ptr;

    if(len < 0)
        len = strv_len(v);

    size = (strlen(delim)*(len-1))+1;
    for(i=0; i < len; ++i)
        size += strlen(v[i]);
    if(prefix != NULL)
        size += strlen(prefix);
    if(postfix != NULL)
        size += strlen(postfix);

    str = malloc(size);
    if(str == NULL)
        return NULL;

    ptr = str;
    max = size;

    if(prefix != NULL) {
        n = snprintf(ptr, max, "%s", prefix);
        ptr += n;
        max -= n;
    }
    
    if(len > 0) {
        n = snprintf(ptr, max, "%s", v[0]);
        ptr += n;
        max -= n;

        for(i=1; i < len; ++i) {
            n = snprintf(ptr, max, "%s", delim);
            ptr += n;
            max -= n;

            n = snprintf(ptr, max, "%s", v[i]);
            ptr += n;
            max -= n;
        }
    }

    if(postfix != NULL) {
        n = snprintf(ptr, max, "%s", postfix);
        ptr += n;
        max -= n;
    }

    return str;
}

size_t strv_len(char **v) {
    size_t len,
           i;

    len = 0;
    for(i=0; v[i] != NULL; ++i)
        ++len;

    return len;
}

int strv_remove(char **v, ssize_t len, size_t i, size_t r, size_t *len_ret) {
    size_t e,j,
           new_len;

    if(len < 0)
        len = strv_len(v);

    if(i >= len)
        return 1;

    e = i+r;
    if(e > len)
        return 1;

    for(j=i; j < e; ++j)
        free(v[j]);

    new_len = len-r;

    memmove(v+i, v+e, sizeof(char *)*(len-e));
    memset(v+new_len, 0, sizeof(char *)*r);

    if(len_ret != NULL)
        *len_ret = new_len;

    return 0;
}

char ** strv_slice(char **v, ssize_t len, size_t a, ssize_t b) {
    if(len < 0)
        len = strv_len(v);

    if(b < 0)
        b = len;
    if(a >= len || b <= a)
        return NULL;

    return strv_copy(v+a, b-a);
}

//
// strsplit
//

char ** strnsplit(char *str, ssize_t n, ssize_t max, char *delim, size_t *count_ret) {
    char **buf,
         **tmp,
         *tk;
    size_t delim_len,
           a,b,z,len,
           c,ac;

    if(n == 0 || max == 0 || *delim == '\0')
        goto r1;

    buf = NULL;
    a=0; b=0;
    c=0; ac=0;

    while(true) {
        if(b == n || str[b] == '\0') {
            // end of string; tokenize once more
            max = c+1;
            goto tokenize;
        }
        else if(strscancmp(str+b, delim, &delim_len) == 0) {
            z = delim_len;
            goto tokenize;
        }

        ++b;
        continue;

    tokenize:
        len = b-a;

        if(ac == 0 || c == (ac-1)) {
            ac += WAT_STRSPLIT_MEMPOOL;

            tmp = realloc(buf, sizeof(char *) * ac);
            if(tmp == NULL)
                goto e1;

            buf = tmp;
        }

        tk = strndup(str+a, len);
        if(tk == NULL)
            goto e1;

        buf[c++] = tk;
        if(c == max)
            break;

        a = b+z;
        b = a;
    }

    if(c == 0) {
    r1: c = 1;
        ac = 2;

        buf = malloc(sizeof(char *) * ac);
        if(buf == NULL)
            goto e1;

        buf[0] = strdup(str);
        if(buf[0] == NULL)
            goto e1;
    }

    memset(buf+c, 0, sizeof(char *) * (ac-c));

    if(count_ret != NULL)
        *count_ret = c;

    return buf;

e1: if(buf != NULL)
        strv_free(buf, c);

    return NULL;
}

char ** strnsplitq(char *str, ssize_t n, ssize_t max, char *delim, size_t *count_ret) {
    char **buf,
         **tmp,
         *tk;
    size_t delim_len,
           a,b,z,len,
           c,ac,
           tail,
           tail_len,
           new_tail_len;
    unsigned int qc,q;
    bool insert,
         insert_next;

    if(n == 0 || max == 0 || *delim == '\0')
        goto r1;

    buf = NULL;
    a=0; b=0; q=0;
    c=0; ac=0;
    insert = true;
    insert_next = false;

    while(true) {
        if(b == n || str[b] == '\0') { // if end of string
            if(q != 0) {
                // unclosed quote; unset quotation, decrement a, set b to a+1, reiterate.
                q = 0;
                b = (--a)+1;
                continue;
            }
            else if(b > a) {
                // unclean state; tokenize only once more.
                max = insert ? c+1 : c;
                goto tokenize;
            }
            else {
                break;
            }
        }
        else if(q == 0 && strscancmp(str+b, delim, &delim_len) == 0) {
            z = delim_len;
            insert_next = true; // tokens after delimiters should always be inserted
            goto tokenize;
        }
        else {
            qc = check_quote(str[b]);
            if(qc != 0) {    // if this is a quote
                if(q == 0) { // if no active quotation
                    q = qc;  // set active quotation
                    if(b > a) {
                        // unclean state
                        //
                        // currently:
                        // "wat, 'no'" -> [ "wat" ] (insert_next=true)
                        //      ^^^
                        //      abz
                        //
                        // after tokenize:
                        // "wat, 'no'" -> [ "wat", " " ] (insert=true)
                        //        ^
                        //        a
                        //        b
                        //
                        // continued:
                        // "wat, 'no'" -> [ "wat", " " ]
                        //        ^ ^^
                        //        a bz
                        //
                        // subsequently:
                        // "wat, 'no'" -> [ "wat", " no" ]
                        //           ^
                        //           a
                        //           b

                        z = 1;
                        goto tokenize;
                    }
                    else {
                        // clean state
                        //
                        // currently:
                        // "'wat ',no" -> [ ] (insert=true)
                        //  ^
                        //  a
                        //  b
                        //
                        // after:
                        // "'wat ',no" -> [ ]
                        //   ^
                        //   a
                        //   b

                        a = b+1;
                        b = a;
                        continue;
                    }
                }
                // if this is an end quote
                else if(qc == q) {
                    q = 0;
                    z = 1;
                    goto tokenize;
                }
            }
        }

        ++b;
        continue;

    tokenize:
        len = b-a;

        if(insert) {
            if(ac == 0 || c == (ac-1)) {
                ac += WAT_STRSPLIT_MEMPOOL;

                tmp = realloc(buf, sizeof(char *) * ac);
                if(tmp == NULL)
                    goto e1;

                buf = tmp;
            }

            tk = malloc(len+1);
            if(tk == NULL)
                goto e1;

            memcpy(tk, str+a, len);
            tk[len] = 0;

            tail = c;
            tail_len = len;

            buf[c++] = tk;
            if(c == max)
                break;
        }
        else if(len > 0) {
            new_tail_len = tail_len+len;

            tk = realloc(buf[tail], new_tail_len+1);
            if(tk == NULL)
                goto e1;

            memcpy(tk+tail_len, str+a, len);
            tk[new_tail_len] = '\0';

            tail_len = new_tail_len;

            buf[tail] = tk;
        }

        insert = insert_next;
        insert_next = false;

        a = b+z;
        b = a;
    }

    if(c == 0) {
    r1: c = 1;
        ac = 2;

        buf = malloc(sizeof(char *) * (ac+1));
        if(buf == NULL)
            goto e1;

        buf[0] = strdup(str);
        if(buf[0] == NULL)
            goto e1;
    }

    memset(buf+c, 0, sizeof(char *) * (ac-c));

    if(count_ret != NULL)
        *count_ret = c;

    return buf;

e1: strv_free(buf, c);

    return NULL;
}

//
// strtoken
//

void strtoken_init(struct strtoken_s *s, char *str, ssize_t len, unsigned int flags) {
    memset(s, 0, sizeof(struct strtoken_s));
    s->str = str;
    s->len = len;
    s->flags = flags;
}

void strtoken_clear(struct strtoken_s *s) {
    if(s->save != 0)
        s->str[s->tail] = s->save;

    memset(s, 0, sizeof(struct strtoken_s));
}

int strtoken(struct strtoken_s *s, char *delim, char **tk_ret, size_t *len_ret) {
    char *tk,
         *ptr;
    size_t i,
           delim_len;

    if((s->flags & STRTOKEN_STICKY) == 0 && s->save != 0) {
        s->str[s->tail] = s->save;
        s->save = 0;
    }

    tk = s->str+s->next;

    if(delim == NULL || *delim == '\0') {
        s->next = s->len;
        goto s2;
    }

    if(s->len < 0) {
        for(i=s->next,ptr=tk; *ptr != '\0'; ++i,++ptr) {
            if(strscancmp(ptr, delim, &delim_len) == 0)
                goto s1;
        }
    }
    else {
        for(i=s->next,ptr=tk; i < s->len && *ptr != '\0'; ++i,++ptr) {
            if(strscancmp(ptr, delim, &delim_len) == 0)
                goto s1;
        }
    }

    return 1;

s1: s->save = s->str[i];
    s->tail = i;
    s->next = i+delim_len;

    s->str[i] = '\0';

s2: if(tk_ret != NULL) {
        if(s->flags & STRTOKEN_DUP) {
            tk = strdup(tk);
            if(tk == NULL)
                return -1;
        }

        *tk_ret = tk;
    }

    if(len_ret != NULL)
        *len_ret = ptr-tk;

    return 0;
}
