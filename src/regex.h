#ifndef CLIP_REGEX_H
#define CLIP_REGEX_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

// 
// SYNTAX
// -------
//
// ()     group
// (#:)   named group
// (!:)   noncaptured group
// []     set
//
//  /a     alphabetical
//  /A     non-alphabetical
//  /n     numerical
//  /N     non-numerical
//  /x     hexadecimal
//  /X     non-hexadecimal
//  /b     alphanumerical
//  /B     non-alphanumerical
//  /v     printable
//  /V     unprintable
//  /s     whitespace
//  /S     non-whitespace
//  /p     punctuation
//
//  *      repeat >= 0
//  ?      repeat <= 1
//  +      repeat >= 1
//
//  {n}    repeat n times
//  {n,m}  repeat at least n times, up to m times
//
//  &      greedy bias
//  !      logical not
//  |      logical or
//
//  ^      line start
//  $      line end
//  <      word start
//  >      word end
//

enum re_status {
    RE_OK,
    RE_ERROR,
    RE_MALLOC,
};

enum re_node_type {
    RE_GROUP,
    RE_ANY,
    RE_SET,
    RE_SEQ,
    RE_LINE_START,
    RE_LINE_END,
    RE_WORD_START,
    RE_WORD_END,
    RE_MAX_TYPE
};

extern char * re_error_str[];
extern char * re_type_str[];

struct re_node {
    size_t id;
    int type;
    bool greedy,
         negate,
         or_next;
    size_t rc_min;
    ssize_t rc_max;
    struct re_node *parent,
                   *child,
                   *prev,
                   *next;
};

struct re_match {
    char **names,
         **results;
    size_t n_captures;
    struct re_match *next;
};

int re_compile(char *expr, struct re_node **re_ret);
int re_exec(struct re_node *re, char *str, bool recurse,
            struct re_match **match_ret);
bool re_get_capture(struct re_match *m, char *name, char **result_ret);
void re_destroy(struct re_node *re);
void re_destroy_match(struct re_match *m);

#endif
