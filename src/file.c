#include "file.h"
#include "string.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

int fsanitizepath(char *path, char **path_ret) {
    bool is_abs;
    char **pathv;
    size_t pathc,
           i,j,r;

    is_abs = path[0] == '/';
    path = strstrip(path, -1, "/", false, NULL);

    pathv = strsplitq(path, "/", &pathc);
    if(pathv == NULL)
        return -1;

    i=0;
    while(i < pathc) {
        if(strcmp(pathv[i], ".") == 0) {
            r = 1;
        }
        else if(strcmp(pathv[i], "..") == 0) {
            if(i > 0) {
                // remove the previous component and the '..'
                i -= 1;
                r = 2;
            }
            else {
                // out of bounds, only remove the '..'
                r = 1;
            }
        }
        else {
            ++i;
            continue;
        }

        for(j=i; i < pathc && strcmp(pathv[j], ".") == 0; ++j)
            ++r;

        strv_remove(pathv, pathc, i, r, &pathc);
    }

    if(pathc > 0)
        path = strv_join(pathv, pathc, "/", is_abs ? "/" : NULL, NULL);
    else if(is_abs)
        path = strdup("/");
    else
        path = strdup(".");

    strv_free(pathv, pathc);

    if(path == NULL)
        return -1;

    *path_ret = path;

    return 0;
}

char * fjoinpath(char *a, char *b) {
    char *path,
         *ptr;
    size_t a_len,
           a_max,
           b_len,
           size;

    a_len = strlen(a);
    a_max = a_len-1;

    while(a_len > 0 && a[a_max] == '/') {
        a[a_max--] = '\0';
        --a_len;
    }

    while(*b == '/')
        ++b;

    b_len = strlen(b);

    size = a_len+b_len+2;
    path = malloc(size);
    if(path == NULL)
        return NULL;

    ptr = path;
    memcpy(ptr, a, a_len);
    ptr += a_len;
    *ptr = '/';
    ptr += 1;
    memcpy(ptr, b, b_len);
    ptr += b_len;
    *ptr = '\0';

    return path;
}

int freadall(void *data_ret, size_t *len_ret, FILE *fh) {
    int e;
    size_t len;
    char *data;

    fseek(fh, 0, SEEK_END);
    len = ftell(fh);

    data = malloc(len+1);
    if(data == NULL) {
        e = -1;
        goto e0;
    }

    if(fseek(fh, 0, SEEK_SET) == -1) {
        e = 1;
        goto e1;
    }

    fread(data, len, 1, fh);
    if(ferror(fh)) {
        e = 1;
        goto e1;
    }

    data[len] = '\0';

    *((void **)data_ret) = (void *)data;
    if(len_ret != NULL)
        *len_ret = len;

    return 0;

e1: free(data);

e0: return e;
}

int freadall_pooled(void *data_ret, size_t *len_ret, size_t *alloc_len_ret, FILE *fh) {
    int e;
    char *data,
         *tmp;
    size_t len,
           alloc_len;

    data = NULL;
    len = 0;
    alloc_len = 0;

    while(true) {
        alloc_len += WAT_FILE_FREADALL_POOL;

        tmp = realloc(data, alloc_len+1);
        if(tmp == NULL) {
            e = -1;
            goto e1;
        }

        data = tmp;
        len += fread(data+len, 1, WAT_FILE_FREADALL_POOL, fh);

        if(ferror(stdin)) {
            e = 1;
            goto e1;
        }
        else if(feof(stdin)) {
            break;
        }
    }

    memset(data+len, 0, (alloc_len-len)+1);

    *((void **)data_ret) = (void *)data;
    if(len_ret != NULL)
        *len_ret = len;
    if(alloc_len_ret != NULL)
        *alloc_len_ret = alloc_len;

    return 0;

e1: free(data);

    return e;
}

int freadline(char **line_ret, size_t *len_ret, size_t *alloc_len_ret, FILE *fh) {
    int e;
    char *buf,
         *tmp;
    size_t len,
           alloc_len;

    buf = NULL;
    len = 0;
    alloc_len = 0;

    while(true) {
        if(len == alloc_len) {
            alloc_len += WAT_FILE_FREADLINE_POOL;

            tmp = realloc(buf, alloc_len+1);
            if(tmp == NULL) {
                e = -1;
                goto e1;
            }

            buf = tmp;
        }

        fread(buf+len, 1, 1, fh);
        if(ferror(fh)) {
            e = 1;
            goto e1;
        }

        if(buf[len] == '\n')
            break;

        ++len;
        if(feof(fh))
            break;
    }

    memset(buf+len, 0, (alloc_len-len)+1);

    *line_ret = buf;
    if(len_ret != NULL)
        *len_ret = len;
    if(alloc_len_ret != NULL)
        *alloc_len_ret = alloc_len;

    return 0;

e1: free(buf);

    return e;
}
