#include "utf8.h"

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

//
// UTF-8
//

int utf8_byte_count(uint32_t code, size_t *byte_count_ret) {
    size_t c;

    // reserved for UTF-16; not valid
    if(code >= 0xD800 && code <= 0xDFFF)
        return UTF8_INVALID;

    // ascii
    if(code <= 0x7F)
        c=1;
    // multi-byte
    else if(code <= 0x07FF)
        c=2;
    else if(code <= 0xFFFF)
        c=3;
    else if(code <= 0x10FFFF)
        c=4;
    // invalid
    else
        return UTF8_INVALID;

    *byte_count_ret = c;

    return UTF8_OK;
}

int utf8_encode_char(uint32_t code, char *ptr, size_t *byte_count_ret) {
    int e;
    size_t i,c;

    e = utf8_byte_count(code, &c);
    if(e != UTF8_OK)
        return e;

    // set continuation bytes
    for(i=c-1; i > 0; --i) {
        ptr[i] = UTF8_C1 | ((char)code & UTF8_M2);
        code >>= 6;
    }

    // set start byte
    if(c == 1)
        ptr[0] = (char)code;
    else if(c == 2)
        ptr[0] = UTF8_C2 | ((char)code & UTF8_M2);
    else if(c == 3)
        ptr[0] = UTF8_C3 | ((char)code & UTF8_M3);
    else // if(c == 4)
        ptr[0] = UTF8_C4 | ((char)code & UTF8_M4);

    // terminate sequence
    ptr[c] = '\0';

    if(byte_count_ret != NULL)
        *byte_count_ret = c;

    return UTF8_OK;
}

int utf8_decode_char(char *ptr, uint32_t *code_ret, size_t *byte_count_ret) {
    size_t c,m,i;
    uint32_t code;

    static char cb_mask[] = {
        UTF8_M1,
        UTF8_M2,
        UTF8_M3,
        UTF8_M4
    };

    // ascii
    if((unsigned)*ptr <= 0x7F)
        c = 1;
    // multi-byte
    else if((*ptr & UTF8_C3) == UTF8_C2)
        c = 2;
    else if((*ptr & UTF8_C4) == UTF8_C3)
        c = 3;
    else if((*ptr & UTF8_C5) == UTF8_C4)
        c = 4;
    // invalid
    else
        return UTF8_INVALID;

    m = c-1;
    code = ptr[0] & cb_mask[m];
    for(i=1; i < c; ++i) {
        // validate continuation
        if(ptr[i] == '\0' || (ptr[i] & UTF8_C2) != UTF8_C1)
            return UTF8_INVALID;

        code <<= 6;
        code |= ptr[i] & UTF8_M1;
    }

    if(code_ret != NULL)
        *code_ret = code;
    if(byte_count_ret != NULL)
        *byte_count_ret = c;

    return UTF8_OK;
}

size_t utf8_strlen(char *str, size_t *byte_len_ret) {
    size_t i,j,c;

    for(i=0,j=0; str[j] != '\0'; ++i,j+=c) {
        if(utf8_decode_char(str+j, NULL, &c) == UTF8_INVALID)
            c = 1;
    }

    if(byte_len_ret != NULL)
        *byte_len_ret = j;

    return i;
}

int utf8_validate(char *str, size_t *len_ret, size_t *byte_len_ret) {
    bool valid;
    size_t i,j,c;

    valid = true;
    for(i=0,j=0; str[j] != '\0'; ++i,j+=c) {
        if(utf8_decode_char(str+j, NULL, &c) == UTF8_INVALID) {
            c = 1;
            valid = false;
        }
    }

    if(len_ret != NULL)
        *len_ret = i;
    if(byte_len_ret != NULL)
        *byte_len_ret = j;

    if(valid == false)
        return UTF8_INVALID;

    return UTF8_OK;
}

size_t utf8_normalize(char *str, char repl) {
    int e;
    size_t i,j,c;

    for(i=0,j=0; str[j] != '\0'; ++i,j+=c) {
        e = utf8_decode_char(str+j, NULL, &c);
        if(e == UTF8_INVALID)
            c = 1;
        
        if(c == 1)
            str[i] = str[j];
        else
            str[i] = repl;
    }

    str[i] = '\0';

    return i;
}

int utf8_to_utf32(char *str, uint32_t **buf_ret, size_t *len_ret, size_t *byte_len_ret) {
    size_t len;
    uint32_t *buf;
    size_t i,j,c;
    uint32_t code;

    len = utf8_strlen(str, NULL);
    buf = malloc(sizeof(uint32_t)*(len+1));
    if(buf == NULL)
        return UTF8_MALLOC;

    for(i=0,j=0; i < len; ++i,j+=c) {
        if(utf8_decode_char(str+j, &code, &c) == UTF8_INVALID) {
            buf[i] = str[j];
            goto e1;
        }

        buf[i] = code;
    }

    buf[len] = 0;

    *buf_ret = buf;
    if(len_ret != NULL)
        *len_ret = len;
    if(byte_len_ret != NULL)
        *byte_len_ret = j;

    return UTF8_OK;

e1: free(buf);

    return UTF8_INVALID;
}

int utf8_offset_to_byte(char *str, size_t offset, size_t *byte_ret) {
    size_t i,j,c;

    for(i=0,j=0; i < offset; ++i,j+=c) {
        if(str[j] == '\0')
            return UTF8_RANGE;

        if(utf8_decode_char(str+j, NULL, &c) == UTF8_INVALID)
            c = 1;
    }

    *byte_ret = j;

    return UTF8_OK;
}

int utf8_byte_to_offset(char *str, size_t byte, size_t *offset_ret) {
    size_t i,j,c;

    for(i=0,j=0; j < byte; ++i,j+=c) {
        if(str[j] == '\0')
            return UTF8_RANGE;

        if(utf8_decode_char(str+j, NULL, &c) == UTF8_INVALID)
            c = 1;
    }

    *offset_ret = i;

    return UTF8_OK;
}

int utf8_strsimil(char *a, char *b, double *result_ret) {
    int e;
    size_t a_len,
           b_len;
    uint32_t *a_buf,
             *b_buf;
    double s;

    if(*a == '\0' && *b == '\0')
        return 1.0;

    e = utf8_to_utf32(a, &a_buf, &a_len, NULL);
    if(e != UTF8_OK)
        goto e0;

    e = utf8_to_utf32(b, &b_buf, &b_len, NULL);
    if(e != UTF8_OK)
        goto e1;

    s = utf32_strsimil(a_buf, a_len, b_buf, b_len);

    free(a_buf);
    free(b_buf);

    *result_ret = s;

    return UTF8_OK;

e1: free(a_buf);
e0: return e;
}

//
// UTF-32
//

size_t utf32_strlen(uint32_t *str32) {
    size_t i;
    for(i=0; str32[i] != 0; ++i);
    return i;
}

double utf32_strsimil(uint32_t *a, ssize_t a_len, uint32_t *b, ssize_t b_len) {
    size_t hits,
           i,j;

    if(a_len < 0)
        a_len = utf32_strlen(a);
    if(b_len < 0)
        b_len = utf32_strlen(b);

    hits = 0;
    for(i=0; i < a_len; ++i) {
        for(j=0; j < b_len; ++j) {
            if(memcmp(a+i,b+j,sizeof(uint32_t)*2) == 0) {
                ++hits;
                break;
            }
        }
    }

    return (2.0*hits)/(a_len+b_len);
}

int utf32_to_utf8(uint32_t *str32, ssize_t len, char **str_ret, size_t *byte_len_ret) {
    int e;
    size_t byte_len,
           i,j,c;
    char *str;

    byte_len = 0;
    for(i=0; i < len; ++i) {
        e = utf8_byte_count(str32[i], &c);
        if(e != UTF8_OK)
            return e;

        byte_len += c;
    }

    str = malloc(byte_len+1);
    if(str == NULL)
        return UTF8_MALLOC;

    for(i=0,j=0; i < len; ++i,j+=c) {
        e = utf8_encode_char(str32[i], str+j, &c);
        if(e != UTF8_OK)
            return e;
    }

    str[j] = '\0';

    *str_ret = str;
    if(byte_len_ret != NULL)
        *byte_len_ret = j;

    return UTF8_OK;
}
